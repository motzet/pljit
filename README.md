# JIT Compiler for PL/I

Just-in-Time Compiler (JIT) for [PL/I](https://en.wikipedia.org/wiki/PL/I)

## Requirements

  * cmake (3.22.1)
  * ClangTidy (14)
  * recent C++20 compiler (g++ or clang++)
  

## Get Started

Clone the repository and run the following commands 

```
$ mkdir build
$ cmake ..
$ make
```

Run pljit:

```
$ make pljit
$ ./pljit/pljit

```

Run tests:

```
$ make test
$ ./test/tester
```

## Contact

Marvin Motzet <marvin.motzet@tum.de>
