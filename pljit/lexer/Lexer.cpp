#include "pljit/lexer/Lexer.hpp"
//---------------------------------------------------------------------------
namespace {
//---------------------------------------------------------------------------
// Return true if char is a whitespace character (space, tab, newline)
bool isSpace(char c) {
    switch (c) {
        case ' ': // SPACE
        case '\t': // HT (horizontal tab)
        case '\r': // CR (carriage ret)
        case '\n': // LF (new line)
        case '\v': // VT (vertical tab)
            return true;
        default:
            return false;
    }
}
//---------------------------------------------------------------------------
// Return true if char is a digit (0-9)
bool isDigit(char c) {
    return '0' <= c && c <= '9';
}
//---------------------------------------------------------------------------
// Return true if char is alpha (a-z, A-Z)
bool isAlpha(char c) {
    return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z');
}
//---------------------------------------------------------------------------
} // namespace
//---------------------------------------------------------------------------
namespace pljit::lexer {
//---------------------------------------------------------------------------
using LocationRef = code::SourceCode::LocationRef;
using RangeRef = code::SourceCode::RangeRef;
using Token = token::Token;
//---------------------------------------------------------------------------
// Class for the lexical analysis stage of the complication process (Lexer)
//---------------------------------------------------------------------------
// Constructor with string_view
Lexer::Lexer(std::string_view sv) : sv(sv) {}
//---------------------------------------------------------------------------
// Constructor with string_view and ostream
Lexer::Lexer(std::string_view sv, std::ostream& out) : sv(sv), out(out) {}
//---------------------------------------------------------------------------
// Return next token
Token Lexer::next() {
    // 1. Advance current lexer position until a non-whitespace character is found
    while (isSpace(sv[index])) {
        ++index;
    }

    // 2. Examine the character at the current position
    char c = sv[index];
    ++index;

    if (c == '\0') {
        return Token(RangeRef(sv, index - 1, index - 1), Token::Type::End);
    } else if (isDigit(c)) {
        return literal();
    } else if (isAlpha(c)) {
        return identifierOrKeyword();
    } else if (c == ':') {
        // assignment
        return assignmentOperator();
    } else if (c == '.') {
        return atom(Token::Type::DotSeparator);
    } else if (c == ',') {
        return atom(Token::Type::CommaSeparator);
    } else if (c == ';') {
        return atom(Token::Type::SemicolonSeparator);
    } else if (c == '=') {
        return atom(Token::Type::EqualSeparator);
    } else if (c == '(') {
        return atom(Token::Type::OpeningParenthesisSeparator);
    } else if (c == ')') {
        return atom(Token::Type::ClosingParenthesisSeparator);
    } else if (c == '+') {
        return atom(Token::Type::AddOperator);
    } else if (c == '-') {
        return atom(Token::Type::SubOperator);
    } else if (c == '*') {
        return atom(Token::Type::MulOperator);
    } else if (c == '/') {
        return atom(Token::Type::DivOperator);
    } else {
        // Error: illegal character
        LocationRef ref(sv, index - 1);
        ref.print("error: illegal character", out);
        return atom(Token::Type::Error);
    }
}
//---------------------------------------------------------------------------
// Return given token with single character
Token Lexer::atom(Token::Type type) {
    return Token(RangeRef(sv, index - 1, index), type);
}
//---------------------------------------------------------------------------
// Return literal token (0-9)
Token Lexer::literal() {
    std::size_t start = index - 1;

    while (isDigit(sv[index])) {
        index++;
    }

    return Token(RangeRef(sv, start, index), Token::Type::Literal);
}
//---------------------------------------------------------------------------
// Return identifier or keyword token
Token Lexer::identifierOrKeyword() {
    std::size_t start = index - 1;

    while (isAlpha(sv[index])) {
        index++;
    }

    // Filter keywords (PARAM, VAR, CONST, BEGIN, RETURN, END)
    std::string_view token = sv.substr(start, index - start);
    RangeRef ref(sv, start, index);
    if (token == "PARAM") {
        return Token(ref, Token::Type::PARAMKeyword);
    } else if (token == "VAR") {
        return Token(ref, Token::Type::VARKeyword);
    } else if (token == "CONST") {
        return Token(ref, Token::Type::CONSTKeyword);
    } else if (token == "BEGIN") {
        return Token(ref, Token::Type::BEGINKeyword);
    } else if (token == "RETURN") {
        return Token(ref, Token::Type::RETURNKeyword);
    } else if (token == "END") {
        return Token(ref, Token::Type::ENDKeyword);
    }

    return Token(ref, Token::Type::Identifier);
}
//---------------------------------------------------------------------------
// Return assignment operator token
Token Lexer::assignmentOperator() {
    std::size_t start = index - 1;

    char c = sv[index];
    ++index;

    if (c == '=') {
        return Token(RangeRef(sv, start, index), Token::Type::AssignmentOperator);
    }

    // Error: expected '='
    LocationRef ref(sv, index - 1);
    ref.print("error: expected '='", out);
    return atom(Token::Type::Error);
}
//---------------------------------------------------------------------------
} // namespace pljit::lexer
//---------------------------------------------------------------------------