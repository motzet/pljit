#ifndef H_pljit_lexer_Lexer
#define H_pljit_lexer_Lexer
//---------------------------------------------------------------------------
#include "pljit/token/Token.hpp"
#include <cstddef> // to use std::size_t
#include <iostream>
#include <ostream>
#include <string_view> // to use std::string_view
//---------------------------------------------------------------------------
namespace pljit::lexer {
//---------------------------------------------------------------------------
/// Class for the lexical analysis stage of the complication process (Lexer)
class Lexer {
    /// String of SourceCode
    std::string_view sv;
    /// Index of current lexer position
    std::size_t index = 0;
    /// Output stream
    std::ostream& out = std::cout;

    /// Return given token with single character
    token::Token atom(token::Token::Type type);
    /// Return literal token (0-9)
    token::Token literal();
    /// Return identifier or keyword token
    token::Token identifierOrKeyword();
    /// Return assignment operator token
    token::Token assignmentOperator();

    public:
    /// Constructor with string_view
    explicit Lexer(std::string_view sv);
    /// Constructor with string_view and ostream
    Lexer(std::string_view sv, std::ostream& out);

    /// Return next token
    token::Token next();
};
//---------------------------------------------------------------------------
} // namespace pljit::lexer
//---------------------------------------------------------------------------
#endif