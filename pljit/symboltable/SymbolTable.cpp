#include "pljit/symboltable/SymbolTable.hpp"
#include <cassert>
//---------------------------------------------------------------------------
namespace pljit::st {
//---------------------------------------------------------------------------
// Struct with properties of identifiers
//---------------------------------------------------------------------------
// Constructor with RangeRef (valid for Variable)
SymbolTable::SymbolProperties::SymbolProperties(code::SourceCode::RangeRef ref) : type(SymbolProperties::Type::Variable), ref(ref) {}
//---------------------------------------------------------------------------
// Constructor with RangeRef and value (valid for Constant)
SymbolTable::SymbolProperties::SymbolProperties(code::SourceCode::RangeRef ref, int64_t value) : type(SymbolProperties::Type::Constant), ref(ref), isInit(true), value(value) {}
//---------------------------------------------------------------------------
// Constructor with RangeRef and indexParam (valid for Parameter)
SymbolTable::SymbolProperties::SymbolProperties(code::SourceCode::RangeRef ref, std::size_t indexParam) : type(SymbolProperties::Type::Parameter), ref(ref), isInit(true), indexParam(indexParam) {}
//---------------------------------------------------------------------------
// An iterator over symbols (const)
//---------------------------------------------------------------------------
// Constructor with iterator
SymbolTable::ConstIterator::ConstIterator(std::unordered_map<std::string, SymbolTable::SymbolProperties>::const_iterator it, std::unordered_map<std::string, SymbolProperties>::const_iterator end) : it(it), end(end) {}
//---------------------------------------------------------------------------
// Dereference
SymbolTable::ConstIterator::reference SymbolTable::ConstIterator::operator*() const {
    assert(it != end);

    return it->first;
}
//---------------------------------------------------------------------------
// Pointer to member
SymbolTable::ConstIterator::pointer SymbolTable::ConstIterator::operator->() const {
    assert(it != end);

    return &it->first;
}
//---------------------------------------------------------------------------
// Pre-increment
SymbolTable::ConstIterator& SymbolTable::ConstIterator::operator++() {
    assert(it != end);

    ++it;
    return *this;
}
//---------------------------------------------------------------------------
// Post-increment
SymbolTable::ConstIterator SymbolTable::ConstIterator::operator++(int) {
    assert(it != end);

    ConstIterator iterator(*this);
    operator++();
    return iterator;
}
//---------------------------------------------------------------------------
// Equality comparison
bool SymbolTable::ConstIterator::operator==(const ConstIterator& other) const {
    return it == other.it && end == other.end;
}
//---------------------------------------------------------------------------
// Inequality comparison
bool SymbolTable::ConstIterator::operator!=(const ConstIterator& other) const {
    return !operator==(other);
}
//---------------------------------------------------------------------------
// Class for the symbol table taking existing identifiers and their properties
//---------------------------------------------------------------------------
// Insert Variable
bool SymbolTable::insertVar(code::SourceCode::RangeRef ref) {
    std::string key(ref.string_view());
    if (!table.contains(key)) {
        table[key] = SymbolProperties(ref);
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
// Insert Constant
bool SymbolTable::insertConst(code::SourceCode::RangeRef key, code::SourceCode::RangeRef literal) {
    std::string _key(key.string_view());
    std::string _lit(literal.string_view());
    if (!table.contains(_key)) {
        table[_key] = SymbolProperties(key, static_cast<int64_t>(std::stoll(_lit)));
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
// Insert Parameter
bool SymbolTable::insertParam(code::SourceCode::RangeRef ref) {
    std::string key(ref.string_view());
    if (!table.contains(key)) {
        table[key] = SymbolProperties(ref, indexParam++);
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
// True if contained
bool SymbolTable::contains(const std::string& key) const {
    return table.contains(key);
}
//---------------------------------------------------------------------------
// Lookup if const
bool SymbolTable::lookupConst(const std::string& key) const {
    assert(table.contains(key));

    return table.at(key).type == SymbolProperties::Type::Constant;
}
//---------------------------------------------------------------------------
// Lookup if param
bool SymbolTable::lookupParam(const std::string& key) const {
    assert(table.contains(key));

    return table.at(key).type == SymbolProperties::Type::Parameter;
}
//---------------------------------------------------------------------------
// Lookup if init
bool SymbolTable::lookupInit(const std::string& key) const {
    assert(table.contains(key));

    return table.at(key).isInit;
}
//---------------------------------------------------------------------------
// Return value (only valid for const identifiers)
int64_t SymbolTable::getConstValue(const std::string& key) const {
    assert(lookupConst(key));

    return table.at(key).value;
}
//---------------------------------------------------------------------------
// Return paramIndex (only valid for parameters)
std::size_t SymbolTable::getParamIndex(const std::string& key) const {
    assert(lookupParam(key));

    return table.at(key).indexParam;
}
//---------------------------------------------------------------------------
// Return num of parameters
std::size_t SymbolTable::getNumParam() const {
    return indexParam;
}
//---------------------------------------------------------------------------
// Set initialized
void SymbolTable::setInit(const std::string& key) {
    assert(table.contains(key));

    table[key].isInit = true;
}
//---------------------------------------------------------------------------
// Return an iterator pointing to the first symbol
SymbolTable::ConstIterator SymbolTable::begin() const {
    return {table.begin(), table.end()};
}
//---------------------------------------------------------------------------
// Return an iterator pointing past the last symbol
SymbolTable::ConstIterator SymbolTable::end() const {
    return {table.end(), table.end()};
}
//---------------------------------------------------------------------------
} // namespace pljit::st
//---------------------------------------------------------------------------
