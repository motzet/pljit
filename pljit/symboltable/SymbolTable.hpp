#ifndef H_pljit_symboltable_SymbolTable
#define H_pljit_symboltable_SymbolTable
//---------------------------------------------------------------------------
#include "pljit/code/SourceCode.hpp"
#include <cstddef> // std::ptrdiff_t
#include <cstdint> // int64_t
#include <iterator> // std::forward_iterator_tag
#include <unordered_map>
#include <vector>
//---------------------------------------------------------------------------
namespace pljit::st {
//---------------------------------------------------------------------------
/// Class for the symbol table taking existing identifiers and their properties
class SymbolTable {
    /// Struct with properties of identifiers
    struct SymbolProperties {
        enum class Type {
            Constant,
            Variable,
            Parameter,
            None
        };
        Type type = Type::None;
        code::SourceCode::RangeRef ref;
        bool isInit = false;
        int64_t value = 0; // only valid for const identifiers
        std::size_t indexParam = 0; // only valid for parameters

        /// Default constructor
        SymbolProperties() = default;
        /// Constructor with RangeRef (valid for Variable)
        explicit SymbolProperties(code::SourceCode::RangeRef ref);
        /// Constructor with RangeRef and value (valid for Constant)
        SymbolProperties(code::SourceCode::RangeRef ref, int64_t value);
        /// Constructor with RangeRef and indexParam (valid for Parameter)
        SymbolProperties(code::SourceCode::RangeRef ref, std::size_t indexParam);
    };

    /// Mapping identifier to properties
    std::unordered_map<std::string, SymbolProperties> table;
    /// Index for parameters
    std::size_t indexParam = 0;

    public:
    /// An iterator over symbols (const)
    class ConstIterator {
        public:
        /// The difference between two iterators
        using difference_type = std::ptrdiff_t;
        /// The type obtained when dereferencing an iterator
        using value_type = std::string;
        /// A pointer to the value type
        using pointer = const value_type*;
        /// A reference to the value type
        using reference = const value_type&;
        /// The iterator category
        using iterator_category = std::forward_iterator_tag;

        private:
        /// ConstIterator to current key (identifier)
        std::unordered_map<std::string, SymbolProperties>::const_iterator it;
        /// ConstIterator to end key (identifier)
        std::unordered_map<std::string, SymbolProperties>::const_iterator end;

        friend class SymbolTable;

        /// Constructor with iterator
        ConstIterator(std::unordered_map<std::string, SymbolProperties>::const_iterator it, std::unordered_map<std::string, SymbolProperties>::const_iterator end);

        public:
        /// Default constructor
        ConstIterator() = default;

        /// Dereference
        reference operator*() const;
        /// Pointer to member
        pointer operator->() const;

        /// Pre-increment
        ConstIterator& operator++();
        /// Post-increment
        ConstIterator operator++(int);

        /// Equality comparison
        bool operator==(const ConstIterator& other) const;
        /// Inequality comparison
        bool operator!=(const ConstIterator& other) const;
    };

    /// Constructor
    SymbolTable() = default;

    /// Insert Variable
    bool insertVar(code::SourceCode::RangeRef ref);
    /// Insert Constant
    bool insertConst(code::SourceCode::RangeRef key, code::SourceCode::RangeRef literal);
    /// Insert Parameter
    bool insertParam(code::SourceCode::RangeRef ref);

    /// True if contained
    bool contains(const std::string& key) const;
    /// Lookup if const
    bool lookupConst(const std::string& key) const;
    /// Lookup if param
    bool lookupParam(const std::string& key) const;
    /// Lookup if init
    bool lookupInit(const std::string& key) const;
    /// Return value (only valid for const identifiers)
    int64_t getConstValue(const std::string& key) const;
    /// Return paramIndex (only valid for parameters)
    std::size_t getParamIndex(const std::string& key) const;
    /// Return num of parameters
    std::size_t getNumParam() const;

    /// Set initialized
    void setInit(const std::string& key);

    /// Return an iterator pointing to the first symbol
    ConstIterator begin() const;
    /// Return an iterator pointing past the last symbol
    ConstIterator end() const;
};
//---------------------------------------------------------------------------
} // namespace pljit::st
//---------------------------------------------------------------------------
#endif