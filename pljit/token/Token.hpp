#ifndef H_pljit_token_Token
#define H_pljit_token_Token
//---------------------------------------------------------------------------
#include "pljit/code/SourceCode.hpp"
#include <string>
//---------------------------------------------------------------------------
namespace pljit::token {
//---------------------------------------------------------------------------
/// Class to represent different types of tokens (terminal symbol of the grammar)
class Token {
    public:
    /// Detailed types of tokens
    enum class Type {
        Identifier,

        PARAMKeyword,
        VARKeyword,
        CONSTKeyword,
        BEGINKeyword,
        RETURNKeyword,
        ENDKeyword,

        Literal,

        AddOperator,
        SubOperator,
        MulOperator,
        DivOperator,
        AssignmentOperator,

        DotSeparator,
        CommaSeparator,
        SemicolonSeparator,
        EqualSeparator,
        OpeningParenthesisSeparator,
        ClosingParenthesisSeparator,

        End,
        Error
    };

    private:
    /// Reference to the source code (RangeRef)
    code::SourceCode::RangeRef ref;
    /// Type of the token
    Type type;

    public:
    /// Constructor with RangeRef and type
    Token(code::SourceCode::RangeRef ref, Type type);

    /// Return type
    Type getType() const;
    /// Return ref
    code::SourceCode::RangeRef getRef() const;
    /// String representation
    static std::string toString(Token::Type type);
};
//---------------------------------------------------------------------------
} // namespace pljit::token
//---------------------------------------------------------------------------
#endif