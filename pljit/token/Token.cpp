#include "pljit/token/Token.hpp"
#include <iostream>
//---------------------------------------------------------------------------
namespace pljit::token {
//---------------------------------------------------------------------------
using RangeRef = code::SourceCode::RangeRef;
//---------------------------------------------------------------------------
// Class to represent different types of tokens (terminal symbol of the grammar)
//---------------------------------------------------------------------------
// Constructor with RangeRef and type
Token::Token(RangeRef ref, Type type) : ref(ref), type(type) {}
//---------------------------------------------------------------------------
// Return type
Token::Type Token::getType() const {
    return type;
}
//---------------------------------------------------------------------------
// Return ref
RangeRef Token::getRef() const {
    return ref;
}
//---------------------------------------------------------------------------
// String representation
std::string Token::toString(Token::Type type) {
    switch (type) {
        case Type::Identifier:
            return "identifier";
        case Type::PARAMKeyword:
            return "PARAM";
        case Type::VARKeyword:
            return "VAR";
        case Type::CONSTKeyword:
            return "CONST";
        case Type::BEGINKeyword:
            return "BEGIN";
        case Type::RETURNKeyword:
            return "RETURN";
        case Type::ENDKeyword:
            return "END";
        case Type::Literal:
            return "literal";
        case Type::AddOperator:
            return "+";
        case Type::SubOperator:
            return "-";
        case Type::MulOperator:
            return "*";
        case Type::DivOperator:
            return "/";
        case Type::AssignmentOperator:
            return ":=";
        case Type::DotSeparator:
            return ".";
        case Type::CommaSeparator:
            return ",";
        case Type::SemicolonSeparator:
            return ";";
        case Type::EqualSeparator:
            return "=";
        case Type::OpeningParenthesisSeparator:
            return "(";
        case Type::ClosingParenthesisSeparator:
            return ")";
        case Type::End:
            return "no token left";
        case Type::Error:
            return "error during tokenizing";
    }

    __builtin_unreachable();
}
//---------------------------------------------------------------------------
} // namespace pljit::token
//---------------------------------------------------------------------------