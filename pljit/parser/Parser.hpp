#ifndef H_pljit_parser_Parser
#define H_pljit_parser_Parser
//---------------------------------------------------------------------------
#include "pljit/lexer/Lexer.hpp"
#include "pljit/parsetree/ParseTree.hpp"
#include <iostream>
#include <memory>
#include <ostream>
//---------------------------------------------------------------------------
namespace pljit::parser {
//---------------------------------------------------------------------------
/// Class for the syntax analysis stage of the compilation process (Parser)
/// Implementing a recursive-descent parser
class Parser {
    /// Lexer
    lexer::Lexer& lexer;
    /// Current token
    token::Token token;
    /// Error flag
    bool error = false;
    /// Output stream
    std::ostream& out = std::cout;

    /// Accept token with called type
    bool accept(token::Token::Type type);
    /// Expect token with called type, oth. error
    bool expect(token::Token::Type type);

    /// Return accepted generic terminal node (operator, keyword, separator) of given type
    std::unique_ptr<pt::GenericTerminal> acceptGenericTerminal(token::Token::Type type);
    /// Return expected generic terminal node (operator, keyword, separator) of given type
    std::unique_ptr<pt::GenericTerminal> expectGenericTerminal(token::Token::Type type);
    /// Return accepted literal node
    std::unique_ptr<pt::Literal> acceptLiteral();
    /// Return expected literal node
    std::unique_ptr<pt::Literal> expectLiteral();
    /// Return accepted identifier node
    std::unique_ptr<pt::Identifier> acceptIdentifier();
    /// Return expected identifier node
    std::unique_ptr<pt::Identifier> expectIdentifier();

    /// Return primary-expression node
    std::unique_ptr<pt::PrimaryExpression> parsePrimaryExpression();
    /// Return unary-expression node
    std::unique_ptr<pt::UnaryExpression> parseUnaryExpression();
    /// Return multiplicative-expression node
    std::unique_ptr<pt::MultiplicativeExpression> parseMultiplicativeExpression();
    /// Return additive-expression node
    std::unique_ptr<pt::AdditiveExpression> parseAdditiveExpression();
    /// Return assignment-expression node
    std::unique_ptr<pt::AssignmentExpression> parseAssignmentExpression();
    /// Return statement node
    std::unique_ptr<pt::Statement> parseStatement();
    /// Return statement-list node
    std::unique_ptr<pt::StatementList> parseStatementList();
    /// Return compound-statement node
    std::unique_ptr<pt::CompoundStatement> parseCompoundStatement();

    /// Return init-declarator node
    std::unique_ptr<pt::InitDeclarator> parseInitDeclarator();
    /// Return init-declarator-list node
    std::unique_ptr<pt::InitDeclaratorList> parseInitDeclaratorList();
    /// Return declarator-list node
    std::unique_ptr<pt::DeclaratorList> parseDeclaratorList();
    /// Return constant-declarations node
    std::unique_ptr<pt::ConstantDeclarations> parseConstantDeclarations();
    /// Return variable-declarations node
    std::unique_ptr<pt::VariableDeclarations> parseVariableDeclarations();
    /// Return parameter-declarations node
    std::unique_ptr<pt::ParameterDeclarations> parseParameterDeclarations();

    public:
    /// Constructor with Lexer
    explicit Parser(lexer::Lexer& lexer);
    /// Constructor with Lexer and ostream
    Parser(lexer::Lexer& lexer, std::ostream& out);

    /// Return function-definition node
    std::unique_ptr<pt::FunctionDefinition> parseFunctionDefinition();
};
//---------------------------------------------------------------------------
} // namespace pljit::parser
//---------------------------------------------------------------------------
#endif