#include "pljit/parser/Parser.hpp"
#include <string>
#include <utility> // to use std::move
//---------------------------------------------------------------------------
namespace pljit::parser {
//---------------------------------------------------------------------------
using LocationRef = code::SourceCode::LocationRef;
using RangeRef = code::SourceCode::RangeRef;
//---------------------------------------------------------------------------
// Class for the syntax analysis stage of the compilation process (Parser)
// Implementing a recursive-descent parser
//---------------------------------------------------------------------------
// Constructor with Lexer
Parser::Parser(lexer::Lexer& lexer) : lexer(lexer), token(lexer.next()) {}
//---------------------------------------------------------------------------
/// Constructor with Lexer and ostream
Parser::Parser(lexer::Lexer& lexer, std::ostream& out) : lexer(lexer), token(lexer.next()), out(out) {}
//---------------------------------------------------------------------------
// Accept token with called type
bool Parser::accept(token::Token::Type type) {
    if (token.getType() == type) {
        token = lexer.next();
        return true;
    } else if (token.getType() == token::Token::Type::Error) {
        error = true;
    }
    return false;
}
//---------------------------------------------------------------------------
// Expect token with called type, oth. error
bool Parser::expect(token::Token::Type type) {
    if (accept(type)) {
        return true;
    }
    if (!error) {
        // Error: expected type (token mismatch)
        token.getRef().print("error: expected '" + token::Token::toString(type) + "'", out);
        error = true;
    }

    return false;
}
//---------------------------------------------------------------------------
// Return accepted generic terminal node (operator, keyword, separator) of given type
std::unique_ptr<pt::GenericTerminal> Parser::acceptGenericTerminal(token::Token::Type type) {
    auto ref = token.getRef();
    if (accept(type)) {
        return std::make_unique<pt::GenericTerminal>(ref);
    }
    return nullptr; // token mismatch
}
//---------------------------------------------------------------------------
// Return expected generic terminal node (operator, keyword, separator) of given type
std::unique_ptr<pt::GenericTerminal> Parser::expectGenericTerminal(token::Token::Type type) {
    auto ref = token.getRef();
    if (expect(type)) {
        return std::make_unique<pt::GenericTerminal>(ref);
    }
    return nullptr; // token mismatch
}
//---------------------------------------------------------------------------
// Return accepted literal node
std::unique_ptr<pt::Literal> Parser::acceptLiteral() {
    auto ref = token.getRef();
    if (accept(token::Token::Type::Literal)) {
        return std::make_unique<pt::Literal>(ref);
    }
    return nullptr; // token mismatch
}
//---------------------------------------------------------------------------
// Return expected literal node
std::unique_ptr<pt::Literal> Parser::expectLiteral() {
    auto ref = token.getRef();
    if (expect(token::Token::Type::Literal)) {
        return std::make_unique<pt::Literal>(ref);
    }
    return nullptr; // token mismatch
}
//---------------------------------------------------------------------------
// Return accepted identifier node
std::unique_ptr<pt::Identifier> Parser::acceptIdentifier() {
    auto ref = token.getRef();
    if (accept(token::Token::Type::Identifier)) {
        return std::make_unique<pt::Identifier>(ref);
    }
    return nullptr; // token mismatch
}
//---------------------------------------------------------------------------
// Return expected identifier node
std::unique_ptr<pt::Identifier> Parser::expectIdentifier() {
    auto ref = token.getRef();
    if (expect(token::Token::Type::Identifier)) {
        return std::make_unique<pt::Identifier>(ref);
    }
    return nullptr; // token mismatch
}
//---------------------------------------------------------------------------
/// Return primary-expression node
std::unique_ptr<pt::PrimaryExpression> Parser::parsePrimaryExpression() {
    // Check for identifier
    auto iden = acceptIdentifier();
    if (iden) {
        return std::make_unique<pt::PrimaryExpression>(
            iden->getRef(),
            std::move(iden));
    }
    // Check for literal
    auto literal = acceptLiteral();
    if (literal) {
        return std::make_unique<pt::PrimaryExpression>(
            literal->getRef(),
            std::move(literal));
    }
    // Check for opening parenthesis
    auto openParent = acceptGenericTerminal(token::Token::Type::OpeningParenthesisSeparator);
    if (openParent) {
        auto addExpr = parseAdditiveExpression();
        if (error) {
            return nullptr;
        }

        auto closeParent = expectGenericTerminal(token::Token::Type::ClosingParenthesisSeparator);
        if (error) {
            return nullptr;
        }

        return std::make_unique<pt::PrimaryExpression>(
            RangeRef(openParent->getBeginRef(), closeParent->getEndRef()),
            std::move(openParent),
            std::move(addExpr),
            std::move(closeParent));
    }

    /// Print error
    token.getRef().print("error: expected literal, identifier or opening parenthesis", out);
    error = true;

    return nullptr;
}
//---------------------------------------------------------------------------
/// Return unary-expression node
std::unique_ptr<pt::UnaryExpression> Parser::parseUnaryExpression() {
    pt::UnaryExpression::OpType opType;

    // Check for "+"
    auto op = acceptGenericTerminal(token::Token::Type::AddOperator);
    if (op) {
        opType = pt::UnaryExpression::OpType::Add;
    } else {
        // Check for "-"
        op = acceptGenericTerminal(token::Token::Type::SubOperator);
        opType = op ? pt::UnaryExpression::OpType::Sub : pt::UnaryExpression::OpType::None;
    }

    auto primaryExpr = parsePrimaryExpression();
    if (error) {
        return nullptr;
    }

    // Determine LocationRef begin
    LocationRef begin;
    if (op) {
        begin = op->getBeginRef();
    } else {
        begin = primaryExpr->getBeginRef();
    }

    // Return without additional operator
    return std::make_unique<pt::UnaryExpression>(
        RangeRef(begin, primaryExpr->getEndRef()),
        std::move(op),
        std::move(primaryExpr),
        opType);
}
//---------------------------------------------------------------------------
// Return multiplicative-expression node
std::unique_ptr<pt::MultiplicativeExpression> Parser::parseMultiplicativeExpression() {
    pt::MultiplicativeExpression::OpType opType = pt::MultiplicativeExpression::OpType::None;

    auto unaryExpr = parseUnaryExpression();
    if (error) {
        return nullptr;
    }

    // Check for "*"
    auto op = acceptGenericTerminal(token::Token::Type::MulOperator);
    if (!op) {
        // Check for "/"
        op = acceptGenericTerminal(token::Token::Type::DivOperator);
        if (!op) {
            // Return without additional expression
            return std::make_unique<pt::MultiplicativeExpression>(
                RangeRef(unaryExpr->getBeginRef(), unaryExpr->getEndRef()),
                std::move(unaryExpr),
                nullptr,
                nullptr,
                opType);
        } else {
            opType = pt::MultiplicativeExpression::OpType::Div;
        }
    } else {
        opType = pt::MultiplicativeExpression::OpType::Mul;
    }

    auto mulExpr = parseMultiplicativeExpression();
    if (error) {
        return nullptr;
    }

    return std::make_unique<pt::MultiplicativeExpression>(
        RangeRef(unaryExpr->getBeginRef(), mulExpr->getEndRef()),
        std::move(unaryExpr),
        std::move(op),
        std::move(mulExpr),
        opType);
}
//---------------------------------------------------------------------------
// Return additive-expression node
std::unique_ptr<pt::AdditiveExpression> Parser::parseAdditiveExpression() {
    pt::AdditiveExpression::OpType opType = pt::AdditiveExpression::OpType::None;

    auto mulExpr = parseMultiplicativeExpression();
    if (error) {
        return nullptr;
    }

    // Check for "+"
    auto op = acceptGenericTerminal(token::Token::Type::AddOperator);
    if (!op) {
        // Check for "-"
        op = acceptGenericTerminal(token::Token::Type::SubOperator);
        if (!op) {
            // Return without additional expression
            return std::make_unique<pt::AdditiveExpression>(
                RangeRef(mulExpr->getBeginRef(), mulExpr->getEndRef()),
                std::move(mulExpr),
                nullptr,
                nullptr,
                opType);
        } else {
            opType = pt::AdditiveExpression::OpType::Sub;
        }
    } else {
        opType = pt::AdditiveExpression::OpType::Add;
    }

    auto addExpr = parseAdditiveExpression();
    if (error) {
        return nullptr;
    }

    return std::make_unique<pt::AdditiveExpression>(
        RangeRef(mulExpr->getBeginRef(), addExpr->getEndRef()),
        std::move(mulExpr),
        std::move(op),
        std::move(addExpr),
        opType);
}
//---------------------------------------------------------------------------
// Return assignment-expression node
std::unique_ptr<pt::AssignmentExpression> Parser::parseAssignmentExpression() {
    auto iden = expectIdentifier();
    if (error) {
        return nullptr;
    }

    auto assign = expectGenericTerminal(token::Token::Type::AssignmentOperator);
    if (error) {
        return nullptr;
    }

    auto addExpr = parseAdditiveExpression();
    if (error) {
        return nullptr;
    }

    return std::make_unique<pt::AssignmentExpression>(
        RangeRef(iden->getBeginRef(), addExpr->getEndRef()),
        std::move(iden),
        std::move(assign),
        std::move(addExpr));
}
//---------------------------------------------------------------------------
// Return statement node
std::unique_ptr<pt::Statement> Parser::parseStatement() {
    // Check for keyword RETURN
    auto keyword = acceptGenericTerminal(token::Token::Type::RETURNKeyword);
    if (keyword) {
        auto expr = parseAdditiveExpression();
        if (error) {
            return nullptr;
        }

        // Return RETURNExpr
        return std::make_unique<pt::Statement>(
            RangeRef(keyword->getBeginRef(), expr->getEndRef()),
            std::move(keyword),
            std::move(expr));
    }

    // Check for assignment-expression
    auto expr = parseAssignmentExpression();
    if (expr && !error) {
        // Return assignment-expression
        return std::make_unique<pt::Statement>(
            expr->getRef(),
            std::move(expr));
    }

    return nullptr;
}
//---------------------------------------------------------------------------
// Return statement-list node
std::unique_ptr<pt::StatementList> Parser::parseStatementList() {
    std::vector<std::unique_ptr<pt::Statement>> stmts;
    std::vector<std::unique_ptr<pt::GenericTerminal>> semicolons;

    do {
        auto stmt = parseStatement();
        if (error) {
            return nullptr;
        }
        stmts.push_back(std::move(stmt));

        auto semicolon = acceptGenericTerminal(token::Token::Type::SemicolonSeparator);
        if (semicolon) {
            semicolons.push_back(std::move(semicolon));
        } else {
            break;
        }
    } while (true);

    return std::make_unique<pt::StatementList>(
        RangeRef(stmts.front()->getBeginRef(), stmts.back()->getEndRef()),
        std::move(stmts), std::move(semicolons));
}
//---------------------------------------------------------------------------
// Return compound-statement node
std::unique_ptr<pt::CompoundStatement> Parser::parseCompoundStatement() {
    auto begin = expectGenericTerminal(token::Token::Type::BEGINKeyword);
    if (error) {
        return nullptr;
    }

    auto stmtList = parseStatementList();
    if (error) {
        return nullptr;
    }

    auto end = expectGenericTerminal(token::Token::Type::ENDKeyword);
    if (error) {
        return nullptr;
    }

    return std::make_unique<pt::CompoundStatement>(
        RangeRef(begin->getBeginRef(), end->getEndRef()),
        std::move(begin),
        std::move(stmtList),
        std::move(end));
}
//---------------------------------------------------------------------------
// Return init-declarator node
std::unique_ptr<pt::InitDeclarator> Parser::parseInitDeclarator() {
    auto iden = expectIdentifier();
    if (error) {
        return nullptr;
    }

    auto equal = expectGenericTerminal(token::Token::Type::EqualSeparator);
    if (error) {
        return nullptr;
    }

    auto literal = expectLiteral();
    if (error) {
        return nullptr;
    }

    return std::make_unique<pt::InitDeclarator>(
        RangeRef(iden->getBeginRef(), literal->getEndRef()),
        std::move(iden), std::move(equal), std::move(literal));
}
//---------------------------------------------------------------------------
// Return init-declarator-list node
std::unique_ptr<pt::InitDeclaratorList> Parser::parseInitDeclaratorList() {
    std::vector<std::unique_ptr<pt::InitDeclarator>> initDecls;
    std::vector<std::unique_ptr<pt::GenericTerminal>> commas;

    do {
        auto initDecl = parseInitDeclarator();
        if (error) {
            return nullptr;
        }
        initDecls.push_back(std::move(initDecl));

        auto comma = acceptGenericTerminal(token::Token::Type::CommaSeparator);
        if (comma) {
            commas.push_back(std::move(comma));
        } else {
            break;
        }
    } while (true);

    return std::make_unique<pt::InitDeclaratorList>(
        RangeRef(initDecls.front()->getBeginRef(), initDecls.back()->getEndRef()),
        std::move(initDecls), std::move(commas));
}
//---------------------------------------------------------------------------
// Return declarator-list node
std::unique_ptr<pt::DeclaratorList> Parser::parseDeclaratorList() {
    std::vector<std::unique_ptr<pt::Identifier>> idens;
    std::vector<std::unique_ptr<pt::GenericTerminal>> commas;

    do {
        auto iden = expectIdentifier();
        if (error) {
            return nullptr;
        }
        idens.push_back(std::move(iden));

        auto comma = acceptGenericTerminal(token::Token::Type::CommaSeparator);
        if (comma) {
            commas.push_back(std::move(comma));
        } else {
            break;
        }
    } while (true);

    return std::make_unique<pt::DeclaratorList>(
        RangeRef(idens.front()->getBeginRef(), idens.back()->getEndRef()),
        std::move(idens), std::move(commas));
}
//---------------------------------------------------------------------------
// Return constant-declarations node
std::unique_ptr<pt::ConstantDeclarations> Parser::parseConstantDeclarations() {
    auto keyword = acceptGenericTerminal(token::Token::Type::CONSTKeyword);
    if (keyword) {
        auto initDeclList = parseInitDeclaratorList();
        if (error) {
            return nullptr;
        }

        auto semicolon = expectGenericTerminal(token::Token::Type::SemicolonSeparator);
        if (error) {
            return nullptr;
        }

        return std::make_unique<pt::ConstantDeclarations>(
            RangeRef(keyword->getBeginRef(), semicolon->getEndRef()),
            std::move(keyword),
            std::move(initDeclList),
            std::move(semicolon));
    }

    return nullptr; // no constant-declarations
}
//---------------------------------------------------------------------------
// Return variable-declarations node
std::unique_ptr<pt::VariableDeclarations> Parser::parseVariableDeclarations() {
    auto keyword = acceptGenericTerminal(token::Token::Type::VARKeyword);
    if (keyword) {
        auto declList = parseDeclaratorList();
        if (error) {
            return nullptr;
        }

        auto semicolon = expectGenericTerminal(token::Token::Type::SemicolonSeparator);
        if (error) {
            return nullptr;
        }

        return std::make_unique<pt::VariableDeclarations>(
            RangeRef(keyword->getBeginRef(), semicolon->getEndRef()),
            std::move(keyword),
            std::move(declList),
            std::move(semicolon));
    }

    return nullptr; // no variable-declarations
}
//---------------------------------------------------------------------------
// Return parameter-declarations node
std::unique_ptr<pt::ParameterDeclarations> Parser::parseParameterDeclarations() {
    auto keyword = acceptGenericTerminal(token::Token::Type::PARAMKeyword);
    if (keyword) {
        auto declList = parseDeclaratorList();
        if (error) {
            return nullptr;
        }

        auto semicolon = expectGenericTerminal(token::Token::Type::SemicolonSeparator);
        if (error) {
            return nullptr;
        }

        return std::make_unique<pt::ParameterDeclarations>(
            RangeRef(keyword->getBeginRef(), semicolon->getEndRef()),
            std::move(keyword),
            std::move(declList),
            std::move(semicolon));
    }

    return nullptr; // no parameter-declarations
}
//---------------------------------------------------------------------------
// Return function-definition node
std::unique_ptr<pt::FunctionDefinition> Parser::parseFunctionDefinition() {
    // parameter-declarations
    auto paramDelcs = parseParameterDeclarations();
    if (error) {
        return nullptr;
    }

    // variable-declarations
    auto varDecls = parseVariableDeclarations();
    if (error) {
        return nullptr;
    }

    // constant-declarations
    auto constDecls = parseConstantDeclarations();
    if (error) {
        return nullptr;
    }

    // compound-statement
    auto compStmt = parseCompoundStatement();
    if (error) {
        return nullptr;
    }

    // dot
    auto dot = expectGenericTerminal(token::Token::Type::DotSeparator);
    if (error) {
        return nullptr;
    }

    // Determine LocationRef begin
    LocationRef begin;
    if (paramDelcs) {
        begin = paramDelcs->getBeginRef();
    } else if (varDecls) {
        begin = varDecls->getBeginRef();
    } else if (constDecls) {
        begin = constDecls->getBeginRef();
    } else {
        begin = compStmt->getBeginRef();
    }

    // Determine LocationRef end
    LocationRef end;
    end = dot->getEndRef();

    return std::make_unique<pt::FunctionDefinition>(
        RangeRef(begin, end),
        std::move(paramDelcs),
        std::move(varDecls),
        std::move(constDecls),
        std::move(compStmt),
        std::move(dot));
}
//---------------------------------------------------------------------------
} // namespace pljit::parser
//---------------------------------------------------------------------------