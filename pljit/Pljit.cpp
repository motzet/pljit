#include "Pljit.hpp"
#include <cassert>
#include <mutex>
#include <utility>
//---------------------------------------------------------------------------
namespace pljit {
//---------------------------------------------------------------------------
// Class for function resources
//---------------------------------------------------------------------------
// Constructor
Pljit::FunctionResource::FunctionResource(code::SourceCode&& code) : code(code) {}
//---------------------------------------------------------------------------
// Class for the evaluation result
//---------------------------------------------------------------------------
// Constructor
Pljit::FunctionHandle::Result::Result(int64_t val, bool error) : val(val), error(error) {}
//---------------------------------------------------------------------------
// Class for the function handle
//---------------------------------------------------------------------------
// Constructor
Pljit::FunctionHandle::FunctionHandle(Pljit* pljit, std::size_t index) : pljit(pljit), index(index) {}
//---------------------------------------------------------------------------
// Parse function
void Pljit::FunctionHandle::parse(std::ostream& out) {
    std::unique_lock lock(*pljit->resources[index].mutex);

    if (!pljit->resources[index].error && !pljit->resources[index].pt) {
        lexer::Lexer lexer(pljit->resources[index].code.string_view(), out);
        parser::Parser parser(lexer, out);
        auto pt = parser.parseFunctionDefinition();

        if (!pt) {
            // Error during parsing
            pljit->resources[index].error = true;
        }
        pljit->resources[index].pt = std::move(pt);
    }
}
//---------------------------------------------------------------------------
// Analyze function
void Pljit::FunctionHandle::analyse(std::ostream& out) {
    std::unique_lock lock(*pljit->resources[index].mutex);

    if (!pljit->resources[index].error && !pljit->resources[index].ast) {
        semantics::SemanticAnalyser sem(out);
        auto ast = sem.analyzeFunction(*pljit->resources[index].pt);
        if (!ast) {
            // Error during semantic analysis
            pljit->resources[index].error = true;
        }
        pljit->resources[index].ast = std::move(ast);
    }
}
//---------------------------------------------------------------------------
// Optimize function
void Pljit::FunctionHandle::optimize() {
    std::unique_lock lock(*pljit->resources[index].mutex);

    if (!pljit->resources[index].error && pljit->resources[index].ast && !pljit->resources[index].isOptimized) {
        optimizer::DeadCodeOptimizer opt1;
        optimizer::ConstPropOptimizer opt2;
        opt1.optimize(pljit->resources[index].ast);
        opt2.optimize(pljit->resources[index].ast);
        pljit->resources[index].isOptimized = true;
    }
}
//---------------------------------------------------------------------------
// Call function with parameters (compile and optimize)
Pljit::FunctionHandle::Result Pljit::FunctionHandle::operator()(const std::vector<int64_t>& params, std::ostream& out) {
    // Compile and optimize
    parse(out);
    analyse(out);
    optimize();

    // Evaluate if error is false
    std::shared_lock lock(*pljit->resources[index].mutex);
    if (!pljit->resources[index].error) {
        assert(pljit->resources[index].ast->getSymTable().getNumParam() == params.size());
        ast::EvaluationContext ctx(out);

        for (auto it = pljit->resources[index].ast->getSymTable().begin(); it != pljit->resources[index].ast->getSymTable().end(); ++it) {
            if (pljit->resources[index].ast->getSymTable().lookupConst(*it)) {
                ctx.setValue(*it, pljit->resources[index].ast->getSymTable().getConstValue(*it));
            }
            if (pljit->resources[index].ast->getSymTable().lookupParam(*it)) {
                ctx.setValue(*it, params[pljit->resources[index].ast->getSymTable().getParamIndex(*it)]);
            }
        }

        int64_t val = pljit->resources[index].ast->evaluate(ctx);
        if (ctx.isError()) {
            return {0, true};
        }
        return {val, false};
    } else {
        return {0, true};
    }
}
//---------------------------------------------------------------------------
// Top-level class for the JIT
//---------------------------------------------------------------------------
// Register function
Pljit::FunctionHandle Pljit::registerFunction(std::string str) {
    resources.emplace_back(FunctionResource(code::SourceCode(std::move(str))));

    return FunctionHandle(this, resources.size() - 1);
}
//---------------------------------------------------------------------------
} // namespace pljit
//---------------------------------------------------------------------------
