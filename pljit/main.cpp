#include "pljit/Pljit.hpp"
#include <iostream>
#include <string>
//---------------------------------------------------------------------------
using namespace std;
using namespace pljit;
//---------------------------------------------------------------------------
int main() {
    string inputFunction01 =
        "PARAM width, height, depth;\n"
        "VAR volume;\n"
        "CONST density = 2400;\n\n"
        "BEGIN\n"
        "  volume := width * height * depth;\n"
        "  RETURN density * volume\n\n"
        "END.";
    string inputFunction02 =
        "PARAM a, b, c;\n"
        "VAR d;\n"
        "CONST e = 2;\n\n"
        "BEGIN\n"
        "  d := +(1+a) -3 * b + c;\n"
        "  RETURN e * d\n\n"
        "END.";

    cout << "calculate" << endl;

    Pljit jit;
    auto func01 = jit.registerFunction(move(inputFunction01));
    auto func02 = jit.registerFunction(move(inputFunction02));
    
    auto result01 = func01({10, 50, 100});
    cout << "result01: " << result01.val << endl;

    auto result02 = func02({10, 50, 100});
    cout << "result02: " << result02.val << endl;
}
//---------------------------------------------------------------------------
