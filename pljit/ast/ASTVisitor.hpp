#ifndef H_pljit_ast_ASTVisitor
#define H_pljit_ast_ASTVisitor
//---------------------------------------------------------------------------
#include "pljit/ast/AST.hpp"
//---------------------------------------------------------------------------
namespace pljit::ast {
//---------------------------------------------------------------------------
/// Interface for visitor pattern for the AST (abstract class)
class ASTVisitor {
    public:
    /// Constructor (see C.126)

    /// Destructor
    virtual ~ASTVisitor() = default;

    /// Visit function for Identifier (pure virtual)
    virtual void visit(const Identifier& node) = 0;
    /// Visit function for Literal (pure virtual)
    virtual void visit(const Literal& node) = 0;
    /// Visit function for UnaryExpression (pure virtual)
    virtual void visit(const UnaryExpression& node) = 0;
    /// Visit function for BinaryExpression (pure virtual)
    virtual void visit(const BinaryExpression& node) = 0;

    /// Visit function for AssignmentStatement (pure virtual)
    virtual void visit(const AssignmentStatement& node) = 0;
    /// Visit function for ReturnStatement (pure virtual)
    virtual void visit(const ReturnStatement& node) = 0;
    /// Visit function for Function (pure virtual)
    virtual void visit(const Function& node) = 0;
};
//---------------------------------------------------------------------------
} // namespace pljit::ast
//---------------------------------------------------------------------------
#endif
