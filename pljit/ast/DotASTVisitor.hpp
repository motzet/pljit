#ifndef H_pljit_ast_DotASTVisitor
#define H_pljit_ast_DotASTVisitor
//---------------------------------------------------------------------------
#include "pljit/ast/ASTVisitor.hpp"
#include <cstddef>
#include <iostream>
#include <ostream>
//---------------------------------------------------------------------------
namespace pljit::ast {
//---------------------------------------------------------------------------
/// Class DotASTVisitor: Implementation of the ASTVisitor
/// Printing the AST in DOT format
class DotASTVisitor : public ASTVisitor {
    /// Output stream
    std::ostream& out = std::cout;
    /// Index for DOT node
    std::size_t index = 0;

    public:
    /// Default constructor
    DotASTVisitor() = default;
    /// Constructor with std::ostream
    explicit DotASTVisitor(std::ostream& out);

    /// Destructor
    ~DotASTVisitor() override = default; // implicitly-declared destructor is virtual too

    /// Visit function for Identifier
    void visit(const Identifier& node) override;
    /// Visit function for Literal
    void visit(const Literal& node) override;
    /// Visit function for UnaryExpression
    void visit(const UnaryExpression& node) override;
    /// Visit function for BinaryExpression
    void visit(const BinaryExpression& node) override;

    /// Visit function for AssignmentStatement
    void visit(const AssignmentStatement& node) override;
    /// Visit function for ReturnStatement
    void visit(const ReturnStatement& node) override;
    /// Visit function for Function
    void visit(const Function& node) override;
};
//---------------------------------------------------------------------------
} // namespace pljit::ast
//---------------------------------------------------------------------------
#endif
