#ifndef H_pljit_ast_EvaluationContext
#define H_pljit_ast_EvaluationContext
//---------------------------------------------------------------------------
#include <cstdint> // to use int64_t
#include <iostream>
#include <ostream>
#include <string>
#include <string_view>
#include <unordered_map>
//---------------------------------------------------------------------------
namespace pljit::ast {
//---------------------------------------------------------------------------
/// Class EvaluationContext
/// Storing all variables used in the program
class EvaluationContext {
    /// Map variable to current value
    std::unordered_map<std::string, int64_t> values;
    /// Error flag (division by zero)
    bool error = false;
    /// Result value
    int64_t result = 0;
    /// Output stream
    std::ostream& out = std::cout;

    public:
    /// Default constructor
    EvaluationContext() = default;
    /// Constructor with ostream
    explicit EvaluationContext(std::ostream& out);

    /// Insert a value for the given variable/identifier
    void setValue(const std::string& key, int64_t value);
    /// Return the value for the given variable/identifier
    int64_t getValue(const std::string& key) const;

    /// Set result value
    void setResult(int64_t value);
    /// Return result value
    int64_t getResult() const;

    /// Set error flag
    void setError();
    /// Return error flag
    bool isError() const;

    /// Print error division by zero
    void print(std::string_view err) const;
};
//---------------------------------------------------------------------------
} // namespace pljit::ast
//---------------------------------------------------------------------------
#endif
