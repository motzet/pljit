#include "pljit/ast/AST.hpp"
#include "pljit/ast/ASTVisitor.hpp"
#include "pljit/ast/EvaluationContext.hpp"
#include <utility> // to use std::move
//---------------------------------------------------------------------------
namespace pljit::ast {
//---------------------------------------------------------------------------
// Base class for the abstract syntax tree (AST) nodes (abstract)
//---------------------------------------------------------------------------
// Constructor with type
ASTNode::ASTNode(Type type) : type(type) {}
//---------------------------------------------------------------------------
/// Return the type of the PT node as a member of the enum class Type
ASTNode::Type ASTNode::getType() const {
    return type;
}
//---------------------------------------------------------------------------
// Base class for expressions (abstract)
//---------------------------------------------------------------------------
// Constructor with type
Expression::Expression(Type type) : ASTNode(type) {}
//---------------------------------------------------------------------------
// Class Identifier
//---------------------------------------------------------------------------
// Constructor
Identifier::Identifier(std::string_view id) : Expression(Type::Identifier), id(id) {}
//---------------------------------------------------------------------------
// Return id
const std::string& Identifier::getId() const {
    return id;
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void Identifier::accept(ASTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Evaluate specific node
int64_t Identifier::evaluate(EvaluationContext& ctx) const {
    if (ctx.isError()) {
        return 0;
    }

    return ctx.getValue(id);
}
//---------------------------------------------------------------------------
// Class Literal
//---------------------------------------------------------------------------
// Constructor
Literal::Literal(int64_t value) : Expression(Type::Literal), value(value) {}
//---------------------------------------------------------------------------
// Return value
int64_t Literal::getValue() const {
    return value;
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void Literal::accept(ASTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Evaluate specific node
int64_t Literal::evaluate(EvaluationContext& ctx) const {
    if (ctx.isError()) {
        return 0;
    }

    return value;
}
//---------------------------------------------------------------------------
// Class UnaryExpression (arithmetic)
//---------------------------------------------------------------------------
// Constructor
UnaryExpression::UnaryExpression(std::unique_ptr<Expression> child, UnaryOperator op) : Expression(Type::UnaryExpression), child(std::move(child)), op(op) {}
//---------------------------------------------------------------------------
// Return const reference of child
const Expression& UnaryExpression::getChild() const {
    return *child;
}
//---------------------------------------------------------------------------
// Return reference of child
std::unique_ptr<Expression>& UnaryExpression::getChild() {
    return child;
}
//---------------------------------------------------------------------------
// Return op
UnaryExpression::UnaryOperator UnaryExpression::getOp() const {
    return op;
}
//---------------------------------------------------------------------------
// Return op as string
std::string UnaryExpression::getOpString() const {
    switch (op) {
        case UnaryOperator::Add:
            return "+";
        case UnaryOperator::Sub:
            return "-";
    }

    __builtin_unreachable();
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void UnaryExpression::accept(ASTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Evaluate specific node
int64_t UnaryExpression::evaluate(EvaluationContext& ctx) const {
    if (ctx.isError()) {
        return 0;
    }

    int64_t val = 0;
    switch (op) {
        case UnaryOperator::Add:
            val = +child->evaluate(ctx);
            break;
        case UnaryOperator::Sub:
            val = -child->evaluate(ctx);
            break;
        default:
            ctx.setError();
            break;
    }

    if (ctx.isError()) {
        return 0;
    }
    return val;
}
//---------------------------------------------------------------------------
// Class BinaryExpression (arithmetic)
//---------------------------------------------------------------------------
// Constructor
BinaryExpression::BinaryExpression(std::unique_ptr<Expression> left, std::unique_ptr<Expression> right, BinaryOperator op) : Expression(Type::BinaryExpression), left(std::move(left)), right(std::move(right)), op(op) {}
//---------------------------------------------------------------------------
// Return const reference of left children
const Expression& BinaryExpression::getLeft() const {
    return *left;
}
//---------------------------------------------------------------------------
// Return const reference of right children
const Expression& BinaryExpression::getRight() const {
    return *right;
}
//---------------------------------------------------------------------------
// Return reference of left children
std::unique_ptr<Expression>& BinaryExpression::getLeft() {
    return left;
}
//---------------------------------------------------------------------------
// Return reference of right children
std::unique_ptr<Expression>& BinaryExpression::getRight() {
    return right;
}
//---------------------------------------------------------------------------
// Return op
BinaryExpression::BinaryOperator BinaryExpression::getOp() const {
    return op;
}
//---------------------------------------------------------------------------
// Return op as string
std::string BinaryExpression::getOpString() const {
    switch (op) {
        case BinaryOperator::Add:
            return "+";
        case BinaryOperator::Sub:
            return "-";
        case BinaryOperator::Mul:
            return "*";
        case BinaryOperator::Div:
            return "/";
    }

    __builtin_unreachable();
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void BinaryExpression::accept(ASTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Evaluate specific node
int64_t BinaryExpression::evaluate(EvaluationContext& ctx) const {
    if (ctx.isError()) {
        return 0;
    }

    int64_t val = 0;
    switch (op) {
        case BinaryOperator::Add:
            val = left->evaluate(ctx) + right->evaluate(ctx);
            break;
        case BinaryOperator::Sub:
            val = left->evaluate(ctx) - right->evaluate(ctx);
            break;
        case BinaryOperator::Mul:
            val = left->evaluate(ctx) * right->evaluate(ctx);
            break;
        case BinaryOperator::Div: {
            int64_t denum = right->evaluate(ctx);
            // Error: division by zero
            if (!denum) {
                ctx.setError();
                ctx.print("division by zero");
                break;
            }
            val = left->evaluate(ctx) / denum;
            break;
        }
        default:
            ctx.setError();
            break;
    }

    if (ctx.isError()) {
        return 0;
    }
    return val;
}
//---------------------------------------------------------------------------
// Base class for statements (abstract)
//---------------------------------------------------------------------------
// Constructor with type
Statement::Statement(Type type) : ASTNode(type) {}
//---------------------------------------------------------------------------
// Class AssignmentStatement
//---------------------------------------------------------------------------
// Constructor
AssignmentStatement::AssignmentStatement(std::unique_ptr<Identifier> iden, std::unique_ptr<Expression> expr) : Statement(Type::AssignmentStatement), iden(std::move(iden)), expr(std::move(expr)) {}
//---------------------------------------------------------------------------
// Return const reference of identifier
const Identifier& AssignmentStatement::getIden() const {
    return *iden;
}
//---------------------------------------------------------------------------
// Return const reference of expression
const Expression& AssignmentStatement::getExpr() const {
    return *expr;
}
//---------------------------------------------------------------------------
// Return reference of identifier
std::unique_ptr<Identifier>& AssignmentStatement::getIden() {
    return iden;
}
//---------------------------------------------------------------------------
// Return reference of expression
std::unique_ptr<Expression>& AssignmentStatement::getExpr() {
    return expr;
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void AssignmentStatement::accept(ASTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Evaluate specific node
int64_t AssignmentStatement::evaluate(EvaluationContext& ctx) const {
    if (ctx.isError()) {
        return 0;
    }

    int64_t val = expr->evaluate(ctx);
    if (ctx.isError()) {
        return 0;
    }

    ctx.setValue(iden->getId(), val);
    return val;
}
//---------------------------------------------------------------------------
// Class ReturnStatement
//---------------------------------------------------------------------------
// Constructor
ReturnStatement::ReturnStatement(std::unique_ptr<Expression> child) : Statement(Type::ReturnStatement), child(std::move(child)) {}
//---------------------------------------------------------------------------
// Return const reference of child
const Expression& ReturnStatement::getChild() const {
    return *child;
}
//---------------------------------------------------------------------------
// Return reference of child
std::unique_ptr<Expression>& ReturnStatement::getChild() {
    return child;
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void ReturnStatement::accept(ASTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Evaluate specific node
int64_t ReturnStatement::evaluate(EvaluationContext& ctx) const {
    if (ctx.isError()) {
        return 0;
    }

    int64_t val = child->evaluate(ctx);
    if (ctx.isError()) {
        return 0;
    }

    ctx.setResult(val);
    return val;
}
//---------------------------------------------------------------------------
// Class Function (Root node of AST)
//---------------------------------------------------------------------------
// Constructor
Function::Function(std::unique_ptr<st::SymbolTable> symTable, std::vector<std::unique_ptr<Statement>> stmts) : ASTNode(Type::Function), symTable(std::move(symTable)), stmts(std::move(stmts)) {}
//---------------------------------------------------------------------------
// Return const reference of symTable
const st::SymbolTable& Function::getSymTable() const {
    return *symTable;
}
//---------------------------------------------------------------------------
// Return const reference of stmts
const std::vector<std::unique_ptr<Statement>>& Function::getStmts() const {
    return stmts;
}
//---------------------------------------------------------------------------
// Return reference of stmts
std::vector<std::unique_ptr<Statement>>& Function::getStmts() {
    return stmts;
}
//---------------------------------------------------------------------------
// Transfer ownership of the symTable to the caller
std::unique_ptr<st::SymbolTable> Function::releaseSymTable() {
    return std::move(symTable);
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void Function::accept(ASTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Evaluate specific node
int64_t Function::evaluate(EvaluationContext& ctx) const {
    for (const auto& stmt : stmts) {
        stmt->evaluate(ctx);

        // Break after first return stmt
        if (stmt->getType() == Type::ReturnStatement) {
            break;
        }
    }

    if (ctx.isError()) {
        return 0;
    }

    return ctx.getResult();
}
//---------------------------------------------------------------------------
} // namespace pljit::ast
//---------------------------------------------------------------------------