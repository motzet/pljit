#include "pljit/ast/DotASTVisitor.hpp"
//---------------------------------------------------------------------------
namespace pljit::ast {
//---------------------------------------------------------------------------
// Class DotASTVisitor: Implementation of the ASTVisitor
//---------------------------------------------------------------------------
// Constructor with std::ostream
DotASTVisitor::DotASTVisitor(std::ostream& out) : out(out) {}
//---------------------------------------------------------------------------
// Visit function for Identifier
void DotASTVisitor::visit(const Identifier& node) {
    out << "\t" << index << " [label=\"" << node.getId() << "\"];" << std::endl;
}
//---------------------------------------------------------------------------
// Visit function for Literal
void DotASTVisitor::visit(const Literal& node) {
    out << "\t" << index << " [label=\"" << node.getValue() << "\"];" << std::endl;
}
//---------------------------------------------------------------------------
// Visit function for UnaryExpression
void DotASTVisitor::visit(const UnaryExpression& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"" << node.getOpString() << "\"];" << std::endl;
    out << "\t" << id << " -> " << ++index << ";" << std::endl;

    node.getChild().accept(*this);
}
//---------------------------------------------------------------------------
// Visit function for BinaryExpression
void DotASTVisitor::visit(const BinaryExpression& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"" << node.getOpString() << "\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getLeft().accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getRight().accept(*this);
}
//---------------------------------------------------------------------------
// Visit function for AssignmentStatement
void DotASTVisitor::visit(const AssignmentStatement& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\":=\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getIden().accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getExpr().accept(*this);
}
//---------------------------------------------------------------------------
// Visit function for ReturnStatement
void DotASTVisitor::visit(const ReturnStatement& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"RETURN\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getChild().accept(*this);
}
//---------------------------------------------------------------------------
// Visit function for Function
void DotASTVisitor::visit(const Function& node) {
    std::size_t id = index;

    // Begin DOT format
    out << "digraph {" << std::endl;
    out << "\t" << id << " [label=\"Function\"];" << std::endl;

    for (const auto& stmt : node.getStmts()) {
        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        stmt->accept(*this);
    }

    out << "}" << std::endl;
    // End DOT format
}
//---------------------------------------------------------------------------
} // namespace pljit::ast
//---------------------------------------------------------------------------