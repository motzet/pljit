#include "pljit/ast/EvaluationContext.hpp"
#include <cassert>
//---------------------------------------------------------------------------
namespace pljit::ast {
//---------------------------------------------------------------------------
// Class EvaluationContext
//---------------------------------------------------------------------------
// Constructor with ostream
EvaluationContext::EvaluationContext(std::ostream& out) : out(out) {}
//---------------------------------------------------------------------------
// Insert a value for the given variable/identifier
void EvaluationContext::setValue(const std::string& key, int64_t value) {
    values[key] = value;
}
//---------------------------------------------------------------------------
// Return the value for the given variable/identifier
int64_t EvaluationContext::getValue(const std::string& key) const {
    // Semantic Analyser should detect undeclared and uninitialized variable
    assert(values.contains(key));
    return values.at(key);
}
//---------------------------------------------------------------------------
// Set result value
void EvaluationContext::setResult(int64_t value) {
    result = value;
}
//---------------------------------------------------------------------------
// Return result value
int64_t EvaluationContext::getResult() const {
    return result;
}
//---------------------------------------------------------------------------
// Set error flag
void EvaluationContext::setError() {
    error = true;
}
//---------------------------------------------------------------------------
// Return error flag
bool EvaluationContext::isError() const {
    return error;
}
//---------------------------------------------------------------------------
// Print error division by zero
void EvaluationContext::print(std::string_view err) const {
    out << "error: " << err << std::endl;
}
//---------------------------------------------------------------------------
} // namespace pljit::ast
//---------------------------------------------------------------------------
