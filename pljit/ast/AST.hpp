#ifndef H_pljit_ast_AST
#define H_pljit_ast_AST
//---------------------------------------------------------------------------
#include "pljit/symboltable/SymbolTable.hpp"
#include <cstdint> // to use int64_t
#include <memory>
#include <string>
#include <string_view>
#include <vector>
//---------------------------------------------------------------------------
namespace pljit::ast {
//---------------------------------------------------------------------------
/// AST visitor base class (forward declared)
class ASTVisitor;
//---------------------------------------------------------------------------
/// EvaluationContext class (forward declared)
class EvaluationContext;
//---------------------------------------------------------------------------
/// Base class for the abstract syntax tree (AST) nodes (abstract)
class ASTNode {
    public:
    /// All possible types of ASTNode
    enum class Type {
        Identifier,
        Literal,
        UnaryExpression,
        BinaryExpression,

        AssignmentStatement,
        ReturnStatement,

        Function
    };

    private:
    /// Type of ASTNode
    Type type;

    protected:
    /// Constructor with type
    explicit ASTNode(Type type);

    public:
    /// Destructor
    virtual ~ASTNode() = default;

    /// Copy constructor (see C.67)
    ASTNode(const ASTNode&) = delete;
    /// Copy assignment (see C.67)
    ASTNode& operator=(const ASTNode&) = delete;

    /// Return the type of the AST node as a member of the enum class Type
    Type getType() const;

    /// Accept function of the visitor pattern (pure virtual)
    virtual void accept(ASTVisitor& visitor) const = 0;
    /// Evaluate specific node (pure virtual)
    virtual int64_t evaluate(EvaluationContext& ctx) const = 0;
};
//---------------------------------------------------------------------------
/// Base class for expressions (abstract)
class Expression : public ASTNode {
    protected:
    /// Constructor with type
    explicit Expression(Type type);

    public:
    /// Destructor
    ~Expression() override = default; // implicitly-declared destructor is virtual too
};
//---------------------------------------------------------------------------
/// Class Identifier
class Identifier : public Expression {
    /// Id
    std::string id;

    public:
    /// Constructor
    explicit Identifier(std::string_view id);

    /// Return id
    const std::string& getId() const;

    /// Accept function of the visitor pattern
    void accept(ASTVisitor& visitor) const override;
    /// Evaluate specific node
    int64_t evaluate(EvaluationContext& ctx) const override;
};
//---------------------------------------------------------------------------
/// Class Literal
class Literal : public Expression {
    /// Value
    int64_t value;

    public:
    /// Constructor
    explicit Literal(int64_t value);

    /// Return value
    int64_t getValue() const;

    /// Accept function of the visitor pattern
    void accept(ASTVisitor& visitor) const override;
    /// Evaluate specific node
    int64_t evaluate(EvaluationContext& ctx) const override;
};
//---------------------------------------------------------------------------
/// Class UnaryExpression (arithmetic)
class UnaryExpression : public Expression {
    public:
    /// Type of possible unary operators
    enum class UnaryOperator {
        Add,
        Sub
    };

    private:
    /// Child node
    std::unique_ptr<Expression> child;
    /// Operator
    UnaryOperator op;

    public:
    /// Constructor
    UnaryExpression(std::unique_ptr<Expression> child, UnaryOperator op);

    /// Return const reference of child
    const Expression& getChild() const;
    /// Return reference of child
    std::unique_ptr<Expression>& getChild();
    /// Return op
    UnaryOperator getOp() const;
    /// Return op as string
    std::string getOpString() const;

    /// Accept function of the visitor pattern
    void accept(ASTVisitor& visitor) const override;
    /// Evaluate specific node
    int64_t evaluate(EvaluationContext& ctx) const override;
};
//---------------------------------------------------------------------------
/// Class BinaryExpression (arithmetic)
class BinaryExpression : public Expression {
    public:
    /// Type of possible binary operators
    enum class BinaryOperator {
        Add,
        Sub,
        Mul,
        Div
    };

    private:
    /// Left node
    std::unique_ptr<Expression> left;
    /// Right node
    std::unique_ptr<Expression> right;
    /// Operator
    BinaryOperator op;

    public:
    /// Constructor
    BinaryExpression(std::unique_ptr<Expression> left, std::unique_ptr<Expression> right, BinaryOperator op);

    /// Return const reference of left children
    const Expression& getLeft() const;
    /// Return const reference of right children
    const Expression& getRight() const;
    /// Return reference of left children
    std::unique_ptr<Expression>& getLeft();
    /// Return reference of right children
    std::unique_ptr<Expression>& getRight();
    /// Return op
    BinaryOperator getOp() const;
    /// Return op as string
    std::string getOpString() const;

    /// Accept function of the visitor pattern
    void accept(ASTVisitor& visitor) const override;
    /// Evaluate specific node
    int64_t evaluate(EvaluationContext& ctx) const override;
};
//---------------------------------------------------------------------------
/// Base class for statements (abstract)
class Statement : public ASTNode {
    protected:
    /// Constructor with type
    explicit Statement(Type type);

    public:
    /// Destructor
    ~Statement() override = default; // implicitly-declared destructor is virtual too
};
//---------------------------------------------------------------------------
/// Class AssignmentStatement
class AssignmentStatement : public Statement {
    /// Left node (Identifier)
    std::unique_ptr<Identifier> iden;
    /// Right node (Expression)
    std::unique_ptr<Expression> expr;

    public:
    /// Constructor
    AssignmentStatement(std::unique_ptr<Identifier> iden, std::unique_ptr<Expression> expr);

    /// Return const reference of identifier
    const Identifier& getIden() const;
    /// Return const reference of expression
    const Expression& getExpr() const;
    /// Return reference of identifier
    std::unique_ptr<Identifier>& getIden();
    /// Return reference of expression
    std::unique_ptr<Expression>& getExpr();

    /// Accept function of the visitor pattern
    void accept(ASTVisitor& visitor) const override;
    /// Evaluate specific node
    int64_t evaluate(EvaluationContext& ctx) const override;
};
//---------------------------------------------------------------------------
/// Class ReturnStatement
class ReturnStatement : public Statement {
    /// Child node
    std::unique_ptr<Expression> child;

    public:
    /// Constructor
    explicit ReturnStatement(std::unique_ptr<Expression> child);

    /// Return const reference of child
    const Expression& getChild() const;
    /// Return reference of child
    std::unique_ptr<Expression>& getChild();

    /// Accept function of the visitor pattern
    void accept(ASTVisitor& visitor) const override;
    /// Evaluate specific node
    int64_t evaluate(EvaluationContext& ctx) const override;
};
//---------------------------------------------------------------------------
/// Class Function (Root node of AST)
class Function : public ASTNode {
    /// Symbol table
    std::unique_ptr<st::SymbolTable> symTable;
    /// List of Statement
    std::vector<std::unique_ptr<Statement>> stmts;

    public:
    /// Constructor
    Function(std::unique_ptr<st::SymbolTable> symTable, std::vector<std::unique_ptr<Statement>> stmts);

    /// Return const reference of symTable
    const st::SymbolTable& getSymTable() const;
    /// Return const reference of stmts
    const std::vector<std::unique_ptr<Statement>>& getStmts() const;
    /// Return reference of stmts
    std::vector<std::unique_ptr<Statement>>& getStmts();
    /// Transfer ownership of the symTable to the caller
    std::unique_ptr<st::SymbolTable> releaseSymTable();

    /// Accept function of the visitor pattern
    void accept(ASTVisitor& visitor) const override;
    /// Evaluate specific node
    int64_t evaluate(EvaluationContext& ctx) const override;
};
//---------------------------------------------------------------------------
} // namespace pljit::ast
//---------------------------------------------------------------------------
#endif