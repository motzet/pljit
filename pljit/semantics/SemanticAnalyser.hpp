#ifndef H_pljit_semantics_SemanticAnalyser
#define H_pljit_semantics_SemanticAnalyser
//---------------------------------------------------------------------------
#include "pljit/ast/AST.hpp"
#include "pljit/parsetree/ParseTree.hpp"
#include <iostream>
#include <memory>
#include <ostream>
//---------------------------------------------------------------------------
namespace pljit::semantics {
//---------------------------------------------------------------------------
/// Class for the semantic analysis stage of the compilation process (SemanticAnalyser)
class SemanticAnalyser {
    /// SymbolTable
    std::unique_ptr<st::SymbolTable> symTable = std::make_unique<st::SymbolTable>();
    /// Error flag
    bool error = false;
    /// Flag return stmt
    bool returnFound = false;
    /// Output stream
    std::ostream& out = std::cout;

    /// Build symbol table from parameter-declarations
    void buildSymTable(const pt::ParameterDeclarations* decls);
    /// Build symbol table from variable-declarations
    void buildSymTable(const pt::VariableDeclarations* decls);
    /// Build symbol table from constant-declarations
    void buildSymTable(const pt::ConstantDeclarations* decls);

    /// Return Identifier node from parse tree
    std::unique_ptr<ast::Identifier> analyzeIdentifier(const pt::Identifier* iden, bool checkInit = true);
    /// Return Literal node from parse tree
    std::unique_ptr<ast::Literal> analyzeLiteral(const pt::Literal* literal) const;

    /// Return Expression node from parse tree unary expression
    std::unique_ptr<ast::UnaryExpression> analyzeUnaryExpression(const pt::UnaryExpression* expr);
    
    /// Return Expression node from parse tree additive expression
    std::unique_ptr<ast::BinaryExpression> analyzeBinaryExpression(const pt::AdditiveExpression* expr);
    /// Return Expression node from parse tree multiplicative expression
    std::unique_ptr<ast::BinaryExpression> analyzeBinaryExpression(const pt::MultiplicativeExpression* expr);

    /// Return Expression node from parse tree additive expression
    std::unique_ptr<ast::Expression> analyzeExpression(const pt::AdditiveExpression* expr);
    /// Return Expression node from parse tree multiplicative expression
    std::unique_ptr<ast::Expression> analyzeExpression(const pt::MultiplicativeExpression* expr);
    /// Return Expression node from parse tree unary expression
    std::unique_ptr<ast::Expression> analyzeExpression(const pt::UnaryExpression* expr);
    /// Return Expression node from parse tree primary expression
    std::unique_ptr<ast::Expression> analyzeExpression(const pt::PrimaryExpression* expr);

    /// Return Statement node from parse tree statement
    std::unique_ptr<ast::Statement> analyzeStatement(const pt::Statement* stmt);

    public:
    /// Constructor
    SemanticAnalyser() = default;
    /// Constructor with ostream
    explicit SemanticAnalyser(std::ostream& out);

    /// Return root note of AST
    std::unique_ptr<ast::Function> analyzeFunction(const pt::FunctionDefinition& root);
};
//---------------------------------------------------------------------------
} // namespace pljit::semantics
//---------------------------------------------------------------------------
#endif