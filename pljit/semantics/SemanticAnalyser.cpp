#include "pljit/semantics/SemanticAnalyser.hpp"
#include <cassert>
#include <utility>
//---------------------------------------------------------------------------
namespace pljit::semantics {
//---------------------------------------------------------------------------
// Class for the semantic analysis stage of the compilation process (SemanticAnalyser)
//---------------------------------------------------------------------------
// Constructor with ostream
SemanticAnalyser::SemanticAnalyser(std::ostream& out) : out(out) {}
//---------------------------------------------------------------------------
// Build symbol table from parameter-declarations
void SemanticAnalyser::buildSymTable(const pt::ParameterDeclarations* decls) {
    if (decls) {
        for (const auto& iden : decls->getDeclList()->getIdens()) {
            bool b = symTable->insertParam(iden->getRef());
            if (!b) {
                // Error: The same identifier being declared twice
                iden->getRef().print("error: identifier being declared twice", out);
                error = true;
            }
        }
    }
}
//---------------------------------------------------------------------------
// Build symbol table from variable-declarations
void SemanticAnalyser::buildSymTable(const pt::VariableDeclarations* decls) {
    if (decls) {
        for (const auto& iden : decls->getDeclList()->getIdens()) {
            bool b = symTable->insertVar(iden->getRef());
            if (!b) {
                // Error: The same identifier being declared twice
                iden->getRef().print("error: identifier being declared twice", out);
                error = true;
            }
        }
    }
}
//---------------------------------------------------------------------------
// Build symbol table from constant-declarations
void SemanticAnalyser::buildSymTable(const pt::ConstantDeclarations* decls) {
    if (decls) {
        for (const auto& initDecls : decls->getInitDeclList()->getInitDecls()) {
            bool b = symTable->insertConst(initDecls->getIden()->getRef(), initDecls->getLiteral()->getRef());
            if (!b) {
                // Error: The same identifier being declared twice
                initDecls->getIden()->getRef().print("error: identifier being declared twice", out);
                error = true;
            }
        }
    }
}
//---------------------------------------------------------------------------
// Return Identifier node from parse tree
std::unique_ptr<ast::Identifier> SemanticAnalyser::analyzeIdentifier(const pt::Identifier* iden, bool checkInit) {
    std::string str(std::string(iden->getRef().string_view()));
    // Error: Using an undeclared identifier
    if (!symTable->contains(str)) {
        iden->getRef().print("error: undeclared identifier", out);
        error = true;
    }

    // Error: Using an uninitialized variable
    if (checkInit) {
        if (symTable->contains(str) && !symTable->lookupInit(str)) {
            iden->getRef().print("error: uninitialized variable", out);
            error = true;
        }
    }

    return std::make_unique<ast::Identifier>(iden->getRef().string_view());
}
//---------------------------------------------------------------------------
// Return Literal node from parse tree
std::unique_ptr<ast::Literal> SemanticAnalyser::analyzeLiteral(const pt::Literal* literal) const {
    int64_t val = static_cast<int64_t>(std::stoll(std::string(literal->getRef().string_view())));
    return std::make_unique<ast::Literal>(val);
}
//---------------------------------------------------------------------------
// Return Expression node from parse tree unary expression
std::unique_ptr<ast::UnaryExpression> SemanticAnalyser::analyzeUnaryExpression(const pt::UnaryExpression* expr) {
    assert(expr->getOpType() != pt::UnaryExpression::OpType::None);

    auto astExpr = analyzeExpression(expr->getPrimExpr());

    switch (expr->getOpType()) {
        case pt::UnaryExpression::OpType::Add:
            return std::make_unique<ast::UnaryExpression>(std::move(astExpr), ast::UnaryExpression::UnaryOperator::Add);
        case pt::UnaryExpression::OpType::Sub:
            return std::make_unique<ast::UnaryExpression>(std::move(astExpr), ast::UnaryExpression::UnaryOperator::Sub);
        default:
            return nullptr;
    }
}
//---------------------------------------------------------------------------
// Return Expression node from parse tree additive expression
std::unique_ptr<ast::BinaryExpression> SemanticAnalyser::analyzeBinaryExpression(const pt::AdditiveExpression* expr) {
    assert(expr->getOp());

    auto left = analyzeExpression(expr->getMulExpr());
    auto right = analyzeExpression(expr->getAddExpr());

    switch (expr->getOpType()) {
        case pt::AdditiveExpression::OpType::Add:
            return std::make_unique<ast::BinaryExpression>(std::move(left), std::move(right), ast::BinaryExpression::BinaryOperator::Add);
        case pt::AdditiveExpression::OpType::Sub:
            return std::make_unique<ast::BinaryExpression>(std::move(left), std::move(right), ast::BinaryExpression::BinaryOperator::Sub);
        default:
            return nullptr;
    }
}
//---------------------------------------------------------------------------
// Return Expression node from parse tree multiplicative expression
std::unique_ptr<ast::BinaryExpression> SemanticAnalyser::analyzeBinaryExpression(const pt::MultiplicativeExpression* expr) {
    assert(expr->getOp());

    auto left = analyzeExpression(expr->getUnaryExpr());
    auto right = analyzeExpression(expr->getMulExpr());

    switch (expr->getOpType()) {
        case pt::MultiplicativeExpression::OpType::Mul:
            return std::make_unique<ast::BinaryExpression>(std::move(left), std::move(right), ast::BinaryExpression::BinaryOperator::Mul);
        case pt::MultiplicativeExpression::OpType::Div:
            return std::make_unique<ast::BinaryExpression>(std::move(left), std::move(right), ast::BinaryExpression::BinaryOperator::Div);
        default:
            return nullptr;
    }
}
//---------------------------------------------------------------------------
// Return Expression node from parse tree additive expression
std::unique_ptr<ast::Expression> SemanticAnalyser::analyzeExpression(const pt::AdditiveExpression* expr) {
    if (expr->getOp()) {
        return analyzeBinaryExpression(expr);
    } else {
        return analyzeExpression(expr->getMulExpr());
    }
}
//---------------------------------------------------------------------------
// Return Expression node from parse tree multiplicative expression
std::unique_ptr<ast::Expression> SemanticAnalyser::analyzeExpression(const pt::MultiplicativeExpression* expr) {
    if (expr->getOp()) {
        return analyzeBinaryExpression(expr);
    } else {
        return analyzeExpression(expr->getUnaryExpr());
    }
}
//---------------------------------------------------------------------------
// Return Expression node from parse tree unary expression
std::unique_ptr<ast::Expression> SemanticAnalyser::analyzeExpression(const pt::UnaryExpression* expr) {
    if (expr->getOp()) {
        return analyzeUnaryExpression(expr);
    } else {
        return analyzeExpression(expr->getPrimExpr());
    }
}
//---------------------------------------------------------------------------
// Return Expression node from parse tree primary expression
std::unique_ptr<ast::Expression> SemanticAnalyser::analyzeExpression(const pt::PrimaryExpression* expr) {
    switch (expr->getAType()) {
        case pt::PrimaryExpression::AlternationType::Literal: {
            assert(expr->getChildren().size() == 1);

            const auto* lit = static_cast<const pt::Literal*>(expr->getChildren()[0].get()); // NOLINT
            return analyzeLiteral(lit);
        }
        case pt::PrimaryExpression::AlternationType::Identifier: {
            assert(expr->getChildren().size() == 1);

            const auto* iden = static_cast<const pt::Identifier*>(expr->getChildren()[0].get()); // NOLINT
            return analyzeIdentifier(iden);
        }
        case pt::PrimaryExpression::AlternationType::ParenthesisedAdditiveExpression: {
            assert(expr->getChildren().size() == 3);

            const auto* addExpr = static_cast<const pt::AdditiveExpression*>(expr->getChildren()[1].get()); // NOLINT
            return analyzeExpression(addExpr);
        }
    }

    __builtin_unreachable();
}
//---------------------------------------------------------------------------
// Return Statement node from parse tree statement
std::unique_ptr<ast::Statement> SemanticAnalyser::analyzeStatement(const pt::Statement* stmt) {
    switch (stmt->getAType()) {
        case pt::Statement::AlternationType::AssignmentExpr: {
            assert(stmt->getChildren().size() == 1);

            const auto* assignExpr = static_cast<const pt::AssignmentExpression*>(stmt->getChildren()[0].get()); // NOLINT
            auto iden = analyzeIdentifier(assignExpr->getIden(), false);

            if (symTable->lookupConst(iden->getId())) {
                // Error: Assigning a value to a constant
                assignExpr->getRef().print("error: assigning a value to a constant", out);
                error = true;
            }

            auto expr = analyzeExpression(assignExpr->getAddExpr());
            if (!error) {
                // Update symTable (iden is initialized)
                symTable->setInit(iden->getId());
            }
            
            return std::make_unique<ast::AssignmentStatement>(std::move(iden), std::move(expr));
        }
        case pt::Statement::AlternationType::RETURNExpr: {
            assert(stmt->getChildren().size() == 2);

            returnFound = true;
            const auto* addExpr = static_cast<const pt::AdditiveExpression*>(stmt->getChildren()[1].get()); // NOLINT
            auto child = analyzeExpression(addExpr);
            
            return std::make_unique<ast::ReturnStatement>(std::move(child));
        }
    }

    __builtin_unreachable();
}
//---------------------------------------------------------------------------
// Return root note of AST
std::unique_ptr<ast::Function> SemanticAnalyser::analyzeFunction(const pt::FunctionDefinition& root) {
    // Build symbol table
    buildSymTable(root.getParamDecls());
    buildSymTable(root.getVarDecls());
    buildSymTable(root.getConstDecls());

    // Analyze statements from statement-list
    std::vector<std::unique_ptr<ast::Statement>> stmts;
    for (const auto& stmt : root.getCompStmt()->getStmtList()->getStmts()) {
        stmts.push_back(analyzeStatement(stmt.get()));
    }

    if (!returnFound) {
        // Error: Missing return statement
        root.getCompStmt()->getStmtList()->getStmts().back()->getRef().print("error: missing return statement", out);
        error = true;
    }

    if (error) {
        return nullptr;
    }

    return std::make_unique<ast::Function>(std::move(symTable), std::move(stmts));
}
//---------------------------------------------------------------------------
} // namespace pljit::semantics
//---------------------------------------------------------------------------