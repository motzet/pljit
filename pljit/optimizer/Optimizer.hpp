#ifndef H_pljit_optimizer_Optimizer
#define H_pljit_optimizer_Optimizer
//---------------------------------------------------------------------------
#include "pljit/ast/AST.hpp"
#include <memory>
//---------------------------------------------------------------------------
namespace pljit::optimizer {
//---------------------------------------------------------------------------
/// Class Optimizer (abstract class)
/// Representing an optimization pass on the AST
class Optimizer {
    public:
    /// Constructor (see C.126)

    /// Destructor
    virtual ~Optimizer() = default;

    /// Optimize AST (pure virtual)
    virtual void optimize(std::unique_ptr<ast::Function>& fun) = 0;
};
//---------------------------------------------------------------------------
} // namespace pljit::optimizer
//---------------------------------------------------------------------------
#endif
