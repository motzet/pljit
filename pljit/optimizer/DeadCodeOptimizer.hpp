#ifndef H_pljit_optimizer_DeadCodeOptimizer
#define H_pljit_optimizer_DeadCodeOptimizer
//---------------------------------------------------------------------------
#include "pljit/optimizer/Optimizer.hpp"
//---------------------------------------------------------------------------
namespace pljit::optimizer {
//---------------------------------------------------------------------------
/// Optimizer eliminating dead code
class DeadCodeOptimizer : public Optimizer {
    public:
    /// Default constructor
    DeadCodeOptimizer() = default;
    /// Destructor
    ~DeadCodeOptimizer() override = default; // implicitly-declared destructor is virtual too

    /// Optimize AST: Eliminate dead code on Function node
    void optimize(std::unique_ptr<ast::Function>& fun) override;
};
//---------------------------------------------------------------------------
} // namespace pljit::optimizer
//---------------------------------------------------------------------------
#endif