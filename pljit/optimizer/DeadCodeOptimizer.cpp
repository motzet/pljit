#include "pljit/optimizer/DeadCodeOptimizer.hpp"
#include <utility>
//---------------------------------------------------------------------------
namespace pljit::optimizer {
//---------------------------------------------------------------------------
// Optimizer eliminating dead code
//---------------------------------------------------------------------------
// Optimize AST: Eliminate dead code on Function node
void DeadCodeOptimizer::optimize(std::unique_ptr<ast::Function>& fun) {
    if (fun) {
        std::vector<std::unique_ptr<ast::Statement>> stmts;
        for (auto& stmt : fun->getStmts()) {
            stmts.push_back(std::move(stmt));

            if (stmts.back()->getType() == ast::ASTNode::Type::ReturnStatement) {
                break;
            }
        }

        fun = std::make_unique<ast::Function>(fun->releaseSymTable(), std::move(stmts));
    }
}
//---------------------------------------------------------------------------
} // namespace pljit::optimizer
//---------------------------------------------------------------------------