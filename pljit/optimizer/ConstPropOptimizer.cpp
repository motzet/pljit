#include "pljit/optimizer/ConstPropOptimizer.hpp"
#include <utility>
//---------------------------------------------------------------------------
namespace pljit::optimizer {
//---------------------------------------------------------------------------
// Optimizer pre-calculating const expressions
//---------------------------------------------------------------------------
// Traverse AST statement node in post-order (left, right, parent)
void ConstPropOptimizer::traverse(std::unique_ptr<ast::Statement>& node) {
    switch (node->getType()) {
        case ast::ASTNode::Type::AssignmentStatement: {
            auto& stmt = static_cast<ast::AssignmentStatement&>(*node); // NOLINT

            if (traverse(stmt.getExpr())) {
                // iden (left) is const -> insert in table
                int64_t val = node->evaluate(ctx);
                ctx.setValue(stmt.getIden()->getId(), val);
                table[stmt.getIden()->getId()] = val;
                node = std::make_unique<ast::AssignmentStatement>(std::move(stmt.getIden()), std::make_unique<ast::Literal>(val)); // stmt invalid
            } else {
                // iden (left) is not const -> remove from table
                table.erase(stmt.getIden()->getId());
            }
            break;
        }
        case ast::ASTNode::Type::ReturnStatement: {
            auto& stmt = static_cast<ast::ReturnStatement&>(*node); // NOLINT

            if (traverse(stmt.getChild())) {
                node = std::make_unique<ast::ReturnStatement>(std::make_unique<ast::Literal>(node->evaluate(ctx))); // stmt invalid
            }
            break;
        }
        default:
            break;
    }
}
//---------------------------------------------------------------------------
// Traverse AST expression node in post order and return if const
bool ConstPropOptimizer::traverse(std::unique_ptr<ast::Expression>& node) {
    switch (node->getType()) {
        case ast::ASTNode::Type::BinaryExpression: {
            auto& expr = static_cast<ast::BinaryExpression&>(*node); // NOLINT
            bool left = traverse(expr.getLeft());
            bool right = traverse(expr.getRight());

            if (left && right) {
                node = std::make_unique<ast::Literal>(node->evaluate(ctx)); // expr invalid
                return true;
            }
            return false;
        }
        case ast::ASTNode::Type::UnaryExpression: {
            auto& expr = static_cast<ast::UnaryExpression&>(*node); // NOLINT
            if (traverse(expr.getChild())) {
                node = std::make_unique<ast::Literal>(node->evaluate(ctx)); // expr invalid
                return true;
            }
            return false;
        }
        case ast::ASTNode::Type::Identifier: {
            auto& iden = static_cast<ast::Identifier&>(*node); // NOLINT
            return table.contains(iden.getId());
        }
        case ast::ASTNode::Type::Literal: {
            return true;
        }
        default:
            return false;
    }
}
//---------------------------------------------------------------------------
// Optimize AST: Pre-calculate const expression
void ConstPropOptimizer::optimize(std::unique_ptr<ast::Function>& fun) {
    if (fun) {
        // 1. Initialize a table
        // Use symTable from function node to insert constant in table
        for (auto it = fun->getSymTable().begin(); it != fun->getSymTable().end(); ++it) {
            if (fun->getSymTable().lookupConst(*it)) {
                table[*it] = fun->getSymTable().getConstValue(*it);
                ctx.setValue(*it, fun->getSymTable().getConstValue(*it));
            }
        }

        // 2. Iterate over all statements
        for (auto& stmt : fun->getStmts()) {
            traverse(stmt);
        }
    }
}
//---------------------------------------------------------------------------
} // namespace pljit::optimizer
//---------------------------------------------------------------------------