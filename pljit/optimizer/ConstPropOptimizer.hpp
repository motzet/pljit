#ifndef H_pljit_optimizer_ConstPropOptimizer
#define H_pljit_optimizer_ConstPropOptimizer
//---------------------------------------------------------------------------
#include "pljit/ast/EvaluationContext.hpp"
#include "pljit/optimizer/Optimizer.hpp"
#include <cstdint> // to use int64_t
#include <string>
#include <unordered_map>
//---------------------------------------------------------------------------
namespace pljit::optimizer {
//---------------------------------------------------------------------------
/// Optimizer pre-calculating const expressions
class ConstPropOptimizer : public Optimizer {
    /// ConstTable: contains only variables with const value
    std::unordered_map<std::string, int64_t> table;
    /// Evaluation context
    ast::EvaluationContext ctx;

    /// Traverse AST statement node in post-order (left, right, parent)
    void traverse(std::unique_ptr<ast::Statement>& node);
    /// Traverse AST expression node in post-order and return if const
    bool traverse(std::unique_ptr<ast::Expression>& node);

    public:
    /// Default constructor
    ConstPropOptimizer() = default;
    /// Destructor
    ~ConstPropOptimizer() override = default; // implicitly-declared destructor is virtual too

    /// Optimize AST: Pre-calculate const expression
    void optimize(std::unique_ptr<ast::Function>& fun) override;
};
//---------------------------------------------------------------------------
} // namespace pljit::optimizer
//---------------------------------------------------------------------------
#endif