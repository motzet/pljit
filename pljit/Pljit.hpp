#ifndef H_pljit_Pljit
#define H_pljit_Pljit
//---------------------------------------------------------------------------
#include "pljit/optimizer/ConstPropOptimizer.hpp"
#include "pljit/optimizer/DeadCodeOptimizer.hpp"
#include "pljit/parser/Parser.hpp"
#include "pljit/semantics/SemanticAnalyser.hpp"
#include <shared_mutex>
//---------------------------------------------------------------------------
namespace pljit {
//---------------------------------------------------------------------------
/// Top-level class for the JIT
class Pljit {
    /// Class for function resources
    struct FunctionResource {
        /// Store SourceCode
        code::SourceCode code;
        /// Store PT
        std::unique_ptr<pt::FunctionDefinition> pt;
        /// Store AST
        std::unique_ptr<ast::Function> ast;
        /// Store error
        bool error = false;
        /// Store optimized
        bool isOptimized = false;
        /// Mutex (naive, coarse-grained locking)
        mutable std::unique_ptr<std::shared_mutex> mutex = std::make_unique<std::shared_mutex>();

        /// Default constructor
        FunctionResource() = default;
        /// Constructor
        explicit FunctionResource(code::SourceCode&& code);
        /// Destructor
        ~FunctionResource() = default;

        /// Copy constructor
        FunctionResource(const FunctionResource& other) = delete;
        /// Copy assignment
        FunctionResource& operator=(const FunctionResource& other) = delete;
        /// Move constructor
        FunctionResource(FunctionResource&& other) noexcept = default;
        /// Move assignment
        FunctionResource& operator=(FunctionResource&& other) noexcept = default;
    };

    /// Store resources
    std::vector<FunctionResource> resources;

    public:
    /// Class for the function handle
    class FunctionHandle {
        public:
        /// Class for the evaluation result
        struct Result {
            int64_t val = 0;
            bool error = false;

            /// Default constructor
            Result() = default;

            /// Constructor
            Result(int64_t val, bool error);
        };

        private:
        /// Pointer to compiler
        Pljit* pljit = nullptr;
        /// Index in res
        std::size_t index = 0;

        /// Constructor
        FunctionHandle(Pljit* pljit, std::size_t index);

        /// Parse function
        void parse(std::ostream& out);
        /// Analyze function
        void analyse(std::ostream& out);
        /// Optimize function
        void optimize();

        friend Pljit;

        public:
        /// Default constructor
        FunctionHandle() = default;
        
        /// Call function with parameters (compile and optimize)
        Result operator()(const std::vector<int64_t>& params, std::ostream& out = std::cout);
    };

    public:
    /// Constructor
    Pljit() = default;

    /// Register function
    FunctionHandle registerFunction(std::string str);
};
//---------------------------------------------------------------------------
} // namespace pljit
//---------------------------------------------------------------------------
#endif