#include "pljit/parsetree/ParseTree.hpp"
#include "pljit/parsetree/PTVisitor.hpp"
#include <utility>
//---------------------------------------------------------------------------
namespace pljit::pt {
//---------------------------------------------------------------------------
using LocationRef = code::SourceCode::LocationRef;
using RangeRef = code::SourceCode::RangeRef;
//---------------------------------------------------------------------------
// Base class for the parse tree (PT) nodes (abstract)
//---------------------------------------------------------------------------
// Constructor with type
PTNode::PTNode(RangeRef ref, Type type) : ref(ref), type(type) {}
//---------------------------------------------------------------------------
// Return the type of the PT node as a member of the enum class Type
PTNode::Type PTNode::getType() const {
    return type;
}
//---------------------------------------------------------------------------
// Return RangeRef
RangeRef PTNode::getRef() const {
    return ref;
}
//---------------------------------------------------------------------------
// Return LocationRef begin
LocationRef PTNode::getBeginRef() const {
    return ref.getBegin();
}
//---------------------------------------------------------------------------
// Return LocationRef end
LocationRef PTNode::getEndRef() const {
    return ref.getEnd();
}
//---------------------------------------------------------------------------
// Generic terminal node (operator, keyword, separator)
//---------------------------------------------------------------------------
// Constructor
GenericTerminal::GenericTerminal(RangeRef ref) : PTNode(ref, Type::GenericTerminal) {}
//---------------------------------------------------------------------------
/// Accept function of the visitor pattern
void GenericTerminal::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for literal terminal symbol
//---------------------------------------------------------------------------
// Constructor
Literal::Literal(RangeRef ref) : PTNode(ref, Type::Literal) {}
//---------------------------------------------------------------------------
/// Accept function of the visitor pattern
void Literal::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for identifier terminal symbol
//---------------------------------------------------------------------------
// Constructor
Identifier::Identifier(RangeRef ref) : PTNode(ref, Type::Identifier) {}
//---------------------------------------------------------------------------
/// Accept function of the visitor pattern
void Identifier::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for non-terminal symbol with alternation
//---------------------------------------------------------------------------
// Constructor
AlternationNode::AlternationNode(RangeRef ref, Type type) : PTNode(ref, type) {}
//---------------------------------------------------------------------------
// Return const reference of children
const std::vector<std::unique_ptr<PTNode>>& AlternationNode::getChildren() const {
    return children;
}
//---------------------------------------------------------------------------
// Node for primary-expression non-terminal symbol
//---------------------------------------------------------------------------
// Constructor with identifier
PrimaryExpression::PrimaryExpression(RangeRef ref, std::unique_ptr<Identifier> identifier) : AlternationNode(ref, Type::PrimaryExpression), aType(AlternationType::Identifier) {
    children.push_back(std::move(identifier));
}
//---------------------------------------------------------------------------
// Constructor with literal
PrimaryExpression::PrimaryExpression(RangeRef ref, std::unique_ptr<Literal> literal) : AlternationNode(ref, Type::PrimaryExpression), aType(AlternationType::Literal) {
    children.push_back(std::move(literal));
}
//---------------------------------------------------------------------------
// Constructor with parenthesised additive-expression
PrimaryExpression::PrimaryExpression(RangeRef ref, std::unique_ptr<GenericTerminal> opening, std::unique_ptr<AdditiveExpression> expr, std::unique_ptr<GenericTerminal> closing) : AlternationNode(ref, Type::PrimaryExpression), aType(AlternationType::ParenthesisedAdditiveExpression) {
    children.push_back(std::move(opening));
    children.push_back(std::move(expr));
    children.push_back(std::move(closing));
}
//---------------------------------------------------------------------------
// Return alteration type
PrimaryExpression::AlternationType PrimaryExpression::getAType() const {
    return aType;
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void PrimaryExpression::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for arithmetic expression non-terminal symbol (abstract)
//---------------------------------------------------------------------------
ArithmeticNode::ArithmeticNode(RangeRef ref, Type type, std::unique_ptr<GenericTerminal> op, OpType opType) : PTNode(ref, type), op(std::move(op)), opType(opType) {}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Return pointer to operator
const GenericTerminal* ArithmeticNode::getOp() const {
    return op.get();
}
// Return operator type
ArithmeticNode::OpType ArithmeticNode::getOpType() const {
    return opType;
}
//---------------------------------------------------------------------------
// Node for unary-expression non-terminal symbol
//---------------------------------------------------------------------------
// Constructor
UnaryExpression::UnaryExpression(RangeRef ref, std::unique_ptr<GenericTerminal> op, std::unique_ptr<PrimaryExpression> primExpr, OpType opType) : ArithmeticNode(ref, Type::UnaryExpression, std::move(op), opType), primExpr(std::move(primExpr)) {}
//---------------------------------------------------------------------------
// Return pointer to primary-expression
const PrimaryExpression* UnaryExpression::getPrimExpr() const {
    return primExpr.get();
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void UnaryExpression::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for multiplicative-expression non-terminal symbol
//---------------------------------------------------------------------------
// Constructor
MultiplicativeExpression::MultiplicativeExpression(RangeRef ref, std::unique_ptr<UnaryExpression> unaryExpr, std::unique_ptr<GenericTerminal> op, std::unique_ptr<MultiplicativeExpression> mulExpr, OpType opType) : ArithmeticNode(ref, Type::MultiplicativeExpression, std::move(op), opType), unaryExpr(std::move(unaryExpr)), mulExpr(std::move(mulExpr)) {}
//---------------------------------------------------------------------------
// Return pointer to unary-expression
const UnaryExpression* MultiplicativeExpression::getUnaryExpr() const {
    return unaryExpr.get();
}
//---------------------------------------------------------------------------
// Return pointer to multiplicative-expression
const MultiplicativeExpression* MultiplicativeExpression::getMulExpr() const {
    return mulExpr.get();
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void MultiplicativeExpression::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for additive-expression non-terminal symbol
//---------------------------------------------------------------------------
// Constructor
AdditiveExpression::AdditiveExpression(RangeRef ref, std::unique_ptr<MultiplicativeExpression> mulExpr, std::unique_ptr<GenericTerminal> op, std::unique_ptr<AdditiveExpression> addExpr, OpType opType) : ArithmeticNode(ref, Type::AdditiveExpression, std::move(op), opType), mulExpr(std::move(mulExpr)), addExpr(std::move(addExpr)) {}
//---------------------------------------------------------------------------
// Return pointer to multiplicative-expression
const MultiplicativeExpression* AdditiveExpression::getMulExpr() const {
    return mulExpr.get();
}
//---------------------------------------------------------------------------
// Return pointer to additive-expression
const AdditiveExpression* AdditiveExpression::getAddExpr() const {
    return addExpr.get();
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void AdditiveExpression::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for assignment-expression non-terminal symbol
//---------------------------------------------------------------------------
// Constructor
AssignmentExpression::AssignmentExpression(RangeRef ref, std::unique_ptr<Identifier> identifier, std::unique_ptr<GenericTerminal> assign, std::unique_ptr<AdditiveExpression> addExpr) : PTNode(ref, Type::AssignmentExpression), identifier(std::move(identifier)), assign(std::move(assign)), addExpr(std::move(addExpr)) {}
//---------------------------------------------------------------------------
// Return pointer to identifier
const Identifier* AssignmentExpression::getIden() const {
    return identifier.get();
}
//---------------------------------------------------------------------------
// Return pointer to equal
const GenericTerminal* AssignmentExpression::getAssign() const {
    return assign.get();
}
//---------------------------------------------------------------------------
// Return pointer to additive-expression
const AdditiveExpression* AssignmentExpression::getAddExpr() const {
    return addExpr.get();
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void AssignmentExpression::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for statement non-terminal symbol
//---------------------------------------------------------------------------
// Constructor with assignment-expression
Statement::Statement(RangeRef ref, std::unique_ptr<AssignmentExpression> expr) : AlternationNode(ref, Type::Statement), aType(AlternationType::AssignmentExpr) {
    children.push_back(std::move(expr));
}
//---------------------------------------------------------------------------
// Constructor with returned additive-expression
Statement::Statement(RangeRef ref, std::unique_ptr<GenericTerminal> returnKeyword, std::unique_ptr<AdditiveExpression> expr) : AlternationNode(ref, Type::Statement), aType(AlternationType::RETURNExpr) {
    children.push_back(std::move(returnKeyword));
    children.push_back(std::move(expr));
}
//---------------------------------------------------------------------------
// Return alternation type
Statement::AlternationType Statement::getAType() const {
    return aType;
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void Statement::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for statement-list non-terminal symbol
//---------------------------------------------------------------------------
// Constructor
StatementList::StatementList(RangeRef ref, std::vector<std::unique_ptr<Statement>> stmts, std::vector<std::unique_ptr<GenericTerminal>> semicolons) : PTNode(ref, Type::StatementList), stmts(std::move(stmts)), semicolons(std::move(semicolons)) {}
//---------------------------------------------------------------------------
// Return const reference of stmts
const std::vector<std::unique_ptr<Statement>>& StatementList::getStmts() const {
    return stmts;
}
//---------------------------------------------------------------------------
// Return const reference to semicolons
const std::vector<std::unique_ptr<GenericTerminal>>& StatementList::getSemicolons() const {
    return semicolons;
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void StatementList::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for compound-statement non-terminal symbol
//---------------------------------------------------------------------------
// Constructor
CompoundStatement::CompoundStatement(RangeRef ref, std::unique_ptr<GenericTerminal> beginKeyword, std::unique_ptr<StatementList> stmtList, std::unique_ptr<GenericTerminal> endKeyword) : PTNode(ref, Type::CompoundStatement), beginKeyword(std::move(beginKeyword)), stmtList(std::move(stmtList)), endKeyword(std::move(endKeyword)) {}
//---------------------------------------------------------------------------
// Return pointer to begin
const GenericTerminal* CompoundStatement::getBeginKeyword() const {
    return beginKeyword.get();
}
//---------------------------------------------------------------------------
// Return pointer to statement-list
const StatementList* CompoundStatement::getStmtList() const {
    return stmtList.get();
}
//---------------------------------------------------------------------------
// Return pointer to end
const GenericTerminal* CompoundStatement::getEndKeyword() const {
    return endKeyword.get();
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void CompoundStatement::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for init-declarator non-terminal symbol
//---------------------------------------------------------------------------
// Constructor
InitDeclarator::InitDeclarator(RangeRef ref, std::unique_ptr<Identifier> identifier, std::unique_ptr<GenericTerminal> equal, std::unique_ptr<Literal> literal) : PTNode(ref, Type::InitDeclarator), identifier(std::move(identifier)), equal(std::move(equal)), literal(std::move(literal)) {}
//---------------------------------------------------------------------------
// Return pointer to identifier
const Identifier* InitDeclarator::getIden() const {
    return identifier.get();
}
//---------------------------------------------------------------------------
// Return pointer to equal
const GenericTerminal* InitDeclarator::getEqual() const {
    return equal.get();
}
//---------------------------------------------------------------------------
// Return pointer to literal
const Literal* InitDeclarator::getLiteral() const {
    return literal.get();
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void InitDeclarator::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for init-declarator-list non-terminal symbol
//---------------------------------------------------------------------------
/// Constructor
InitDeclaratorList::InitDeclaratorList(RangeRef ref, std::vector<std::unique_ptr<InitDeclarator>> initDecls, std::vector<std::unique_ptr<GenericTerminal>> commas) : PTNode(ref, Type::InitDeclaratorList), initDecls(std::move(initDecls)), commas(std::move(commas)) {}
//---------------------------------------------------------------------------
// Return const reference of initDelcs
const std::vector<std::unique_ptr<InitDeclarator>>& InitDeclaratorList::getInitDecls() const {
    return initDecls;
}
//---------------------------------------------------------------------------
// Return const reference to commas
const std::vector<std::unique_ptr<GenericTerminal>>& InitDeclaratorList::getCommas() const {
    return commas;
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void InitDeclaratorList::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for declarator-list non-terminal symbol
//---------------------------------------------------------------------------
// Constructor
DeclaratorList::DeclaratorList(RangeRef ref, std::vector<std::unique_ptr<Identifier>> identifiers, std::vector<std::unique_ptr<GenericTerminal>> commas) : PTNode(ref, Type::DeclaratorList), identifiers(std::move(identifiers)), commas(std::move(commas)) {}
//---------------------------------------------------------------------------
// Return const reference of identifiers
const std::vector<std::unique_ptr<Identifier>>& DeclaratorList::getIdens() const {
    return identifiers;
}
//---------------------------------------------------------------------------
// Return const reference of commas
const std::vector<std::unique_ptr<GenericTerminal>>& DeclaratorList::getCommas() const {
    return commas;
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void DeclaratorList::accept(PTVisitor& visitor) const {
    return visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for constant-declarations non-terminal symbol
//---------------------------------------------------------------------------
// Constructor
ConstantDeclarations::ConstantDeclarations(RangeRef ref, std::unique_ptr<GenericTerminal> keyword, std::unique_ptr<InitDeclaratorList> initDeclList, std::unique_ptr<GenericTerminal> semicolon) : PTNode(ref, Type::ConstantDeclarations), keyword(std::move(keyword)), initDeclList(std::move(initDeclList)), semicolon(std::move(semicolon)) {}
//---------------------------------------------------------------------------
// Return pointer to keyword
const GenericTerminal* ConstantDeclarations::getKeyword() const {
    return keyword.get();
}
//---------------------------------------------------------------------------
// Return pointer to init-declarator-list
const InitDeclaratorList* ConstantDeclarations::getInitDeclList() const {
    return initDeclList.get();
}
//---------------------------------------------------------------------------
// Return pointer to semicolon
const GenericTerminal* ConstantDeclarations::getSemicolon() const {
    return semicolon.get();
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void ConstantDeclarations::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for declarations (parameter and variable) non-terminal symbol (abstract)
//---------------------------------------------------------------------------
Declarations::Declarations(RangeRef ref, std::unique_ptr<GenericTerminal> keyword, std::unique_ptr<DeclaratorList> declList, std::unique_ptr<GenericTerminal> semicolon, Type type) : PTNode(ref, type), keyword(std::move(keyword)), declList(std::move(declList)), semicolon(std::move(semicolon)) {}
//---------------------------------------------------------------------------
// Return pointer to keyword
const GenericTerminal* Declarations::getKeyword() const {
    return keyword.get();
}
//---------------------------------------------------------------------------
// Return pointer to declarator-list
const DeclaratorList* Declarations::getDeclList() const {
    return declList.get();
}
//---------------------------------------------------------------------------
// Return pointer to semicolon
const GenericTerminal* Declarations::getSemicolon() const {
    return semicolon.get();
}
//---------------------------------------------------------------------------
// Node for variable-declarations non-terminal symbol
//---------------------------------------------------------------------------
// Constructor
VariableDeclarations::VariableDeclarations(RangeRef ref, std::unique_ptr<GenericTerminal> keyword, std::unique_ptr<DeclaratorList> declList, std::unique_ptr<GenericTerminal> semicolon) : Declarations(ref, std::move(keyword), std::move(declList), std::move(semicolon), Type::VariableDeclarations) {}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void VariableDeclarations::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for parameter-declarations non-terminal symbol
//---------------------------------------------------------------------------
// Constructor
ParameterDeclarations::ParameterDeclarations(RangeRef ref, std::unique_ptr<GenericTerminal> keyword, std::unique_ptr<DeclaratorList> declList, std::unique_ptr<GenericTerminal> semicolon) : Declarations(ref, std::move(keyword), std::move(declList), std::move(semicolon), Type::ParameterDeclarations) {}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void ParameterDeclarations::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
// Node for function-definition non-terminal symbol (Root node)
//---------------------------------------------------------------------------
// Constructor
FunctionDefinition::FunctionDefinition(RangeRef ref, std::unique_ptr<ParameterDeclarations> paramDecls, std::unique_ptr<VariableDeclarations> varDecls, std::unique_ptr<ConstantDeclarations> constDecls, std::unique_ptr<CompoundStatement> compStmt, std::unique_ptr<GenericTerminal> dot) : PTNode(ref, Type::FunctionDefinition), paramDecls(std::move(paramDecls)), varDecls(std::move(varDecls)), constDecls(std::move(constDecls)), compStmt(std::move(compStmt)), dot(std::move(dot)) {}
//---------------------------------------------------------------------------
// Return pointer to parameter-declarations
const ParameterDeclarations* FunctionDefinition::getParamDecls() const {
    return paramDecls.get();
}
//---------------------------------------------------------------------------
// Return pointer to variable-declarations
const VariableDeclarations* FunctionDefinition::getVarDecls() const {
    return varDecls.get();
}
//---------------------------------------------------------------------------
// Return pointer to constant-declarations
const ConstantDeclarations* FunctionDefinition::getConstDecls() const {
    return constDecls.get();
}
//---------------------------------------------------------------------------
// Return pointer to compound-statement
const CompoundStatement* FunctionDefinition::getCompStmt() const {
    return compStmt.get();
}
//---------------------------------------------------------------------------
// Return pointer to dot
const GenericTerminal* FunctionDefinition::getDot() const {
    return dot.get();
}
//---------------------------------------------------------------------------
// Accept function of the visitor pattern
void FunctionDefinition::accept(PTVisitor& visitor) const {
    visitor.visit(*this);
}
//---------------------------------------------------------------------------
} // namespace pljit::pt
//---------------------------------------------------------------------------