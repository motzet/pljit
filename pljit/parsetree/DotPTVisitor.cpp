#include "pljit/parsetree/DotPTVisitor.hpp"
#include <cassert>
//---------------------------------------------------------------------------
namespace pljit::pt {
//---------------------------------------------------------------------------
// Class DotPTVisitor: Implementation of the PTVisitor
//---------------------------------------------------------------------------
// Constructor with std::ostream
DotPTVisitor::DotPTVisitor(std::ostream& out) : out(out) {}
//---------------------------------------------------------------------------
// Visit function for GenericTerminal
void DotPTVisitor::visit(const GenericTerminal& node) {
    out << "\t" << index << R"( [label="\")" << node.getRef().string_view() << R"(\""];)" << std::endl;
}
//---------------------------------------------------------------------------
// Visit function for Literal
void DotPTVisitor::visit(const Literal& node) {
    out << "\t" << index << " [label=\"" << node.getRef().string_view() << "\"];" << std::endl;
}
//---------------------------------------------------------------------------
// Visit function for Identifier
void DotPTVisitor::visit(const Identifier& node) {
    out << "\t" << index << R"( [label="\")" << node.getRef().string_view() << R"(\""];)" << std::endl;
}
//---------------------------------------------------------------------------
// Visit function for PrimaryExpression
void DotPTVisitor::visit(const PrimaryExpression& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"primary-expression\"];" << std::endl;

    if (node.getAType() == PrimaryExpression::AlternationType::Identifier) {
        assert(node.getChildren().size() == 1);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getChildren()[0]->accept(*this);
    } else if (node.getAType() == PrimaryExpression::AlternationType::Literal) {
        assert(node.getChildren().size() == 1);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getChildren()[0]->accept(*this);
    } else {
        assert(node.getChildren().size() == 3);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getChildren()[0]->accept(*this);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getChildren()[1]->accept(*this);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getChildren()[2]->accept(*this);
    }
}
//---------------------------------------------------------------------------
// Visit function for UnaryExpression
void DotPTVisitor::visit(const UnaryExpression& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"unary-expression\"];" << std::endl;

    if (node.getOp()) {
        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getOp()->accept(*this);
    }

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getPrimExpr()->accept(*this);
}
//---------------------------------------------------------------------------
// Visit function for MultiplicativeExpression
void DotPTVisitor::visit(const MultiplicativeExpression& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"multiplicative-expression\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getUnaryExpr()->accept(*this);

    if (node.getMulExpr()) {
        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getOp()->accept(*this);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getMulExpr()->accept(*this);
    }
}
//---------------------------------------------------------------------------
// Visit function for AdditiveExpression
void DotPTVisitor::visit(const AdditiveExpression& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"additive-expression\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getMulExpr()->accept(*this);

    if (node.getAddExpr()) {
        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getOp()->accept(*this);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getAddExpr()->accept(*this);
    }
}
//---------------------------------------------------------------------------
// Visit function for AssignmentExpression
void DotPTVisitor::visit(const AssignmentExpression& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"assignment-expression\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getIden()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getAssign()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getAddExpr()->accept(*this);
}
//---------------------------------------------------------------------------
// Visit function for Statement
void DotPTVisitor::visit(const Statement& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"statement\"];" << std::endl;

    if (node.getAType() == Statement::AlternationType::AssignmentExpr) {
        assert(node.getChildren().size() == 1);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getChildren()[0]->accept(*this);
    } else {
        assert(node.getChildren().size() == 2);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getChildren()[0]->accept(*this);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getChildren()[1]->accept(*this);
    }
}
//---------------------------------------------------------------------------
// Visit function for StatementList
void DotPTVisitor::visit(const StatementList& node) {
    assert(node.getStmts().size() == node.getSemicolons().size() + 1);

    std::size_t id = index;
    out << "\t" << id << " [label=\"statement-list\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getStmts()[0].get()->accept(*this);

    for (std::size_t i = 0; i < node.getSemicolons().size(); ++i) {
        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getSemicolons()[i].get()->accept(*this);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getStmts()[i + 1].get()->accept(*this);
    }
}
//---------------------------------------------------------------------------
// Visit function for InitDeclarator
void DotPTVisitor::visit(const InitDeclarator& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"init-declarator\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getIden()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getEqual()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getLiteral()->accept(*this);
}
//---------------------------------------------------------------------------
// Visit function for InitDeclaratorList
void DotPTVisitor::visit(const InitDeclaratorList& node) {
    assert(node.getInitDecls().size() == node.getCommas().size() + 1);

    std::size_t id = index;
    out << "\t" << id << " [label=\"init-declarator-list\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getInitDecls()[0].get()->accept(*this);

    for (std::size_t i = 0; i < node.getCommas().size(); ++i) {
        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getCommas()[i].get()->accept(*this);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getInitDecls()[i + 1].get()->accept(*this);
    }
}
//---------------------------------------------------------------------------
// Visit function for DeclaratorList
void DotPTVisitor::visit(const DeclaratorList& node) {
    assert(node.getIdens().size() == node.getCommas().size() + 1);

    std::size_t id = index;
    out << "\t" << id << " [label=\"declarator-list\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getIdens()[0].get()->accept(*this);

    for (std::size_t i = 0; i < node.getCommas().size(); ++i) {
        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getCommas()[i].get()->accept(*this);

        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getIdens()[i + 1].get()->accept(*this);
    }
}
//---------------------------------------------------------------------------
// Visit function for CompoundStatement
void DotPTVisitor::visit(const CompoundStatement& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"compound-statement\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getBeginKeyword()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getStmtList()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getEndKeyword()->accept(*this);
}
//---------------------------------------------------------------------------
// Visit function for ConstantDeclarations
void DotPTVisitor::visit(const ConstantDeclarations& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"constant-declarations\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getKeyword()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getInitDeclList()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getSemicolon()->accept(*this);
}
//---------------------------------------------------------------------------
// Visit function for VariableDeclarations
void DotPTVisitor::visit(const VariableDeclarations& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"variable-declarations\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getKeyword()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getDeclList()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getSemicolon()->accept(*this);
}
//---------------------------------------------------------------------------
// Visit function for ParameterDeclarations
void DotPTVisitor::visit(const ParameterDeclarations& node) {
    std::size_t id = index;
    out << "\t" << id << " [label=\"parameter-declarations\"];" << std::endl;

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getKeyword()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getDeclList()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getSemicolon()->accept(*this);
}
//---------------------------------------------------------------------------
// Visit function for FunctionDefinition
void DotPTVisitor::visit(const FunctionDefinition& node) {
    std::size_t id = index;

    // Begin DOT format
    out << "digraph {" << std::endl;
    out << "\t" << id << " [label=\"function-definition\"];" << std::endl;

    if (node.getParamDecls()) {
        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getParamDecls()->accept(*this);
    }

    if (node.getVarDecls()) {
        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getVarDecls()->accept(*this);
    }

    if (node.getConstDecls()) {
        out << "\t" << id << " -> " << ++index << ";" << std::endl;
        node.getConstDecls()->accept(*this);
    }

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getCompStmt()->accept(*this);

    out << "\t" << id << " -> " << ++index << ";" << std::endl;
    node.getDot()->accept(*this);

    out << "}" << std::endl;
    // End DOT format
}
//---------------------------------------------------------------------------
} // namespace pljit::pt
//---------------------------------------------------------------------------
