#ifndef H_pljit_parsetree_DotPTVisitor
#define H_pljit_parsetree_DotPTVisitor
//---------------------------------------------------------------------------
#include "pljit/parsetree/PTVisitor.hpp"
#include <cstddef>
#include <iostream>
#include <ostream>
//---------------------------------------------------------------------------
namespace pljit::pt {
//---------------------------------------------------------------------------
/// Class DotPTVisitor: Implementation of the PTVisitor
/// Printing the parse tree in DOT format
class DotPTVisitor : public PTVisitor {
    /// Output stream
    std::ostream& out = std::cout;
    /// Index for DOT node
    std::size_t index = 0;

    public:
    /// Default constructor
    DotPTVisitor() = default;
    /// Constructor with std::ostream
    explicit DotPTVisitor(std::ostream& out);

    /// Destructor
    ~DotPTVisitor() override = default; // implicitly-declared destructor is virtual too

    /// Visit function for GenericTerminal
    void visit(const GenericTerminal& node) override;
    /// Visit function for Literal
    void visit(const Literal& node) override;
    /// Visit function for Identifier
    void visit(const Identifier& node) override;

    /// Visit function for PrimaryExpression
    void visit(const PrimaryExpression& node) override;
    /// Visit function for UnaryExpression
    void visit(const UnaryExpression& node) override;
    /// Visit function for MultiplicativeExpression
    void visit(const MultiplicativeExpression& node) override;
    /// Visit function for AdditiveExpression
    void visit(const AdditiveExpression& node) override;
    /// Visit function for AssignmentExpression
    void visit(const AssignmentExpression& node) override;
    /// Visit function for Statement
    void visit(const Statement& node) override;
    /// Visit function for StatementList
    void visit(const StatementList& node) override;
    /// Visit function for InitDeclarator
    void visit(const InitDeclarator& node) override;
    /// Visit function for InitDeclaratorList
    void visit(const InitDeclaratorList& node) override;
    /// Visit function for DeclaratorList
    void visit(const DeclaratorList& node) override;

    /// Visit function for CompoundStatement
    void visit(const CompoundStatement& node) override;
    /// Visit function for ConstantDeclarations
    void visit(const ConstantDeclarations& node) override;
    /// Visit function for VariableDeclarations
    void visit(const VariableDeclarations& node) override;
    /// Visit function for ParameterDeclarations
    void visit(const ParameterDeclarations& node) override;
    /// Visit function for FunctionDefinition
    void visit(const FunctionDefinition& node) override;
};
//---------------------------------------------------------------------------
} // namespace pljit::pt
//---------------------------------------------------------------------------
#endif
