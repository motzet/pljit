#ifndef H_pljit_parsetree_PTVisitor
#define H_pljit_parsetree_PTVisitor
//---------------------------------------------------------------------------
#include "pljit/parsetree/ParseTree.hpp"
//---------------------------------------------------------------------------
namespace pljit::pt {
//---------------------------------------------------------------------------
/// Interface for visitor pattern for the parse tree (abstract class)
class PTVisitor {
    public:
    /// Constructor (see C.126)

    /// Destructor
    virtual ~PTVisitor() = default;

    /// Visit function for GenericTerminal (pure virtual)
    virtual void visit(const GenericTerminal& node) = 0;
    /// Visit function for Literal (pure virtual)
    virtual void visit(const Literal& node) = 0;
    /// Visit function for Identifier (pure virtual)
    virtual void visit(const Identifier& node) = 0;

    /// Visit function for PrimaryExpression (pure virtual)
    virtual void visit(const PrimaryExpression& node) = 0;
    /// Visit function for UnaryExpression (pure virtual)
    virtual void visit(const UnaryExpression& node) = 0;
    /// Visit function for MultiplicativeExpression (pure virtual)
    virtual void visit(const MultiplicativeExpression& node) = 0;
    /// Visit function for AdditiveExpression (pure virtual)
    virtual void visit(const AdditiveExpression& node) = 0;
    /// Visit function for AssignmentExpression (pure virtual)
    virtual void visit(const AssignmentExpression& node) = 0;
    /// Visit function for Statement (pure virtual)
    virtual void visit(const Statement& node) = 0;
    /// Visit function for StatementList (pure virtual)
    virtual void visit(const StatementList& node) = 0;

    /// Visit function for InitDeclarator (pure virtual)
    virtual void visit(const InitDeclarator& node) = 0;
    /// Visit function for InitDeclaratorList (pure virtual)
    virtual void visit(const InitDeclaratorList& node) = 0;
    /// Visit function for DeclaratorList (pure virtual)
    virtual void visit(const DeclaratorList& node) = 0;

    /// Visit function for CompoundStatement (pure virtual)
    virtual void visit(const CompoundStatement& node) = 0;
    /// Visit function for ConstantDeclarations (pure virtual)
    virtual void visit(const ConstantDeclarations& node) = 0;
    /// Visit function for VariableDeclarations (pure virtual)
    virtual void visit(const VariableDeclarations& node) = 0;
    /// Visit function for ParameterDeclarations (pure virtual)
    virtual void visit(const ParameterDeclarations& node) = 0;
    /// Visit function for FunctionDefinition (pure virtual)
    virtual void visit(const FunctionDefinition& node) = 0;
};
//---------------------------------------------------------------------------
} // namespace pljit::pt
//---------------------------------------------------------------------------
#endif
