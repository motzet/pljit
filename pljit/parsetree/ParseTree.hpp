#ifndef H_pljit_parsetree_ParseTree
#define H_pljit_parsetree_ParseTree
//---------------------------------------------------------------------------
#include "pljit/code/SourceCode.hpp"
#include <memory>
#include <vector>
//---------------------------------------------------------------------------
namespace pljit::pt {
//---------------------------------------------------------------------------
/// Parse tree visitor base class (forward declared)
class PTVisitor;
//---------------------------------------------------------------------------
/// Base class for the parse tree (PT) nodes (abstract)
class PTNode {
    public:
    /// All possible types of PTNode
    enum class Type {
        FunctionDefinition,
        ParameterDeclarations,
        VariableDeclarations,
        ConstantDeclarations,
        CompoundStatement,

        DeclaratorList,
        InitDeclaratorList,
        InitDeclarator,

        StatementList,
        Statement,
        AssignmentExpression,
        AdditiveExpression,
        MultiplicativeExpression,
        UnaryExpression,
        PrimaryExpression,

        GenericTerminal,
        Literal,
        Identifier,
    };

    private:
    /// Location reference to original source code
    code::SourceCode::RangeRef ref;
    /// Type of PTNode
    Type type;

    protected:
    /// Constructor with type
    PTNode(code::SourceCode::RangeRef ref, Type type);

    public:
    /// Destructor
    virtual ~PTNode() = default;

    /// Copy constructor (see C.67)
    PTNode(const PTNode&) = delete;
    /// Copy assignment (see C.67)
    PTNode& operator=(const PTNode&) = delete;

    /// Return the type of the PT node as a member of the enum class Type
    Type getType() const;
    /// Return RangeRef
    code::SourceCode::RangeRef getRef() const;
    /// Return LocationRef begin
    code::SourceCode::LocationRef getBeginRef() const;
    /// Return LocationRef end
    code::SourceCode::LocationRef getEndRef() const;

    /// Accept function of the visitor pattern (pure virtual)
    virtual void accept(PTVisitor& visitor) const = 0;
};
//---------------------------------------------------------------------------
/// Generic terminal node (operator, keyword, separator)
class GenericTerminal : public PTNode {
    public:
    /// Constructor
    explicit GenericTerminal(code::SourceCode::RangeRef ref);

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for literal terminal symbol
class Literal : public PTNode {
    public:
    /// Constructor
    explicit Literal(code::SourceCode::RangeRef ref);

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for identifier terminal symbol
class Identifier : public PTNode {
    public:
    /// Constructor
    explicit Identifier(code::SourceCode::RangeRef ref);

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for additive-expression non-terminal symbol (forward declared)
class AdditiveExpression;
//---------------------------------------------------------------------------
/// Node for non-terminal symbol with alternation (abstract)
class AlternationNode : public PTNode {
    protected:
    std::vector<std::unique_ptr<PTNode>> children;

    /// Constructor
    AlternationNode(code::SourceCode::RangeRef ref, Type type);

    public:
    /// Destructor
    ~AlternationNode() override = default;

    /// Return const reference of children
    const std::vector<std::unique_ptr<PTNode>>& getChildren() const;
};
//---------------------------------------------------------------------------
/// Node for primary-expression non-terminal symbol
/// primary-expression = identifier | literal | "(" additive-expression ")".
class PrimaryExpression : public AlternationNode {
    public:
    /// Possible alternation types
    enum class AlternationType {
        Identifier,
        Literal,
        ParenthesisedAdditiveExpression
    };

    private:
    /// AlternationType of PrimaryExpression
    AlternationType aType;

    public:
    /// Constructor with identifier
    PrimaryExpression(code::SourceCode::RangeRef ref, std::unique_ptr<Identifier> identifier);
    /// Constructor with literal
    PrimaryExpression(code::SourceCode::RangeRef ref, std::unique_ptr<Literal> literal);
    /// Constructor with parenthesised additive-expression
    PrimaryExpression(code::SourceCode::RangeRef ref, std::unique_ptr<GenericTerminal> opening, std::unique_ptr<AdditiveExpression> expr, std::unique_ptr<GenericTerminal> closing);

    /// Return alteration type
    AlternationType getAType() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for arithmetic expression non-terminal symbol (abstract)
class ArithmeticNode : public PTNode {
    public:
    /// Possible operator types
    enum class OpType {
        None,
        Add,
        Sub,
        Mul,
        Div
    };

    private:
    std::unique_ptr<GenericTerminal> op;
    /// Operator type
    OpType opType;

    protected:
    /// Constructor
    ArithmeticNode(code::SourceCode::RangeRef ref, Type type, std::unique_ptr<GenericTerminal> op, OpType opType);

    public:
    /// Destructor
    ~ArithmeticNode() override = default;

    /// Return pointer to operator
    const GenericTerminal* getOp() const;
    /// Return operator type
    OpType getOpType() const;
};
//---------------------------------------------------------------------------
/// Node for unary-expression non-terminal symbol
/// unary-expression = [ "+" | "-" ] primary-expression.
class UnaryExpression : public ArithmeticNode {
    std::unique_ptr<PrimaryExpression> primExpr;

    public:
    /// Constructor
    UnaryExpression(code::SourceCode::RangeRef ref, std::unique_ptr<GenericTerminal> op, std::unique_ptr<PrimaryExpression> primExpr, OpType opType);

    /// Return pointer to primary-expression
    const PrimaryExpression* getPrimExpr() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for multiplicative-expression non-terminal symbol
/// multiplicative-expression = unary-expression [ ( "*" | "/" ) multiplicative-expression ].
class MultiplicativeExpression : public ArithmeticNode {
    std::unique_ptr<UnaryExpression> unaryExpr;
    std::unique_ptr<MultiplicativeExpression> mulExpr;

    public:
    /// Constructor
    MultiplicativeExpression(code::SourceCode::RangeRef ref, std::unique_ptr<UnaryExpression> unaryExpr, std::unique_ptr<GenericTerminal> op, std::unique_ptr<MultiplicativeExpression> mulExpr, OpType opType);

    /// Return pointer to unary-expression
    const UnaryExpression* getUnaryExpr() const;
    /// Return pointer to multiplicative-expression
    const MultiplicativeExpression* getMulExpr() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for additive-expression non-terminal symbol
/// additive-expression = multiplicative-expression [ ( "+" | "-" ) additive-expression ].
class AdditiveExpression : public ArithmeticNode {
    std::unique_ptr<MultiplicativeExpression> mulExpr;
    std::unique_ptr<AdditiveExpression> addExpr;

    public:
    /// Constructor
    AdditiveExpression(code::SourceCode::RangeRef ref, std::unique_ptr<MultiplicativeExpression> mulExpr, std::unique_ptr<GenericTerminal> op, std::unique_ptr<AdditiveExpression> addExpr, OpType opType);

    /// Return pointer to multiplicative-expression
    const MultiplicativeExpression* getMulExpr() const;
    /// Return pointer to additive-expression
    const AdditiveExpression* getAddExpr() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for assignment-expression non-terminal symbol
/// assignment-expression = identifier ":=" additive-expression.
class AssignmentExpression : public PTNode {
    std::unique_ptr<Identifier> identifier;
    std::unique_ptr<GenericTerminal> assign;
    std::unique_ptr<AdditiveExpression> addExpr;

    public:
    /// Constructor
    AssignmentExpression(code::SourceCode::RangeRef ref, std::unique_ptr<Identifier> identifier, std::unique_ptr<GenericTerminal> assign, std::unique_ptr<AdditiveExpression> addExpr);

    /// Return pointer to identifier
    const Identifier* getIden() const;
    /// Return pointer to assign
    const GenericTerminal* getAssign() const;
    /// Return pointer to additive-expression
    const AdditiveExpression* getAddExpr() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for statement non-terminal symbol
/// statement = assignment-expression | "RETURN" additive-expression.
class Statement : public AlternationNode {
    public:
    /// Possible alternation types
    enum class AlternationType {
        AssignmentExpr,
        RETURNExpr
    };

    private:
    /// AlternationType of Statement
    AlternationType aType;

    public:
    /// Constructor with assignment-expression
    Statement(code::SourceCode::RangeRef ref, std::unique_ptr<AssignmentExpression> expr);
    /// Constructor with returned additive-expression
    Statement(code::SourceCode::RangeRef ref, std::unique_ptr<GenericTerminal> returnKeyword, std::unique_ptr<AdditiveExpression> expr);

    /// Return alternation type
    AlternationType getAType() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for statement-list non-terminal symbol
/// statement-list = statement { ";" statement }.
class StatementList : public PTNode {
    std::vector<std::unique_ptr<Statement>> stmts;
    std::vector<std::unique_ptr<GenericTerminal>> semicolons;

    public:
    /// Constructor
    StatementList(code::SourceCode::RangeRef ref, std::vector<std::unique_ptr<Statement>> stmts, std::vector<std::unique_ptr<GenericTerminal>> semicolons);

    /// Return const reference of stmts
    const std::vector<std::unique_ptr<Statement>>& getStmts() const;
    /// Return const reference to semicolons
    const std::vector<std::unique_ptr<GenericTerminal>>& getSemicolons() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for compound-statement non-terminal symbol
/// compound-statement = "BEGIN" statement-list "END".
class CompoundStatement : public PTNode {
    std::unique_ptr<GenericTerminal> beginKeyword;
    std::unique_ptr<StatementList> stmtList;
    std::unique_ptr<GenericTerminal> endKeyword;

    public:
    /// Constructor
    CompoundStatement(code::SourceCode::RangeRef ref, std::unique_ptr<GenericTerminal> beginKeyword, std::unique_ptr<StatementList> stmtList, std::unique_ptr<GenericTerminal> endKeyword);

    /// Return pointer to begin
    const GenericTerminal* getBeginKeyword() const;
    /// Return pointer to statement-list
    const StatementList* getStmtList() const;
    /// Return pointer to end
    const GenericTerminal* getEndKeyword() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for init-declarator non-terminal symbol
/// init-declarator = identifier "=" literal.
class InitDeclarator : public PTNode {
    std::unique_ptr<Identifier> identifier;
    std::unique_ptr<GenericTerminal> equal;
    std::unique_ptr<Literal> literal;

    public:
    /// Constructor
    InitDeclarator(code::SourceCode::RangeRef ref, std::unique_ptr<Identifier> identifier, std::unique_ptr<GenericTerminal> equal, std::unique_ptr<Literal> literal);

    /// Return pointer to identifier
    const Identifier* getIden() const;
    /// Return pointer to equal
    const GenericTerminal* getEqual() const;
    /// Return pointer to literal
    const Literal* getLiteral() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for init-declarator-list non-terminal symbol
/// init-declarator-list = init-declarator { "," init-declarator }.
class InitDeclaratorList : public PTNode {
    std::vector<std::unique_ptr<InitDeclarator>> initDecls;
    std::vector<std::unique_ptr<GenericTerminal>> commas;

    public:
    /// Constructor
    InitDeclaratorList(code::SourceCode::RangeRef ref, std::vector<std::unique_ptr<InitDeclarator>> initDecls, std::vector<std::unique_ptr<GenericTerminal>> commas);

    /// Return const reference of initDelcs
    const std::vector<std::unique_ptr<InitDeclarator>>& getInitDecls() const;
    /// Return const reference to commas
    const std::vector<std::unique_ptr<GenericTerminal>>& getCommas() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for declarator-list non-terminal symbol
/// declarator-list = identifier { "," identifier }.
class DeclaratorList : public PTNode {
    std::vector<std::unique_ptr<Identifier>> identifiers;
    std::vector<std::unique_ptr<GenericTerminal>> commas;

    public:
    /// Constructor
    DeclaratorList(code::SourceCode::RangeRef ref, std::vector<std::unique_ptr<Identifier>> identifiers, std::vector<std::unique_ptr<GenericTerminal>> commas);

    /// Return const reference of identifiers
    const std::vector<std::unique_ptr<Identifier>>& getIdens() const;
    /// Return const reference of commas
    const std::vector<std::unique_ptr<GenericTerminal>>& getCommas() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for constant-declarations non-terminal symbol
/// constant-declarations = "CONST" init-declarator-list ";".
class ConstantDeclarations : public PTNode {
    std::unique_ptr<GenericTerminal> keyword;
    std::unique_ptr<InitDeclaratorList> initDeclList;
    std::unique_ptr<GenericTerminal> semicolon;

    public:
    /// Constructor
    ConstantDeclarations(code::SourceCode::RangeRef ref, std::unique_ptr<GenericTerminal> keyword, std::unique_ptr<InitDeclaratorList> initDeclList, std::unique_ptr<GenericTerminal> semicolon);

    /// Return pointer to keyword
    const GenericTerminal* getKeyword() const;
    /// Return pointer to init-declarator-list
    const InitDeclaratorList* getInitDeclList() const;
    /// Return pointer to semicolon
    const GenericTerminal* getSemicolon() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for declarations (parameter and variable) non-terminal symbol (abstract)
class Declarations : public PTNode {
    std::unique_ptr<GenericTerminal> keyword;
    std::unique_ptr<DeclaratorList> declList;
    std::unique_ptr<GenericTerminal> semicolon;

    protected:
    /// Constructor
    Declarations(code::SourceCode::RangeRef ref, std::unique_ptr<GenericTerminal> keyword, std::unique_ptr<DeclaratorList> declList, std::unique_ptr<GenericTerminal> semicolon, Type type);

    public:
    /// Destructor
    ~Declarations() override = default;

    /// Return pointer to keyword
    const GenericTerminal* getKeyword() const;
    /// Return pointer to declarator-list
    const DeclaratorList* getDeclList() const;
    /// Return pointer to semicolon
    const GenericTerminal* getSemicolon() const;
};
//---------------------------------------------------------------------------
/// Node for variable-declarations non-terminal symbol
/// variable-declarations = "VAR" declarator-list ";".
class VariableDeclarations : public Declarations {
    public:
    /// Constructor
    VariableDeclarations(code::SourceCode::RangeRef ref, std::unique_ptr<GenericTerminal> keyword, std::unique_ptr<DeclaratorList> declList, std::unique_ptr<GenericTerminal> semicolon);

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for parameter-declarations non-terminal symbol
/// parameter-declarations = "PARAM" declarator-list ";".
class ParameterDeclarations : public Declarations {
    public:
    /// Constructor
    ParameterDeclarations(code::SourceCode::RangeRef ref, std::unique_ptr<GenericTerminal> keyword, std::unique_ptr<DeclaratorList> declList, std::unique_ptr<GenericTerminal> semicolon);

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
/// Node for function-definition non-terminal symbol (Root node)
class FunctionDefinition : public PTNode {
    std::unique_ptr<ParameterDeclarations> paramDecls;
    std::unique_ptr<VariableDeclarations> varDecls;
    std::unique_ptr<ConstantDeclarations> constDecls;
    std::unique_ptr<CompoundStatement> compStmt;
    std::unique_ptr<GenericTerminal> dot;

    public:
    /// Constructor
    FunctionDefinition(code::SourceCode::RangeRef ref, std::unique_ptr<ParameterDeclarations> paramDecls, std::unique_ptr<VariableDeclarations> varDecls, std::unique_ptr<ConstantDeclarations> constDecls, std::unique_ptr<CompoundStatement> compStmt, std::unique_ptr<GenericTerminal> dot);

    /// Return pointer to parameter-declarations
    const ParameterDeclarations* getParamDecls() const;
    /// Return pointer to variable-declarations
    const VariableDeclarations* getVarDecls() const;
    /// Return pointer to constant-declarations
    const ConstantDeclarations* getConstDecls() const;
    /// Return pointer to compound-statement
    const CompoundStatement* getCompStmt() const;
    /// Return pointer to dot
    const GenericTerminal* getDot() const;

    /// Accept function of the visitor pattern
    void accept(PTVisitor& visitor) const override;
};
//---------------------------------------------------------------------------
} // namespace pljit::pt
//---------------------------------------------------------------------------
#endif