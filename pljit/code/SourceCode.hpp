#ifndef H_pljit_code_SourceCode
#define H_pljit_code_SourceCode
//---------------------------------------------------------------------------
#include <cstddef> // to use std::size_t
#include <iostream>
#include <ostream>
#include <string>
#include <string_view>
//---------------------------------------------------------------------------
namespace pljit::code {
//---------------------------------------------------------------------------
/// Class for the centralized source code management
class SourceCode {
    /// String for source code storage
    std::string code;

    public:
    /// Default constructor
    SourceCode() = default;
    /// Constructor with string (rvalue reference)
    explicit SourceCode(std::string&& code);

    /// Return string_view of source code
    std::string_view string_view() const;

    /// Class RangeRef (forward declared)
    class RangeRef;

    /// Inner class for a location reference (refers to a specific position)
    class LocationRef {
        public:
        /// Position of LocationRef in string
        struct Position {
            std::size_t line;
            std::size_t col;
            std::size_t beginIndex;
            std::size_t endIndex;

            friend SourceCode::LocationRef;

            private:
            /// Constructor
            Position(std::size_t line, std::size_t col, std::size_t beginIndex, std::size_t endIndex);
        };

        private:
        /// string_view of the source code
        std::string_view sv;
        /// Index of the specific position in the source code
        std::size_t index = 0;

        friend class SourceCode::RangeRef;

        public:
        /// Default Constructor
        LocationRef() = default;
        /// Constructor with string_view and index
        LocationRef(std::string_view sv, std::size_t index);

        /// Equality comparison
        bool operator==(const LocationRef& other) const;

        /// Resolve reference to line number and character position (linux only)
        Position resolvePos() const;

        /// Print context (do not support tabs)
        void print(std::string_view cxt, std::ostream& out = std::cout) const;
    };

    /// Inner class for a range reference [begin, end)
    class RangeRef {
        /// begin
        LocationRef begin;
        /// end
        LocationRef end;

        public:
        /// Default constructor
        RangeRef() = default;
        /// Constructor with begin and end index
        RangeRef(std::string_view sv, std::size_t begin, std::size_t end);
        /// Constructor with LocationRef
        RangeRef(LocationRef begin, LocationRef end);

        /// Equality comparison
        bool operator==(const RangeRef& other) const;

        /// Return begin LocationRef
        LocationRef getBegin() const;
        /// Return end LocationRef
        LocationRef getEnd() const;

        /// Return string_view
        std::string_view string_view() const;

        /// Print context (do not support tabs)
        void print(std::string_view cxt, std::ostream& out = std::cout) const;
    };
};
//---------------------------------------------------------------------------
} // namespace pljit::code
//---------------------------------------------------------------------------
#endif