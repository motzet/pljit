#include "pljit/code/SourceCode.hpp"
#include <cassert>
#include <iostream>
//---------------------------------------------------------------------------
namespace pljit::code {
//---------------------------------------------------------------------------
// Class for the centralized source code management
//---------------------------------------------------------------------------
// Constructor with string (rvalue reference)
SourceCode::SourceCode(std::string&& code) : code(code) {
}
//---------------------------------------------------------------------------
// Return string_view of source code
std::string_view SourceCode::string_view() const {
    return code;
}
//---------------------------------------------------------------------------
// Position of LocationRef in string
//---------------------------------------------------------------------------
// Constructor
SourceCode::LocationRef::Position::Position(std::size_t line, std::size_t col, std::size_t beginIndex, std::size_t endIndex) : line(line), col(col), beginIndex(beginIndex), endIndex(endIndex) {}
//---------------------------------------------------------------------------
// Inner class for a location reference (refers to a specific position)
//---------------------------------------------------------------------------
// Constructor
SourceCode::LocationRef::LocationRef(std::string_view sv, std::size_t index) : sv(sv), index(index) {
    assert(index <= sv.length());
}
//---------------------------------------------------------------------------
// Equality comparison
bool SourceCode::LocationRef::operator==(const LocationRef& other) const {
    return (sv == other.sv) && (index == other.index);
}
//---------------------------------------------------------------------------
// Resolve reference to line number and character position (linux only)
SourceCode::LocationRef::Position SourceCode::LocationRef::resolvePos() const {
    std::size_t line = 1; // line number (starting from 1)
    std::size_t col; // column number (starting from 1)
    std::size_t beginIndex = 0; // begin index of line [begin, end)
    std::size_t endIndex = sv.length(); // end index of line [begin, end)

    for (std::size_t i = 0; i < sv.length(); ++i) {
        if (i <= index && sv[i] == '\n') {
            beginIndex = i + 1;
            line++;
        }

        if (i > index && sv[i] == '\n') {
            endIndex = i;
            break;
        }
    }

    col = index - beginIndex + 1;

    return Position(line, col, beginIndex, endIndex);
}
//---------------------------------------------------------------------------
// Print context (do not support tabs)
void SourceCode::LocationRef::print(std::string_view cxt, std::ostream& out) const {
    auto pos = resolvePos();

    out << pos.line << ":" << pos.col << ": " << cxt << std::endl;
    out << sv.substr(pos.beginIndex, pos.endIndex - pos.beginIndex) << std::endl;
    out << std::string(pos.col - 1, ' ') << "^" << std::endl;
}
//---------------------------------------------------------------------------
// Inner class for a range reference [begin, end)
//---------------------------------------------------------------------------
// Constructor with start and end index
SourceCode::RangeRef::RangeRef(std::string_view sv, std::size_t begin, std::size_t end) : begin(LocationRef(sv, begin)), end(LocationRef(sv, end)) {}
//---------------------------------------------------------------------------
// Constructor with LocationRef
SourceCode::RangeRef::RangeRef(LocationRef begin, LocationRef end) : begin(begin), end(end) {}
//---------------------------------------------------------------------------
// Equality comparison
bool SourceCode::RangeRef::operator==(const RangeRef& other) const {
    return (begin == other.begin) && (end == other.end);
}
//---------------------------------------------------------------------------
// Return begin LocationRef
SourceCode::LocationRef SourceCode::RangeRef::getBegin() const {
    return begin;
}
//---------------------------------------------------------------------------
// Return end LocationRef
SourceCode::LocationRef SourceCode::RangeRef::getEnd() const {
    return end;
}
//---------------------------------------------------------------------------
// Return string_view
std::string_view SourceCode::RangeRef::string_view() const {
    return begin.sv.substr(begin.index, end.index - begin.index);
}
//---------------------------------------------------------------------------
// Print context (do not support tabs)
void SourceCode::RangeRef::print(std::string_view cxt, std::ostream& out) const {
    auto pos = begin.resolvePos();

    out << pos.line << ":" << pos.col << ": " << cxt << std::endl;
    out << begin.sv.substr(pos.beginIndex, pos.endIndex - pos.beginIndex) << std::endl;
    out << std::string(pos.col - 1, ' ') << "^";
    for (std::size_t i = begin.index + 1; i < end.index; ++i) {
        out << "~";
    }
    out << std::endl;
}
//---------------------------------------------------------------------------
} // namespace pljit::code
//---------------------------------------------------------------------------
