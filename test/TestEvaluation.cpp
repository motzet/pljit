#include "pljit/ast/EvaluationContext.hpp"
#include "pljit/parser/Parser.hpp"
#include "pljit/semantics/SemanticAnalyser.hpp"
#include <sstream>
#include <gtest/gtest.h>
//---------------------------------------------------------------------------
using namespace std;
using namespace pljit::ast;
using namespace pljit::lexer;
using namespace pljit::parser;
using namespace pljit::pt;
using namespace pljit::semantics;
//---------------------------------------------------------------------------
namespace {
//---------------------------------------------------------------------------
void buildEvaluationContext(const Function& fun, EvaluationContext& ctx) {
    for (auto it = fun.getSymTable().begin(); it != fun.getSymTable().end(); ++it) {
        if (fun.getSymTable().lookupConst(*it)) {
            ctx.setValue(*it, fun.getSymTable().getConstValue(*it));
        }
    }
}
//---------------------------------------------------------------------------
} // namespace
//---------------------------------------------------------------------------
static const char* inputLiteral =
    "BEGIN\n"
    "  RETURN - 10\n\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputConstant =
    "CONST a = 1;"
    "BEGIN\n"
    "  RETURN a\n\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputAdd =
    "CONST a = 1;"
    "BEGIN\n"
    "  RETURN a + 10\n\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputSub =
    "CONST a = 1;"
    "BEGIN\n"
    "  RETURN a - 10\n\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputMul =
    "CONST a = 3;"
    "BEGIN\n"
    "  RETURN a * 30\n\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputDiv01 =
    "CONST a = 2;"
    "BEGIN\n"
    "  RETURN 10/ a\n\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputDiv02 =
    "CONST a = 2;"
    "BEGIN\n"
    "  RETURN 13/ a\n\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputIdentifiers01 =
    "PARAM a;"
    "VAR b;"
    "CONST c = 3;"
    "BEGIN\n"
    "  b := 2 * a;\n"
    "  RETURN a - b + c;\n"
    "  RETURN 200\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputIdentifiers02 =
    "PARAM a;"
    "VAR b;"
    "CONST c = 2;"
    "BEGIN\n"
    "  b := 2 * a;\n"
    "  RETURN b / c * a;\n"
    "  RETURN 200\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputFunction01 =
    "BEGIN\n"
    "  RETURN (10 + 2) - 1 * 3 + 4;\n"
    "  RETURN 200\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputFunction02 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  d := (a + 4) * c + i;\n"
    "  e := (a + 4) * c * d * (h + 1);\n"
    "  f := a + b + c + d + e + g;\n"
    "  RETURN (e + f) - 1 * d + i\n\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputFunction03 =
    "PARAM width, height, depth;\n"
    "VAR volume;\n"
    "CONST density = 2400;\n\n"
    "BEGIN\n"
    "  volume := width * height * depth;\n"
    "  RETURN density / volume\n\n"
    "END.";
//---------------------------------------------------------------------------
TEST(TestEvaluation, EvaluateLiteral) {
    ostringstream buf;
    Lexer lexer(inputLiteral, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(ctx.isError());
    ASSERT_EQ(ctx.getResult(), -10);
}
//---------------------------------------------------------------------------
TEST(TestEvaluation, EvaluateConstant) {
    ostringstream buf;
    Lexer lexer(inputConstant, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    buildEvaluationContext(*ast, ctx);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(ctx.isError());
    ASSERT_EQ(ctx.getResult(), 1);
}
//---------------------------------------------------------------------------
TEST(TestEvaluation, EvaluateAdd) {
    ostringstream buf;
    Lexer lexer(inputAdd, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    buildEvaluationContext(*ast, ctx);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(ctx.isError());
    ASSERT_EQ(ctx.getResult(), 11);
}
//---------------------------------------------------------------------------
TEST(TestEvaluation, EvaluateSub) {
    ostringstream buf;
    Lexer lexer(inputSub, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    buildEvaluationContext(*ast, ctx);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(ctx.isError());
    ASSERT_EQ(ctx.getResult(), -9);
}
//---------------------------------------------------------------------------
TEST(TestEvaluation, EvaluateMul) {
    ostringstream buf;
    Lexer lexer(inputMul, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    buildEvaluationContext(*ast, ctx);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(ctx.isError());
    ASSERT_EQ(ctx.getResult(), 90);
}
//---------------------------------------------------------------------------
TEST(TestEvaluation, EvaluateDiv01) {
    ostringstream buf;
    Lexer lexer(inputDiv01, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    buildEvaluationContext(*ast, ctx);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(ctx.isError());
    ASSERT_EQ(ctx.getResult(), 5);
}
//---------------------------------------------------------------------------
TEST(TestEvaluation, EvaluateDiv02) {
    ostringstream buf;
    Lexer lexer(inputDiv02, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    buildEvaluationContext(*ast, ctx);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(ctx.isError());
    ASSERT_EQ(ctx.getResult(), 6);
}
//---------------------------------------------------------------------------
TEST(TestEvaluation, EvaluateIdentifiers01) {
    ostringstream buf;
    Lexer lexer(inputIdentifiers01, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    buildEvaluationContext(*ast, ctx);
    ctx.setValue("a", 1);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(ctx.isError());
    ASSERT_EQ(ctx.getResult(), -4);
}
//---------------------------------------------------------------------------
TEST(TestEvaluation, EvaluateIdentifiers02) {
    ostringstream buf;
    Lexer lexer(inputIdentifiers02, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    buildEvaluationContext(*ast, ctx);
    ctx.setValue("a", 5);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(ctx.isError());
    ASSERT_EQ(ctx.getResult(), 1);
}
//---------------------------------------------------------------------------
TEST(TestEvaluation, EvaluateFunction01) {
    ostringstream buf;
    Lexer lexer(inputFunction01, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(ctx.isError());
    ASSERT_EQ(ctx.getResult(), 5);
}
//---------------------------------------------------------------------------
TEST(TestEvaluation, EvaluateFunction02) {
    ostringstream buf;
    Lexer lexer(inputFunction02, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    buildEvaluationContext(*ast, ctx);
    ctx.setValue("a", 1);
    ctx.setValue("b", 2);
    ctx.setValue("c", 3);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(ctx.isError());
    ASSERT_EQ(ctx.getResult(), 1024);
}
//---------------------------------------------------------------------------
TEST(TestEvaluation, ErrorDivisionByZero) {
    ostringstream buf;
    Lexer lexer(inputFunction03, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    EvaluationContext ctx(buf);
    buildEvaluationContext(*ast, ctx);
    ctx.setValue("width", 0);
    ctx.setValue("height", 2);
    ctx.setValue("depth", 5);
    ast->evaluate(ctx);
    ASSERT_EQ(buf.str(), "error: division by zero\n");
    ASSERT_TRUE(ctx.isError());
}
//---------------------------------------------------------------------------