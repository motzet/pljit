#include "pljit/parsetree/DotPTVisitor.hpp"
#include "pljit/parsetree/ParseTree.hpp"
#include <string_view>
#include <utility>
#include <gtest/gtest.h>
//---------------------------------------------------------------------------
using namespace std;
using namespace pljit::pt;
using namespace pljit::code;
//---------------------------------------------------------------------------
static const char* expectedOutput01 =
    "\t0 [label=\"parameter-declarations\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\"\\\"PARAM\\\"\"];\n"
    "\t0 -> 2;\n"
    "\t2 [label=\"declarator-list\"];\n"
    "\t2 -> 3;\n"
    "\t3 [label=\"\\\"a\\\"\"];\n"
    "\t2 -> 4;\n"
    "\t4 [label=\"\\\",\\\"\"];\n"
    "\t2 -> 5;\n"
    "\t5 [label=\"\\\"b\\\"\"];\n"
    "\t2 -> 6;\n"
    "\t6 [label=\"\\\",\\\"\"];\n"
    "\t2 -> 7;\n"
    "\t7 [label=\"\\\"c\\\"\"];\n"
    "\t0 -> 8;\n"
    "\t8 [label=\"\\\";\\\"\"];\n";
static const char* expectedOutput02 =
    "\t0 [label=\"variable-declarations\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\"\\\"VAR\\\"\"];\n"
    "\t0 -> 2;\n"
    "\t2 [label=\"declarator-list\"];\n"
    "\t2 -> 3;\n"
    "\t3 [label=\"\\\"a\\\"\"];\n"
    "\t2 -> 4;\n"
    "\t4 [label=\"\\\",\\\"\"];\n"
    "\t2 -> 5;\n"
    "\t5 [label=\"\\\"b\\\"\"];\n"
    "\t2 -> 6;\n"
    "\t6 [label=\"\\\",\\\"\"];\n"
    "\t2 -> 7;\n"
    "\t7 [label=\"\\\"c\\\"\"];\n"
    "\t0 -> 8;\n"
    "\t8 [label=\"\\\";\\\"\"];\n";
static const char* expectedOutput03 =
    "\t0 [label=\"init-declarator\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\"\\\"a\\\"\"];\n"
    "\t0 -> 2;\n"
    "\t2 [label=\"\\\"=\\\"\"];\n"
    "\t0 -> 3;\n"
    "\t3 [label=\"0\"];\n";
static const char* expectedOutput04 =
    "\t0 [label=\"constant-declarations\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\"\\\"CONST\\\"\"];\n"
    "\t0 -> 2;\n"
    "\t2 [label=\"init-declarator-list\"];\n"
    "\t2 -> 3;\n"
    "\t3 [label=\"init-declarator\"];\n"
    "\t3 -> 4;\n"
    "\t4 [label=\"\\\"a\\\"\"];\n"
    "\t3 -> 5;\n"
    "\t5 [label=\"\\\"=\\\"\"];\n"
    "\t3 -> 6;\n"
    "\t6 [label=\"0\"];\n"
    "\t0 -> 7;\n"
    "\t7 [label=\"\\\";\\\"\"];\n";
//---------------------------------------------------------------------------
TEST(TestDotPTVisitor, PrintGenericTerminal01) {
    ostringstream buf;
    DotPTVisitor visitor(buf);
    string_view sv("+");
    SourceCode::RangeRef ref(sv, 0, 1);
    unique_ptr<GenericTerminal> node = make_unique<GenericTerminal>(ref);
    node->accept(visitor);

    ASSERT_EQ(buf.str(), "\t0 [label=\"\\\"+\\\"\"];\n");
}
//---------------------------------------------------------------------------
TEST(TestDotPTVisitor, PrintGenericTerminal02) {
    ostringstream buf;
    DotPTVisitor visitor(buf);
    string_view sv(":=");
    SourceCode::RangeRef ref(sv, 0, 2);
    unique_ptr<GenericTerminal> node = make_unique<GenericTerminal>(ref);
    node->accept(visitor);

    ASSERT_EQ(buf.str(), "\t0 [label=\"\\\":=\\\"\"];\n");
}
//---------------------------------------------------------------------------
TEST(TestDotPTVisitor, PrintLiteral) {
    ostringstream buf;
    DotPTVisitor visitor(buf);
    string_view sv("01234567890");
    SourceCode::RangeRef ref(sv, 0, 10);
    unique_ptr<Literal> node = make_unique<Literal>(ref);
    node->accept(visitor);

    ASSERT_EQ(buf.str(), "\t0 [label=\"0123456789\"];\n");
}
//---------------------------------------------------------------------------
TEST(TestDotPTVisitor, PrintIdentifier) {
    ostringstream buf;
    DotPTVisitor visitor(buf);
    string_view sv("abcdefghijklmnopqrstuvwxyz");
    SourceCode::RangeRef ref(sv, 0, 26);
    unique_ptr<Identifier> node = make_unique<Identifier>(ref);
    node->accept(visitor);

    ASSERT_EQ(buf.str(), "\t0 [label=\"\\\"abcdefghijklmnopqrstuvwxyz\\\"\"];\n");
}
//---------------------------------------------------------------------------
TEST(TestDotPTVisitor, PrintPARAMDeclaration) {
    ostringstream buf;
    DotPTVisitor visitor(buf);
    string_view sv("PARAM a, b, c;");

    SourceCode::RangeRef ref(sv, 0, 14);
    vector<unique_ptr<Identifier>> idens;
    vector<unique_ptr<GenericTerminal>> commas;
    idens.emplace_back(make_unique<Identifier>(SourceCode::RangeRef(sv, 6, 7)));
    commas.emplace_back(make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 7, 8)));
    idens.emplace_back(make_unique<Identifier>(SourceCode::RangeRef(sv, 9, 10)));
    commas.emplace_back(make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 10, 11)));
    idens.emplace_back(make_unique<Identifier>(SourceCode::RangeRef(sv, 12, 13)));
    auto keyword = make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 0, 5));
    auto declList = make_unique<DeclaratorList>(SourceCode::RangeRef(sv, 6, 13), move(idens), move(commas));
    auto semicolon = make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 13, 14));
    auto node = make_unique<ParameterDeclarations>(ref, move(keyword), move(declList), move(semicolon));
    node->accept(visitor);

    ASSERT_EQ(buf.str(), expectedOutput01);
}
//---------------------------------------------------------------------------
TEST(TestDotPTVisitor, PrintVARDeclaration) {
    ostringstream buf;
    DotPTVisitor visitor(buf);
    string_view sv("  VAR a, b, c;");

    SourceCode::RangeRef ref(sv, 0, 14);
    vector<unique_ptr<Identifier>> idens;
    vector<unique_ptr<GenericTerminal>> commas;
    idens.emplace_back(make_unique<Identifier>(SourceCode::RangeRef(sv, 6, 7)));
    commas.emplace_back(make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 7, 8)));
    idens.emplace_back(make_unique<Identifier>(SourceCode::RangeRef(sv, 9, 10)));
    commas.emplace_back(make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 10, 11)));
    idens.emplace_back(make_unique<Identifier>(SourceCode::RangeRef(sv, 12, 13)));
    auto keyword = make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 2, 5));
    auto declList = make_unique<DeclaratorList>(SourceCode::RangeRef(sv, 6, 13), move(idens), move(commas));
    auto semicolon = make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 13, 14));
    auto node = make_unique<VariableDeclarations>(ref, move(keyword), move(declList), move(semicolon));
    node->accept(visitor);

    ASSERT_EQ(buf.str(), expectedOutput02);
}
//---------------------------------------------------------------------------
TEST(TestDotPTVisitor, PrintInitDeclarator) {
    ostringstream buf;
    DotPTVisitor visitor(buf);
    string_view sv("a = 0");

    SourceCode::RangeRef ref(sv, 0, 5);
    auto iden = make_unique<Identifier>(SourceCode::RangeRef(sv, 0, 1));
    auto equal = make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 2, 3));
    auto lit = make_unique<Literal>(SourceCode::RangeRef(sv, 4, 5));
    auto node = make_unique<InitDeclarator>(ref, move(iden), move(equal), move(lit));
    node->accept(visitor);

    ASSERT_EQ(buf.str(), expectedOutput03);
}
//---------------------------------------------------------------------------
TEST(TestDotPTVisitor, PrintCONSTDeclaration) {
    ostringstream buf;
    DotPTVisitor visitor(buf);
    string_view sv("CONST a = 0;");

    SourceCode::RangeRef ref(sv, 0, 12);
    vector<unique_ptr<InitDeclarator>> initDecls;
    vector<unique_ptr<GenericTerminal>> commas;
    auto iden = make_unique<Identifier>(SourceCode::RangeRef(sv, 6, 7));
    auto equal = make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 8, 9));
    auto lit = make_unique<Literal>(SourceCode::RangeRef(sv, 10, 11));
    initDecls.emplace_back(make_unique<InitDeclarator>(SourceCode::RangeRef(sv, 6, 11), move(iden), move(equal), move(lit)));
    auto keyword = make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 0, 5));
    auto initDeclList = make_unique<InitDeclaratorList>(SourceCode::RangeRef(sv, 6, 11), move(initDecls), move(commas));
    auto semicolon = make_unique<GenericTerminal>(SourceCode::RangeRef(sv, 11, 12));
    auto node = make_unique<ConstantDeclarations>(ref, move(keyword), move(initDeclList), move(semicolon));
    node->accept(visitor);

    ASSERT_EQ(buf.str(), expectedOutput04);
}
//---------------------------------------------------------------------------