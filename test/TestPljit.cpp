#include "pljit/Pljit.hpp"
#include <sstream>
#include <thread>
#include <gtest/gtest.h>
//---------------------------------------------------------------------------
using namespace std;
using namespace pljit;
//---------------------------------------------------------------------------
static const char* inputFunction01 =
    "PARAM width, height, depth;\n"
    "VAR volume;\n"
    "CONST density = 2400;\n\n"
    "BEGIN\n"
    "  volume := width * height * depth;\n"
    "  RETURN density * volume\n\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputFunction02 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  d := (a + 4) * c + i;\n"
    "  e := (a + 4) * c * d * (h + 1);\n"
    "  f := a + b + c + d + e;\n"
    "  RETURN (e + f) - 1 * d + i\n\n"
    "END.";
//---------------------------------------------------------------------------
static const char* inputFunction03 =
    "PARAM width, height, depth;\n"
    "VAR volume;\n"
    "CONST density = 2400;\n\n"
    "BEGIN\n"
    "  volume := width ^ height * depth;\n"
    "  RETURN density * volume\n\n"
    "END.";
static const char* expectedOutput03 =
    "6:19: error: illegal character\n"
    "  volume := width ^ height * depth;\n"
    "                  ^\n";
//---------------------------------------------------------------------------
static const char* inputFunction04 =
    "PARAM width, height, depth;\n"
    "VAR volume;\n"
    "CONST density = 2400;\n\n"
    "BEGIN\n"
    "  volume = width * height * depth;\n"
    "  RETURN density * volume\n\n"
    "END.";
static const char* expectedOutput04 =
    "6:10: error: expected ':='\n"
    "  volume = width * height * depth;\n"
    "         ^\n";
//---------------------------------------------------------------------------
static const char* inputFunction05 =
    "PARAM width, height, depth;\n"
    "VAR volume, var;\n"
    "CONST density = 2400;\n\n"
    "BEGIN\n"
    "  volume := width * height * depth + var;\n"
    "  RETURN density * volume\n\n"
    "END.";
static const char* expectedOutput05 =
    "6:38: error: uninitialized variable\n"
    "  volume := width * height * depth + var;\n"
    "                                     ^~~\n"
    "7:20: error: uninitialized variable\n"
    "  RETURN density * volume\n"
    "                   ^~~~~~\n";
//---------------------------------------------------------------------------
static const char* inputFunction06 =
    "PARAM width, height, depth;\n"
    "VAR volume;\n"
    "CONST density = 2400;\n\n"
    "BEGIN\n"
    "  volume := width * height * depth;\n"
    "  RETURN density / volume\n\n"
    "END.";
static const char* expectedOutput06 =
    "error: division by zero\n";
//---------------------------------------------------------------------------
TEST(TestPljit, RegisterAndCallFunction01) {
    ostringstream buf;
    Pljit jit;
    auto func = jit.registerFunction(inputFunction01);
    auto result = func({10, 50, 100}, buf);

    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(result.error);
    ASSERT_EQ(result.val, 120000000);
}
//---------------------------------------------------------------------------
TEST(TestPljit, RegisterAndCallFunction02) {
    ostringstream buf;
    Pljit jit;
    auto func = jit.registerFunction(inputFunction02);
    auto result = func({1, 2, 3}, buf);

    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(result.error);
    ASSERT_EQ(result.val, 1024);
}
//---------------------------------------------------------------------------
TEST(TestPljit, RegisterAndMultipleCalls) {
    ostringstream buf;
    Pljit jit;
    auto func = jit.registerFunction(inputFunction01);

    for (int64_t i = 0; i < 100; ++i) {
        auto result = func({i, 2, 3}, buf);

        ASSERT_EQ(buf.str(), "");
        ASSERT_FALSE(result.error);
        ASSERT_EQ(result.val, i * 2 * 3 * 2400);
    }
}
//---------------------------------------------------------------------------
TEST(TestPljit, RegisterMultipleFunctionsAndCall) {
    ostringstream buf;
    Pljit jit;

    for (int64_t i = 0; i < 10; ++i) {
        auto func = jit.registerFunction(inputFunction01);
        auto result = func({i, 3, 4}, buf);
        ASSERT_EQ(buf.str(), "");
        ASSERT_FALSE(result.error);
        ASSERT_EQ(result.val, i * 3 * 4 * 2400);
    }
}
//---------------------------------------------------------------------------
TEST(TestPljit, CopyFunctionHandle) {
    ostringstream buf;
    Pljit jit;

    auto func01 = jit.registerFunction(inputFunction01);
    auto func02 = jit.registerFunction(inputFunction02);
    auto result01 = func01({1, 50, 100}, buf);
    auto result02 = func02({1, 2, 3}, buf);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(result01.error);
    ASSERT_EQ(result01.val, 1 * 50 * 100 * 2400);
    ASSERT_FALSE(result02.error);
    ASSERT_EQ(result02.val, 1024);
    auto func03 = func01;
    Pljit::FunctionHandle func04(func02);
    auto result03 = func03({1, 40, 100}, buf);
    auto result04 = func04({1, 2, 3}, buf);
    ASSERT_EQ(buf.str(), "");
    ASSERT_FALSE(result03.error);
    ASSERT_EQ(result03.val, 1 * 40 * 100 * 2400);
    ASSERT_FALSE(result04.error);
    ASSERT_EQ(result04.val, 1024);
}
//---------------------------------------------------------------------------
TEST(TestPljit, ParallelCallFunctions) {
    Pljit jit;
    auto func = jit.registerFunction(inputFunction02);

    vector<ostringstream> bufs(10);
    vector<Pljit::FunctionHandle::Result> results(10);
    vector<thread> threads;
    for (int64_t i = 0; i < 10; ++i)
        threads.emplace_back([&func, &bufs, &results, i]() {
            results[i] = func({1, 2, 3}, bufs[i]);
        });

    for (auto& thread : threads) {
        thread.join();
    }

    for (size_t i = 0; i < 10; ++i) {
        ASSERT_EQ(bufs[i].str(), "");
        ASSERT_FALSE(results[i].error);
        ASSERT_EQ(results[i].val, 1024);
    }
}
//---------------------------------------------------------------------------
TEST(TestPljit, ErrorLexer) {
    ostringstream buf;
    Pljit jit;
    auto func = jit.registerFunction(inputFunction03);

    for (int64_t i = 0; i < 100; ++i) {
        auto result = func({i, 2, 3}, buf);

        ASSERT_TRUE(result.error);
        ASSERT_EQ(buf.str(), expectedOutput03);
    }
}
//---------------------------------------------------------------------------
TEST(TestPljit, ErrorParser) {
    ostringstream buf;
    Pljit jit;
    auto func = jit.registerFunction(inputFunction04);

    for (int64_t i = 0; i < 100; ++i) {
        auto result = func({i, 2, 3}, buf);

        ASSERT_TRUE(result.error);
        ASSERT_EQ(buf.str(), expectedOutput04);
    }
}
//---------------------------------------------------------------------------
TEST(TestPljit, ParallelErrorParser) {
    Pljit jit;
    auto func = jit.registerFunction(inputFunction04);

    vector<ostringstream> bufs(10);
    vector<Pljit::FunctionHandle::Result> results(10);
    vector<thread> threads;
    for (size_t i = 0; i < 10; ++i)
        threads.emplace_back([&func, &bufs, &results, i]() {
            results[i] = func({1, 2, 3}, bufs[i]);
        });

    for (auto& thread : threads) {
        thread.join();
    }

    size_t counter = 0;
    size_t index = 0;
    for (size_t i = 0; i < 10; ++i) {
        ASSERT_TRUE(results[i].error);
        if (bufs[i].str() != "") {
            ++counter;
            index = i;
        }
    }
    ASSERT_EQ(counter, 1);
    ASSERT_EQ(bufs[index].str(), expectedOutput04);
}
//---------------------------------------------------------------------------
TEST(TestPljit, ErrorSemantics) {
    ostringstream buf;
    Pljit jit;
    auto func = jit.registerFunction(inputFunction05);

    for (int64_t i = 0; i < 100; ++i) {
        auto result = func({i, 2, 3}, buf);

        ASSERT_TRUE(result.error);
        ASSERT_EQ(buf.str(), expectedOutput05);
    }
}
//---------------------------------------------------------------------------
TEST(TestPljit, ParallelErrorSemantics) {
    Pljit jit;
    auto func = jit.registerFunction(inputFunction05);

    vector<ostringstream> bufs(10);
    vector<Pljit::FunctionHandle::Result> results(10);
    vector<thread> threads;
    for (int64_t i = 0; i < 10; ++i)
        threads.emplace_back([&func, &bufs, &results, i]() {
            results[i] = func({i, 2, 3}, bufs[i]);
        });

    for (auto& thread : threads) {
        thread.join();
    }

    size_t counter = 0;
    size_t index = 0;
    for (size_t i = 0; i < 10; ++i) {
        ASSERT_TRUE(results[i].error);
        if (!bufs[i].str().empty()) {
            ++counter;
            index = i;
        }
    }
    ASSERT_EQ(counter, 1);
    ASSERT_EQ(bufs[index].str(), expectedOutput05);
}
//---------------------------------------------------------------------------
TEST(TestPljit, ErrorDivisionByZero) {
    ostringstream buf;
    Pljit jit;
    auto func = jit.registerFunction(inputFunction06);
    auto result = func({0, 2, 3}, buf);

    ASSERT_TRUE(result.error);
    ASSERT_EQ(buf.str(), expectedOutput06);
}
//---------------------------------------------------------------------------
TEST(TestPljit, ParallelErrorDivisionByZero) {
    Pljit jit;
    auto func = jit.registerFunction(inputFunction06);

    vector<ostringstream> bufs(10);
    vector<Pljit::FunctionHandle::Result> results(10);
    vector<thread> threads;
    for (int64_t i = 0; i < 10; ++i)
        threads.emplace_back([&func, &bufs, &results, i]() {
            results[i] = func({0, i, 3}, bufs[i]);
        });

    for (auto& thread : threads) {
        thread.join();
    }

    for (size_t i = 0; i < 10; ++i) {
        ASSERT_TRUE(results[i].error);
        if (!bufs[i].str().empty()) {
            ASSERT_EQ(bufs[i].str(), expectedOutput06);
        }
    }
}
//---------------------------------------------------------------------------