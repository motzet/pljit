#include "pljit/lexer/Lexer.hpp"
#include <array>
#include <sstream>
#include <gtest/gtest.h>
//---------------------------------------------------------------------------
using namespace std;
using namespace pljit::lexer;
using namespace pljit::code;
using namespace pljit::token;
//---------------------------------------------------------------------------
static const char* expectedErrorIllegalCharacter =
    "3:4: error: illegal character\n"
    "   ~a\n"
    "   ^\n";
static const char* expectedErrorAssignmentOperator =
    "1:4: error: expected '='\n"
    "  :+\n"
    "   ^\n";
//---------------------------------------------------------------------------
TEST(TestLexer, SingleIdentifier01) {
    string_view sv("\tabcdefghijklmnopqrstuvwzyzABCDEFGHIJKLMNOPQRSTUVWXYZ  ");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 1, 53);

    ASSERT_EQ(token.getType(), Token::Type::Identifier);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleIdentifier02) {
    string_view sv("   PARAMa");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 3, 9);

    ASSERT_EQ(token.getType(), Token::Type::Identifier);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SinglePARAMKeyword) {
    string_view sv("PARAM");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 0, 5);

    ASSERT_EQ(token.getType(), Token::Type::PARAMKeyword);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleVARKeyword) {
    string_view sv("  VAR");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 2, 5);

    ASSERT_EQ(token.getType(), Token::Type::VARKeyword);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleCONSTKeyword) {
    string_view sv("\nCONST ");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 1, 6);

    ASSERT_EQ(token.getType(), Token::Type::CONSTKeyword);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleBEGINKeyword) {
    string_view sv("\n  BEGIN");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 3, 8);

    ASSERT_EQ(token.getType(), Token::Type::BEGINKeyword);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleRETURNKeyword) {
    string_view sv("\n\n  RETURN\tEND");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 4, 10);

    ASSERT_EQ(token.getType(), Token::Type::RETURNKeyword);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleENDKeyword) {
    string_view sv("\n\tEND");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 2, 5);

    ASSERT_EQ(token.getType(), Token::Type::ENDKeyword);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleLiteral) {
    string_view sv("\n 1234567890 ");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 2, 12);

    ASSERT_EQ(token.getType(), Token::Type::Literal);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleAddOperator) {
    string_view sv("  +124");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 2, 3);

    ASSERT_EQ(token.getType(), Token::Type::AddOperator);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleSubOperator) {
    string_view sv("\n-095");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 1, 2);

    ASSERT_EQ(token.getType(), Token::Type::SubOperator);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleMulOperator) {
    string_view sv("\t * (-095)");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 2, 3);

    ASSERT_EQ(token.getType(), Token::Type::MulOperator);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleDivOperator) {
    string_view sv("/ 3");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 0, 1);

    ASSERT_EQ(token.getType(), Token::Type::DivOperator);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleAssignmentOperator) {
    string_view sv("  := 4");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 2, 4);

    ASSERT_EQ(token.getType(), Token::Type::AssignmentOperator);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleDotSeparator) {
    string_view sv(" . ");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 1, 2);

    ASSERT_EQ(token.getType(), Token::Type::DotSeparator);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleCommaSeparator) {
    string_view sv(",,");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 0, 1);

    ASSERT_EQ(token.getType(), Token::Type::CommaSeparator);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleSemicolonSeparator) {
    string_view sv("\t;");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 1, 2);

    ASSERT_EQ(token.getType(), Token::Type::SemicolonSeparator);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleEqualSeperator) {
    string_view sv("  =");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 2, 3);

    ASSERT_EQ(token.getType(), Token::Type::EqualSeparator);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, SingleParentheseSeperator) {
    string_view sv(" (  ) ");
    Lexer lexer(sv);
    Token token1 = lexer.next();
    Token token2 = lexer.next();
    SourceCode::RangeRef ref1(sv, 1, 2);
    SourceCode::RangeRef ref2(sv, 4, 5);

    ASSERT_EQ(token1.getType(), Token::Type::OpeningParenthesisSeparator);
    ASSERT_EQ(token1.getRef(), ref1);
    ASSERT_EQ(token2.getType(), Token::Type::ClosingParenthesisSeparator);
    ASSERT_EQ(token2.getRef(), ref2);
}
//---------------------------------------------------------------------------
TEST(TestLexer, EndOfSource01) {
    string_view sv("");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 0, 0);

    ASSERT_EQ(token.getType(), Token::Type::End);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, EndOfSource02) {
    string_view sv("  \0");
    Lexer lexer(sv);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 2, 2);

    ASSERT_EQ(token.getType(), Token::Type::End);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, ErrorIllegalCharacter) {
    ostringstream buf;
    string_view sv("\n\n   ~a");
    Lexer lexer(sv, buf);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 5, 6);

    ASSERT_EQ(buf.str(), expectedErrorIllegalCharacter);
    ASSERT_EQ(token.getType(), Token::Type::Error);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, ErrorAssignmentOperator) {
    ostringstream buf;
    string_view sv("  :+");
    Lexer lexer(sv, buf);
    Token token = lexer.next();
    SourceCode::RangeRef ref(sv, 3, 4);

    ASSERT_EQ(buf.str(), expectedErrorAssignmentOperator);
    ASSERT_EQ(token.getType(), Token::Type::Error);
    ASSERT_EQ(token.getRef(), ref);
}
//---------------------------------------------------------------------------
TEST(TestLexer, MultipleToken01) {
    string_view sv("1234abcd");
    Lexer lexer(sv);
    Token token1 = lexer.next();
    Token token2 = lexer.next();
    SourceCode::RangeRef ref1(sv, 0, 4);
    SourceCode::RangeRef ref2(sv, 4, 8);

    ASSERT_EQ(token1.getType(), Token::Type::Literal);
    ASSERT_EQ(token1.getRef(), ref1);
    ASSERT_EQ(token2.getType(), Token::Type::Identifier);
    ASSERT_EQ(token2.getRef(), ref2);
}
//---------------------------------------------------------------------------
TEST(TestLexer, MultipleToken02) {
    string_view sv("abcd1234");
    Lexer lexer(sv);
    Token token1 = lexer.next();
    Token token2 = lexer.next();
    SourceCode::RangeRef ref1(sv, 0, 4);
    SourceCode::RangeRef ref2(sv, 4, 8);

    ASSERT_EQ(token1.getType(), Token::Type::Identifier);
    ASSERT_EQ(token1.getRef(), ref1);
    ASSERT_EQ(token2.getType(), Token::Type::Literal);
    ASSERT_EQ(token2.getRef(), ref2);
}
//---------------------------------------------------------------------------
TEST(TestLexer, MultipleToken03) {
    string_view sv("1234 5678");
    Lexer lexer(sv);
    Token token1 = lexer.next();
    Token token2 = lexer.next();
    SourceCode::RangeRef ref1(sv, 0, 4);
    SourceCode::RangeRef ref2(sv, 5, 9);

    ASSERT_EQ(token1.getType(), Token::Type::Literal);
    ASSERT_EQ(token1.getRef(), ref1);
    ASSERT_EQ(token2.getType(), Token::Type::Literal);
    ASSERT_EQ(token2.getRef(), ref2);
}
//---------------------------------------------------------------------------
TEST(TestLexer, MultipleToken04) {
    string_view sv("VAR0123");
    Lexer lexer(sv);
    Token token1 = lexer.next();
    Token token2 = lexer.next();
    SourceCode::RangeRef ref1(sv, 0, 3);
    SourceCode::RangeRef ref2(sv, 3, 7);

    ASSERT_EQ(token1.getType(), Token::Type::VARKeyword);
    ASSERT_EQ(token1.getRef(), ref1);
    ASSERT_EQ(token2.getType(), Token::Type::Literal);
    ASSERT_EQ(token2.getRef(), ref2);
}
//---------------------------------------------------------------------------
TEST(TestLexer, ParamDeclaration) {
    string_view sv("PARAM  abc, def;");
    Lexer lexer(sv);
    array<Token::Type, 10> types = {Token::Type::PARAMKeyword, Token::Type::Identifier, Token::Type::CommaSeparator, Token::Type::Identifier, Token::Type::SemicolonSeparator};
    array<SourceCode::RangeRef, 10> refs = {SourceCode::RangeRef(sv, 0, 5), SourceCode::RangeRef(sv, 7, 10), SourceCode::RangeRef(sv, 10, 11), SourceCode::RangeRef(sv, 12, 15), SourceCode::RangeRef(sv, 15, 16)};

    for (int i = 0; i < 5; ++i) {
        Token token = lexer.next();
        ASSERT_EQ(token.getType(), types[i]);
        ASSERT_EQ(token.getRef(), refs[i]);
    }
}
//---------------------------------------------------------------------------
TEST(TestLexer, Assignment) {
    string_view sv("a := (b + c) * a; ");
    Lexer lexer(sv);
    array<Token::Type, 10> types = {Token::Type::Identifier, Token::Type::AssignmentOperator, Token::Type::OpeningParenthesisSeparator, Token::Type::Identifier, Token::Type::AddOperator, Token::Type::Identifier, Token::Type::ClosingParenthesisSeparator, Token::Type::MulOperator, Token::Type::Identifier, Token::Type::SemicolonSeparator};
    array<SourceCode::RangeRef, 10> refs = {SourceCode::RangeRef(sv, 0, 1), SourceCode::RangeRef(sv, 2, 4), SourceCode::RangeRef(sv, 5, 6), SourceCode::RangeRef(sv, 6, 7), SourceCode::RangeRef(sv, 8, 9), SourceCode::RangeRef(sv, 10, 11), SourceCode::RangeRef(sv, 11, 12), SourceCode::RangeRef(sv, 13, 14), SourceCode::RangeRef(sv, 15, 16), SourceCode::RangeRef(sv, 16, 17)};

    for (int i = 0; i < 10; ++i) {
        Token token = lexer.next();
        ASSERT_EQ(token.getType(), types[i]);
        ASSERT_EQ(token.getRef(), refs[i]);
    }
}
//---------------------------------------------------------------------------