#include "pljit/parser/Parser.hpp"
#include "pljit/parsetree/DotPTVisitor.hpp"
#include <sstream>
#include <gtest/gtest.h>
//---------------------------------------------------------------------------
using namespace std;
using namespace pljit::lexer;
using namespace pljit::parser;
using namespace pljit::pt;
//---------------------------------------------------------------------------
static const char* inputFunction01 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  RETURN h\n\n"
    "END.";
static const char* expectedPT01 =
    "digraph {\n"
    "\t0 [label=\"function-definition\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\"parameter-declarations\"];\n"
    "\t1 -> 2;\n"
    "\t2 [label=\"\\\"PARAM\\\"\"];\n"
    "\t1 -> 3;\n"
    "\t3 [label=\"declarator-list\"];\n"
    "\t3 -> 4;\n"
    "\t4 [label=\"\\\"a\\\"\"];\n"
    "\t3 -> 5;\n"
    "\t5 [label=\"\\\",\\\"\"];\n"
    "\t3 -> 6;\n"
    "\t6 [label=\"\\\"b\\\"\"];\n"
    "\t3 -> 7;\n"
    "\t7 [label=\"\\\",\\\"\"];\n"
    "\t3 -> 8;\n"
    "\t8 [label=\"\\\"c\\\"\"];\n"
    "\t1 -> 9;\n"
    "\t9 [label=\"\\\";\\\"\"];\n"
    "\t0 -> 10;\n"
    "\t10 [label=\"variable-declarations\"];\n"
    "\t10 -> 11;\n"
    "\t11 [label=\"\\\"VAR\\\"\"];\n"
    "\t10 -> 12;\n"
    "\t12 [label=\"declarator-list\"];\n"
    "\t12 -> 13;\n"
    "\t13 [label=\"\\\"d\\\"\"];\n"
    "\t12 -> 14;\n"
    "\t14 [label=\"\\\",\\\"\"];\n"
    "\t12 -> 15;\n"
    "\t15 [label=\"\\\"e\\\"\"];\n"
    "\t12 -> 16;\n"
    "\t16 [label=\"\\\",\\\"\"];\n"
    "\t12 -> 17;\n"
    "\t17 [label=\"\\\"f\\\"\"];\n"
    "\t10 -> 18;\n"
    "\t18 [label=\"\\\";\\\"\"];\n"
    "\t0 -> 19;\n"
    "\t19 [label=\"constant-declarations\"];\n"
    "\t19 -> 20;\n"
    "\t20 [label=\"\\\"CONST\\\"\"];\n"
    "\t19 -> 21;\n"
    "\t21 [label=\"init-declarator-list\"];\n"
    "\t21 -> 22;\n"
    "\t22 [label=\"init-declarator\"];\n"
    "\t22 -> 23;\n"
    "\t23 [label=\"\\\"g\\\"\"];\n"
    "\t22 -> 24;\n"
    "\t24 [label=\"\\\"=\\\"\"];\n"
    "\t22 -> 25;\n"
    "\t25 [label=\"0\"];\n"
    "\t21 -> 26;\n"
    "\t26 [label=\"\\\",\\\"\"];\n"
    "\t21 -> 27;\n"
    "\t27 [label=\"init-declarator\"];\n"
    "\t27 -> 28;\n"
    "\t28 [label=\"\\\"h\\\"\"];\n"
    "\t27 -> 29;\n"
    "\t29 [label=\"\\\"=\\\"\"];\n"
    "\t27 -> 30;\n"
    "\t30 [label=\"1\"];\n"
    "\t21 -> 31;\n"
    "\t31 [label=\"\\\",\\\"\"];\n"
    "\t21 -> 32;\n"
    "\t32 [label=\"init-declarator\"];\n"
    "\t32 -> 33;\n"
    "\t33 [label=\"\\\"i\\\"\"];\n"
    "\t32 -> 34;\n"
    "\t34 [label=\"\\\"=\\\"\"];\n"
    "\t32 -> 35;\n"
    "\t35 [label=\"2\"];\n"
    "\t19 -> 36;\n"
    "\t36 [label=\"\\\";\\\"\"];\n"
    "\t0 -> 37;\n"
    "\t37 [label=\"compound-statement\"];\n"
    "\t37 -> 38;\n"
    "\t38 [label=\"\\\"BEGIN\\\"\"];\n"
    "\t37 -> 39;\n"
    "\t39 [label=\"statement-list\"];\n"
    "\t39 -> 40;\n"
    "\t40 [label=\"statement\"];\n"
    "\t40 -> 41;\n"
    "\t41 [label=\"\\\"RETURN\\\"\"];\n"
    "\t40 -> 42;\n"
    "\t42 [label=\"additive-expression\"];\n"
    "\t42 -> 43;\n"
    "\t43 [label=\"multiplicative-expression\"];\n"
    "\t43 -> 44;\n"
    "\t44 [label=\"unary-expression\"];\n"
    "\t44 -> 45;\n"
    "\t45 [label=\"primary-expression\"];\n"
    "\t45 -> 46;\n"
    "\t46 [label=\"\\\"h\\\"\"];\n"
    "\t37 -> 47;\n"
    "\t47 [label=\"\\\"END\\\"\"];\n"
    "\t0 -> 48;\n"
    "\t48 [label=\"\\\".\\\"\"];\n"
    "}\n";
//---------------------------------------------------------------------------
static const char* inputFunction02 =
    "PARAM width, height, depth;\n"
    "VAR volume;\n"
    "CONST density = 2400;\n\n"
    "BEGIN\n"
    "  volume := width * height * depth;\n"
    "  RETURN density * volume\n\n"
    "END.";
static const char* expectedPT02 =
    "digraph {\n"
    "\t0 [label=\"function-definition\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\"parameter-declarations\"];\n"
    "\t1 -> 2;\n"
    "\t2 [label=\"\\\"PARAM\\\"\"];\n"
    "\t1 -> 3;\n"
    "\t3 [label=\"declarator-list\"];\n"
    "\t3 -> 4;\n"
    "\t4 [label=\"\\\"width\\\"\"];\n"
    "\t3 -> 5;\n"
    "\t5 [label=\"\\\",\\\"\"];\n"
    "\t3 -> 6;\n"
    "\t6 [label=\"\\\"height\\\"\"];\n"
    "\t3 -> 7;\n"
    "\t7 [label=\"\\\",\\\"\"];\n"
    "\t3 -> 8;\n"
    "\t8 [label=\"\\\"depth\\\"\"];\n"
    "\t1 -> 9;\n"
    "\t9 [label=\"\\\";\\\"\"];\n"
    "\t0 -> 10;\n"
    "\t10 [label=\"variable-declarations\"];\n"
    "\t10 -> 11;\n"
    "\t11 [label=\"\\\"VAR\\\"\"];\n"
    "\t10 -> 12;\n"
    "\t12 [label=\"declarator-list\"];\n"
    "\t12 -> 13;\n"
    "\t13 [label=\"\\\"volume\\\"\"];\n"
    "\t10 -> 14;\n"
    "\t14 [label=\"\\\";\\\"\"];\n"
    "\t0 -> 15;\n"
    "\t15 [label=\"constant-declarations\"];\n"
    "\t15 -> 16;\n"
    "\t16 [label=\"\\\"CONST\\\"\"];\n"
    "\t15 -> 17;\n"
    "\t17 [label=\"init-declarator-list\"];\n"
    "\t17 -> 18;\n"
    "\t18 [label=\"init-declarator\"];\n"
    "\t18 -> 19;\n"
    "\t19 [label=\"\\\"density\\\"\"];\n"
    "\t18 -> 20;\n"
    "\t20 [label=\"\\\"=\\\"\"];\n"
    "\t18 -> 21;\n"
    "\t21 [label=\"2400\"];\n"
    "\t15 -> 22;\n"
    "\t22 [label=\"\\\";\\\"\"];\n"
    "\t0 -> 23;\n"
    "\t23 [label=\"compound-statement\"];\n"
    "\t23 -> 24;\n"
    "\t24 [label=\"\\\"BEGIN\\\"\"];\n"
    "\t23 -> 25;\n"
    "\t25 [label=\"statement-list\"];\n"
    "\t25 -> 26;\n"
    "\t26 [label=\"statement\"];\n"
    "\t26 -> 27;\n"
    "\t27 [label=\"assignment-expression\"];\n"
    "\t27 -> 28;\n"
    "\t28 [label=\"\\\"volume\\\"\"];\n"
    "\t27 -> 29;\n"
    "\t29 [label=\"\\\":=\\\"\"];\n"
    "\t27 -> 30;\n"
    "\t30 [label=\"additive-expression\"];\n"
    "\t30 -> 31;\n"
    "\t31 [label=\"multiplicative-expression\"];\n"
    "\t31 -> 32;\n"
    "\t32 [label=\"unary-expression\"];\n"
    "\t32 -> 33;\n"
    "\t33 [label=\"primary-expression\"];\n"
    "\t33 -> 34;\n"
    "\t34 [label=\"\\\"width\\\"\"];\n"
    "\t31 -> 35;\n"
    "\t35 [label=\"\\\"*\\\"\"];\n"
    "\t31 -> 36;\n"
    "\t36 [label=\"multiplicative-expression\"];\n"
    "\t36 -> 37;\n"
    "\t37 [label=\"unary-expression\"];\n"
    "\t37 -> 38;\n"
    "\t38 [label=\"primary-expression\"];\n"
    "\t38 -> 39;\n"
    "\t39 [label=\"\\\"height\\\"\"];\n"
    "\t36 -> 40;\n"
    "\t40 [label=\"\\\"*\\\"\"];\n"
    "\t36 -> 41;\n"
    "\t41 [label=\"multiplicative-expression\"];\n"
    "\t41 -> 42;\n"
    "\t42 [label=\"unary-expression\"];\n"
    "\t42 -> 43;\n"
    "\t43 [label=\"primary-expression\"];\n"
    "\t43 -> 44;\n"
    "\t44 [label=\"\\\"depth\\\"\"];\n"
    "\t25 -> 45;\n"
    "\t45 [label=\"\\\";\\\"\"];\n"
    "\t25 -> 46;\n"
    "\t46 [label=\"statement\"];\n"
    "\t46 -> 47;\n"
    "\t47 [label=\"\\\"RETURN\\\"\"];\n"
    "\t46 -> 48;\n"
    "\t48 [label=\"additive-expression\"];\n"
    "\t48 -> 49;\n"
    "\t49 [label=\"multiplicative-expression\"];\n"
    "\t49 -> 50;\n"
    "\t50 [label=\"unary-expression\"];\n"
    "\t50 -> 51;\n"
    "\t51 [label=\"primary-expression\"];\n"
    "\t51 -> 52;\n"
    "\t52 [label=\"\\\"density\\\"\"];\n"
    "\t49 -> 53;\n"
    "\t53 [label=\"\\\"*\\\"\"];\n"
    "\t49 -> 54;\n"
    "\t54 [label=\"multiplicative-expression\"];\n"
    "\t54 -> 55;\n"
    "\t55 [label=\"unary-expression\"];\n"
    "\t55 -> 56;\n"
    "\t56 [label=\"primary-expression\"];\n"
    "\t56 -> 57;\n"
    "\t57 [label=\"\\\"volume\\\"\"];\n"
    "\t23 -> 58;\n"
    "\t58 [label=\"\\\"END\\\"\"];\n"
    "\t0 -> 59;\n"
    "\t59 [label=\"\\\".\\\"\"];\n"
    "}\n";
//---------------------------------------------------------------------------
static const char* inputError01 =
    "BEGIN\n"
    "  c := -a * (b + d;\n"
    "END.";
static const char* expectedError01 =
    "2:19: error: expected ')'\n"
    "  c := -a * (b + d;\n"
    "                  ^\n";
//---------------------------------------------------------------------------
static const char* inputError02 =
    "BEGIN\n"
    "  a RETURN b\n"
    "END.";
static const char* expectedError02 =
    "2:5: error: expected ':='\n"
    "  a RETURN b\n"
    "    ^~~~~~\n";
//---------------------------------------------------------------------------
static const char* inputError03 =
    "PARAM a, b c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  RETURN h\n\n"
    "END.";
static const char* expectedError03 =
    "1:12: error: expected ';'\n"
    "PARAM a, b c;\n"
    "           ^\n";
//---------------------------------------------------------------------------
static const char* inputError04 =
    "PARAM a, b, c\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  RETURN h\n\n"
    "END.";
static const char* expectedError04 =
    "2:1: error: expected ';'\n"
    "VAR d, e, f;\n"
    "^~~\n";
//---------------------------------------------------------------------------
static const char* inputError05 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = , h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  RETURN h\n\n"
    "END.";
static const char* expectedError05 =
    "3:11: error: expected 'literal'\n"
    "CONST g = , h = 1, i = 2;\n"
    "          ^\n";
//---------------------------------------------------------------------------
static const char* inputError06 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h 1, i = 2;\n\n"
    "BEGIN\n"
    "  RETURN h\n\n"
    "END.";
static const char* expectedError06 =
    "3:16: error: expected '='\n"
    "CONST g = 0, h 1, i = 2;\n"
    "               ^\n";
//---------------------------------------------------------------------------
static const char* inputError07 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  a = b\n\n"
    "END.";
static const char* expectedError07 =
    "6:5: error: expected ':='\n"
    "  a = b\n"
    "    ^\n";
//---------------------------------------------------------------------------
static const char* inputError08 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  RETURN +b * -4 - h - 4;\n\n"
    "END.";
static const char* expectedError08 =
    "8:1: error: expected 'identifier'\n"
    "END.\n"
    "^~~\n";
//---------------------------------------------------------------------------
static const char* inputError09 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  c := 4"
    "  RETURN +b * -4 - h - 4\n\n"
    "END.";
static const char* expectedError09 =
    "6:11: error: expected 'END'\n"
    "  c := 4  RETURN +b * -4 - h - 4\n"
    "          ^~~~~~\n";
//---------------------------------------------------------------------------
static const char* inputError10 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  c := 4;"
    "  RETURN +b * -4 - h - 4\n\n"
    "END";
static const char* expectedError10 =
    "8:4: error: expected '.'\n"
    "END\n"
    "   ^\n";
static const char* inputError11 =
    "PARAM width, height, depth;\n"
    "VAR volume;\n"
    "CONST density = 2400;\n\n"
    "BEGIN\n"
    "  volume := / height * depth;\n"
    "  RETURN density * volume\n\n"
    "END.";
static const char* expectedError11 =
    "6:13: error: expected literal, identifier or opening parenthesis\n"
    "  volume := / height * depth;\n"
    "            ^\n";
//---------------------------------------------------------------------------
TEST(TestParser, ParseFunction01) {
    ostringstream buf;
    Lexer lexer(inputFunction01, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    DotPTVisitor visitor(buf);
    root->accept(visitor);
    ASSERT_EQ(buf.str(), expectedPT01);
}
//---------------------------------------------------------------------------
TEST(TestParser, ParseFunction02) {
    ostringstream buf;
    Lexer lexer(inputFunction02, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    DotPTVisitor visitor(buf);
    root->accept(visitor);
    ASSERT_EQ(buf.str(), expectedPT02);
}
//---------------------------------------------------------------------------
TEST(TestParser, ErrorExpectedParenthesis) {
    ostringstream buf;
    Lexer lexer(inputError01, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_FALSE(root);
    ASSERT_EQ(buf.str(), expectedError01);
}
//---------------------------------------------------------------------------
TEST(TestParser, ErrorExpectedAssignment) {
    ostringstream buf;
    Lexer lexer(inputError02, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_FALSE(root);
    ASSERT_EQ(buf.str(), expectedError02);
}
//---------------------------------------------------------------------------
TEST(TestParser, ErrorDeclExpectedSemicolon01) {
    ostringstream buf;
    Lexer lexer(inputError03, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_FALSE(root);
    ASSERT_EQ(buf.str(), expectedError03);
}
//---------------------------------------------------------------------------
TEST(TestParser, ErrorDeclExpectedSemicolon02) {
    ostringstream buf;
    Lexer lexer(inputError04, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_FALSE(root);
    ASSERT_EQ(buf.str(), expectedError04);
}
//---------------------------------------------------------------------------
TEST(TestParser, ErrorConstExpectedLiteral) {
    ostringstream buf;
    Lexer lexer(inputError05, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_FALSE(root);
    ASSERT_EQ(buf.str(), expectedError05);
}
//---------------------------------------------------------------------------
TEST(TestParser, ErrorConstExpectedEqual) {
    ostringstream buf;
    Lexer lexer(inputError06, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_FALSE(root);
    ASSERT_EQ(buf.str(), expectedError06);
}
//---------------------------------------------------------------------------
TEST(TestParser, ErrorAssignmentExpectedOperator) {
    ostringstream buf;
    Lexer lexer(inputError07, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_FALSE(root);
    ASSERT_EQ(buf.str(), expectedError07);
}
//---------------------------------------------------------------------------
TEST(TestParser, ErrorStatementExpectedIdentifier) {
    ostringstream buf;
    Lexer lexer(inputError08, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_FALSE(root);
    ASSERT_EQ(buf.str(), expectedError08);
}
//---------------------------------------------------------------------------
TEST(TestParser, ErrorStatementExpectedSemicolon) {
    ostringstream buf;
    Lexer lexer(inputError09, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_FALSE(root);
    ASSERT_EQ(buf.str(), expectedError09);
}
//---------------------------------------------------------------------------
TEST(TestParser, ErrorFunctionExpectedTerminator) {
    ostringstream buf;
    Lexer lexer(inputError10, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_FALSE(root);
    ASSERT_EQ(buf.str(), expectedError10);
}
//---------------------------------------------------------------------------
TEST(TestParser, ErrorFunctionAlternation) {
    ostringstream buf;
    Lexer lexer(inputError11, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_FALSE(root);
    ASSERT_EQ(buf.str(), expectedError11);
}
//---------------------------------------------------------------------------