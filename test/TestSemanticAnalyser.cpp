#include "pljit/ast/DotASTVisitor.hpp"
#include "pljit/parser/Parser.hpp"
#include "pljit/semantics/SemanticAnalyser.hpp"
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <gtest/gtest.h>
//---------------------------------------------------------------------------
using namespace std;
using namespace pljit::ast;
using namespace pljit::lexer;
using namespace pljit::parser;
using namespace pljit::pt;
using namespace pljit::semantics;
//---------------------------------------------------------------------------
static const char* inputFunction01 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  d := (a + 4) * c + i;\n"
    "  e := (a + 4) * c * d * (h + 1);\n"
    "  f := a + b + c + d + e;\n"
    "  RETURN (e + f) - 1 * d + i\n\n"
    "END.";
static const char* expectedOutput01 =
    "digraph {\n"
    "\t0 [label=\"Function\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\":=\"];\n"
    "\t1 -> 2;\n"
    "\t2 [label=\"d\"];\n"
    "\t1 -> 3;\n"
    "\t3 [label=\"+\"];\n"
    "\t3 -> 4;\n"
    "\t4 [label=\"*\"];\n"
    "\t4 -> 5;\n"
    "\t5 [label=\"+\"];\n"
    "\t5 -> 6;\n"
    "\t6 [label=\"a\"];\n"
    "\t5 -> 7;\n"
    "\t7 [label=\"4\"];\n"
    "\t4 -> 8;\n"
    "\t8 [label=\"c\"];\n"
    "\t3 -> 9;\n"
    "\t9 [label=\"i\"];\n"
    "\t0 -> 10;\n"
    "\t10 [label=\":=\"];\n"
    "\t10 -> 11;\n"
    "\t11 [label=\"e\"];\n"
    "\t10 -> 12;\n"
    "\t12 [label=\"*\"];\n"
    "\t12 -> 13;\n"
    "\t13 [label=\"+\"];\n"
    "\t13 -> 14;\n"
    "\t14 [label=\"a\"];\n"
    "\t13 -> 15;\n"
    "\t15 [label=\"4\"];\n"
    "\t12 -> 16;\n"
    "\t16 [label=\"*\"];\n"
    "\t16 -> 17;\n"
    "\t17 [label=\"c\"];\n"
    "\t16 -> 18;\n"
    "\t18 [label=\"*\"];\n"
    "\t18 -> 19;\n"
    "\t19 [label=\"d\"];\n"
    "\t18 -> 20;\n"
    "\t20 [label=\"+\"];\n"
    "\t20 -> 21;\n"
    "\t21 [label=\"h\"];\n"
    "\t20 -> 22;\n"
    "\t22 [label=\"1\"];\n"
    "\t0 -> 23;\n"
    "\t23 [label=\":=\"];\n"
    "\t23 -> 24;\n"
    "\t24 [label=\"f\"];\n"
    "\t23 -> 25;\n"
    "\t25 [label=\"+\"];\n"
    "\t25 -> 26;\n"
    "\t26 [label=\"a\"];\n"
    "\t25 -> 27;\n"
    "\t27 [label=\"+\"];\n"
    "\t27 -> 28;\n"
    "\t28 [label=\"b\"];\n"
    "\t27 -> 29;\n"
    "\t29 [label=\"+\"];\n"
    "\t29 -> 30;\n"
    "\t30 [label=\"c\"];\n"
    "\t29 -> 31;\n"
    "\t31 [label=\"+\"];\n"
    "\t31 -> 32;\n"
    "\t32 [label=\"d\"];\n"
    "\t31 -> 33;\n"
    "\t33 [label=\"e\"];\n"
    "\t0 -> 34;\n"
    "\t34 [label=\"RETURN\"];\n"
    "\t34 -> 35;\n"
    "\t35 [label=\"-\"];\n"
    "\t35 -> 36;\n"
    "\t36 [label=\"+\"];\n"
    "\t36 -> 37;\n"
    "\t37 [label=\"e\"];\n"
    "\t36 -> 38;\n"
    "\t38 [label=\"f\"];\n"
    "\t35 -> 39;\n"
    "\t39 [label=\"+\"];\n"
    "\t39 -> 40;\n"
    "\t40 [label=\"*\"];\n"
    "\t40 -> 41;\n"
    "\t41 [label=\"1\"];\n"
    "\t40 -> 42;\n"
    "\t42 [label=\"d\"];\n"
    "\t39 -> 43;\n"
    "\t43 [label=\"i\"];\n"
    "}\n";
//---------------------------------------------------------------------------
static const char* inputFunction02 =
    "PARAM width, height, depth;\n"
    "VAR volume;\n"
    "CONST density = 2400;\n\n"
    "BEGIN\n"
    "  volume := width * height * depth;\n"
    "  RETURN density * volume\n\n"
    "END.";
static const char* expectedOutput02 =
    "digraph {\n"
    "\t0 [label=\"Function\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\":=\"];\n"
    "\t1 -> 2;\n"
    "\t2 [label=\"volume\"];\n"
    "\t1 -> 3;\n"
    "\t3 [label=\"*\"];\n"
    "\t3 -> 4;\n"
    "\t4 [label=\"width\"];\n"
    "\t3 -> 5;\n"
    "\t5 [label=\"*\"];\n"
    "\t5 -> 6;\n"
    "\t6 [label=\"height\"];\n"
    "\t5 -> 7;\n"
    "\t7 [label=\"depth\"];\n"
    "\t0 -> 8;\n"
    "\t8 [label=\"RETURN\"];\n"
    "\t8 -> 9;\n"
    "\t9 [label=\"*\"];\n"
    "\t9 -> 10;\n"
    "\t10 [label=\"density\"];\n"
    "\t9 -> 11;\n"
    "\t11 [label=\"volume\"];\n"
    "}\n";
//---------------------------------------------------------------------------
static const char* inputFunction03 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  d := (a + 4) * c + i;\n"
    "  e := (a + 4) * c * d * (h + 1);\n"
    "  f := a + b + c + d + e\n\n"
    "END.";
static const char* expectedOutput03 =
    "8:3: error: missing return statement\n"
    "  f := a + b + c + d + e\n"
    "  ^~~~~~~~~~~~~~~~~~~~~~\n";
//---------------------------------------------------------------------------
static const char* inputFunction04 =
    "PARAM a, b, a;\n"
    "BEGIN\n"
    "  RETURN a\n\n"
    "END.";
static const char* expectedOutput04 =
    "1:13: error: identifier being declared twice\n"
    "PARAM a, b, a;\n"
    "            ^\n";
//---------------------------------------------------------------------------
static const char* inputFunction05 =
    "PARAM a, b, c;\n"
    "VAR c, e, f;\n"
    "BEGIN\n"
    "  RETURN a\n\n"
    "END.";
static const char* expectedOutput05 =
    "2:5: error: identifier being declared twice\n"
    "VAR c, e, f;\n"
    "    ^\n";
//---------------------------------------------------------------------------
static const char* inputFunction06 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, c = 2;\n\n"
    "BEGIN\n"
    "  RETURN a\n\n"
    "END.";
static const char* expectedOutput06 =
    "3:21: error: identifier being declared twice\n"
    "CONST g = 0, h = 1, c = 2;\n"
    "                    ^\n";
//---------------------------------------------------------------------------
static const char* inputFunction07 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  RETURN j\n\n"
    "END.";
static const char* expectedOutput07 =
    "6:10: error: undeclared identifier\n"
    "  RETURN j\n"
    "         ^\n";
//---------------------------------------------------------------------------
static const char* inputFunction08 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  RETURN f\n\n"
    "END.";
static const char* expectedOutput08 =
    "6:10: error: uninitialized variable\n"
    "  RETURN f\n"
    "         ^\n";
//---------------------------------------------------------------------------
static const char* inputFunction09 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  g := 1;\n"
    "  RETURN g\n\n"
    "END.";
static const char* expectedOutput09 =
    "6:3: error: assigning a value to a constant\n"
    "  g := 1;\n"
    "  ^~~~~~\n";
//---------------------------------------------------------------------------
static const char* inputFunction10 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, f = 2;\n\n"
    "BEGIN\n"
    "  g := d;\n"
    "  h := z\n"
    "END.";
static const char* expectedOutput10 =
    "3:21: error: identifier being declared twice\n"
    "CONST g = 0, h = 1, f = 2;\n"
    "                    ^\n"
    "6:3: error: assigning a value to a constant\n"
    "  g := d;\n"
    "  ^~~~~~\n"
    "6:8: error: uninitialized variable\n"
    "  g := d;\n"
    "       ^\n"
    "7:3: error: assigning a value to a constant\n"
    "  h := z\n"
    "  ^~~~~~\n"
    "7:8: error: undeclared identifier\n"
    "  h := z\n"
    "       ^\n"
    "7:3: error: missing return statement\n"
    "  h := z\n"
    "  ^~~~~~\n";
//---------------------------------------------------------------------------
static const char* inputFunction11 =
    "PARAM a, b, c;\n"
    "VAR d;\n"
    "CONST e = 2;\n\n"
    "BEGIN\n"
    "  d := +(1+a) -3 * b + d;\n"
    "  RETURN e * d\n\n"
    "END.";
static const char* expectedOutput11 =
    "6:24: error: uninitialized variable\n"
    "  d := +(1+a) -3 * b + d;\n"
    "                       ^\n"
    "7:14: error: uninitialized variable\n"
    "  RETURN e * d\n"
    "             ^\n";
//---------------------------------------------------------------------------
TEST(TestSemantics, AnaylzeFunction01) {
    ostringstream buf;
    Lexer lexer(inputFunction01, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    DotASTVisitor visitor(buf);
    ast->accept(visitor);
    ASSERT_EQ(buf.str(), expectedOutput01);
}
//---------------------------------------------------------------------------
TEST(TestSemantics, AnaylzeFunction02) {
    ostringstream buf;
    Lexer lexer(inputFunction02, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    DotASTVisitor visitor(buf);
    ast->accept(visitor);
    ASSERT_EQ(buf.str(), expectedOutput02);
}
//---------------------------------------------------------------------------
TEST(TestSemantics, CheckSymbolTable) {
    ostringstream buf;
    Lexer lexer(inputFunction01, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    unordered_map<string, int64_t> consts;
    unordered_map<string, int64_t> expectedConsts = {
        {"g", 0},
        {"h", 1},
        {"i", 2}};
    unordered_map<string, size_t> params;
    unordered_map<string, size_t> expectedParams = {
        {"a", 0},
        {"b", 1},
        {"c", 2}};
    unordered_set<string> vars;
    unordered_set<string> expectedVars = {"d", "e", "f"};

    for (auto it = ast->getSymTable().begin(); it != ast->getSymTable().end(); ++it) {
        if (ast->getSymTable().lookupConst(*it)) {
            consts.insert({*it, ast->getSymTable().getConstValue(*it)});
        } else if (ast->getSymTable().lookupParam(*it)) {
            params.insert({*it, ast->getSymTable().getParamIndex(*it)});
        } else {
            vars.insert(*it);
        }
    }

    ASSERT_EQ(consts, expectedConsts);
    ASSERT_EQ(params, expectedParams);
    ASSERT_EQ(vars, expectedVars);
}
//---------------------------------------------------------------------------
TEST(TestSemantics, ErrorMissingReturn) {
    ostringstream buf;
    Lexer lexer(inputFunction03, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_FALSE(ast);
    ASSERT_EQ(buf.str(), expectedOutput03);
}
//---------------------------------------------------------------------------
TEST(TestSemantics, ErrorIdentifierDeclaredTwice01) {
    ostringstream buf;
    Lexer lexer(inputFunction04, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_FALSE(ast);
    ASSERT_EQ(buf.str(), expectedOutput04);
}
//---------------------------------------------------------------------------
TEST(TestSemantics, ErrorIdentifierDeclaredTwice02) {
    ostringstream buf;
    Lexer lexer(inputFunction05, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_FALSE(ast);
    ASSERT_EQ(buf.str(), expectedOutput05);
}
//---------------------------------------------------------------------------
TEST(TestSemantics, ErrorIdentifierDeclaredTwice03) {
    ostringstream buf;
    Lexer lexer(inputFunction06, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_FALSE(ast);
    ASSERT_EQ(buf.str(), expectedOutput06);
}
//---------------------------------------------------------------------------
TEST(TestSemantics, ErrorUndeclaredIdentifier) {
    ostringstream buf;
    Lexer lexer(inputFunction07, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_FALSE(ast);
    ASSERT_EQ(buf.str(), expectedOutput07);
}
//---------------------------------------------------------------------------
TEST(TestSemantics, ErrorUninitializedVariable) {
    ostringstream buf;
    Lexer lexer(inputFunction08, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_FALSE(ast);
    ASSERT_EQ(buf.str(), expectedOutput08);
}
//---------------------------------------------------------------------------
TEST(TestSemantics, ErrorConstAssigning) {
    ostringstream buf;
    Lexer lexer(inputFunction09, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_FALSE(ast);
    ASSERT_EQ(buf.str(), expectedOutput09);
}
//---------------------------------------------------------------------------
TEST(TestSemantics, ErrorVarious) {
    ostringstream buf;
    Lexer lexer(inputFunction10, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_FALSE(ast);
    ASSERT_EQ(buf.str(), expectedOutput10);
}
//---------------------------------------------------------------------------
TEST(TestSemantics, ErrorUninitializedVariableTransitive) {
    ostringstream buf;
    Lexer lexer(inputFunction11, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_FALSE(ast);
    ASSERT_EQ(buf.str(), expectedOutput11);
}
//---------------------------------------------------------------------------