#include "pljit/code/SourceCode.hpp"
#include <sstream>
#include <gtest/gtest.h>
//---------------------------------------------------------------------------
using namespace std;
using namespace pljit::code;
//---------------------------------------------------------------------------
static const char* expectedPrintLocationRef =
    "2:19: error: expected ')'\n"
    "  c := -a * (b + d;\n"
    "                  ^\n";
static const char* expectedPrintRangeRef =
    "1:3: error: expected ':='\n"
    "a RETURN b\n"
    "  ^~~~~~\n";
//---------------------------------------------------------------------------
TEST(TestSourceCode, ResolveLocationRef) {
    SourceCode manager("test");
    SourceCode::LocationRef ref(manager.string_view(), 0);
    auto pos = ref.resolvePos();
    ASSERT_EQ(pos.line, 1);
    ASSERT_EQ(pos.col, 1);
}
//---------------------------------------------------------------------------
TEST(TestSourceCode, ResolveLocationRef01) {
    SourceCode::LocationRef ref("test\ntest", 9);
    auto pos = ref.resolvePos();
    ASSERT_EQ(pos.line, 2);
    ASSERT_EQ(pos.col, 5);
}
//---------------------------------------------------------------------------
TEST(TestSourceCode, PrintLocationRef) {
    SourceCode::LocationRef ref("\n  c := -a * (b + d;", 19);

    ostringstream buf;
    ref.print("error: expected ')'", buf);

    ASSERT_EQ(buf.str(), expectedPrintLocationRef);
}
//---------------------------------------------------------------------------
TEST(TestSourceCode, PrintRangeRef) {
    SourceCode::RangeRef ref("a RETURN b", 2, 8);

    ostringstream buf;
    ref.print("error: expected ':='", buf);

    ASSERT_EQ(buf.str(), expectedPrintRangeRef);
}
//---------------------------------------------------------------------------