#include "pljit/ast/DotASTVisitor.hpp"
#include "pljit/optimizer/ConstPropOptimizer.hpp"
#include "pljit/optimizer/DeadCodeOptimizer.hpp"
#include "pljit/parser/Parser.hpp"
#include "pljit/semantics/SemanticAnalyser.hpp"
#include <sstream>
#include <gtest/gtest.h>
//---------------------------------------------------------------------------
using namespace std;
using namespace pljit::ast;
using namespace pljit::lexer;
using namespace pljit::optimizer;
using namespace pljit::parser;
using namespace pljit::pt;
using namespace pljit::semantics;
//---------------------------------------------------------------------------
static const char* inputFunction01 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  d := (a + 4) * c + i;\n"
    "  e := (a + 4) * c * d * (h + 1);\n"
    "  f := a + b + c + d + e;\n"
    "  RETURN (e + f) - 1 * d + i;\n"
    "  a := a + b + c + f\n\n"
    "END.";
static const char* expectedOutput01 =
    "digraph {\n"
    "\t0 [label=\"Function\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\":=\"];\n"
    "\t1 -> 2;\n"
    "\t2 [label=\"d\"];\n"
    "\t1 -> 3;\n"
    "\t3 [label=\"+\"];\n"
    "\t3 -> 4;\n"
    "\t4 [label=\"*\"];\n"
    "\t4 -> 5;\n"
    "\t5 [label=\"+\"];\n"
    "\t5 -> 6;\n"
    "\t6 [label=\"a\"];\n"
    "\t5 -> 7;\n"
    "\t7 [label=\"4\"];\n"
    "\t4 -> 8;\n"
    "\t8 [label=\"c\"];\n"
    "\t3 -> 9;\n"
    "\t9 [label=\"i\"];\n"
    "\t0 -> 10;\n"
    "\t10 [label=\":=\"];\n"
    "\t10 -> 11;\n"
    "\t11 [label=\"e\"];\n"
    "\t10 -> 12;\n"
    "\t12 [label=\"*\"];\n"
    "\t12 -> 13;\n"
    "\t13 [label=\"+\"];\n"
    "\t13 -> 14;\n"
    "\t14 [label=\"a\"];\n"
    "\t13 -> 15;\n"
    "\t15 [label=\"4\"];\n"
    "\t12 -> 16;\n"
    "\t16 [label=\"*\"];\n"
    "\t16 -> 17;\n"
    "\t17 [label=\"c\"];\n"
    "\t16 -> 18;\n"
    "\t18 [label=\"*\"];\n"
    "\t18 -> 19;\n"
    "\t19 [label=\"d\"];\n"
    "\t18 -> 20;\n"
    "\t20 [label=\"+\"];\n"
    "\t20 -> 21;\n"
    "\t21 [label=\"h\"];\n"
    "\t20 -> 22;\n"
    "\t22 [label=\"1\"];\n"
    "\t0 -> 23;\n"
    "\t23 [label=\":=\"];\n"
    "\t23 -> 24;\n"
    "\t24 [label=\"f\"];\n"
    "\t23 -> 25;\n"
    "\t25 [label=\"+\"];\n"
    "\t25 -> 26;\n"
    "\t26 [label=\"a\"];\n"
    "\t25 -> 27;\n"
    "\t27 [label=\"+\"];\n"
    "\t27 -> 28;\n"
    "\t28 [label=\"b\"];\n"
    "\t27 -> 29;\n"
    "\t29 [label=\"+\"];\n"
    "\t29 -> 30;\n"
    "\t30 [label=\"c\"];\n"
    "\t29 -> 31;\n"
    "\t31 [label=\"+\"];\n"
    "\t31 -> 32;\n"
    "\t32 [label=\"d\"];\n"
    "\t31 -> 33;\n"
    "\t33 [label=\"e\"];\n"
    "\t0 -> 34;\n"
    "\t34 [label=\"RETURN\"];\n"
    "\t34 -> 35;\n"
    "\t35 [label=\"-\"];\n"
    "\t35 -> 36;\n"
    "\t36 [label=\"+\"];\n"
    "\t36 -> 37;\n"
    "\t37 [label=\"e\"];\n"
    "\t36 -> 38;\n"
    "\t38 [label=\"f\"];\n"
    "\t35 -> 39;\n"
    "\t39 [label=\"+\"];\n"
    "\t39 -> 40;\n"
    "\t40 [label=\"*\"];\n"
    "\t40 -> 41;\n"
    "\t41 [label=\"1\"];\n"
    "\t40 -> 42;\n"
    "\t42 [label=\"d\"];\n"
    "\t39 -> 43;\n"
    "\t43 [label=\"i\"];\n"
    "}\n";
//---------------------------------------------------------------------------
static const char* inputFunction02 =
    "PARAM width, height, depth;\n"
    "VAR volume;\n"
    "CONST density = 2400;\n\n"
    "BEGIN\n"
    "  volume := width * height * depth;\n"
    "  RETURN density * volume;\n"
    "  RETURN width;\n"
    "  RETURN density\n\n"
    "END.";
static const char* expectedOutput02 =
    "digraph {\n"
    "\t0 [label=\"Function\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\":=\"];\n"
    "\t1 -> 2;\n"
    "\t2 [label=\"volume\"];\n"
    "\t1 -> 3;\n"
    "\t3 [label=\"*\"];\n"
    "\t3 -> 4;\n"
    "\t4 [label=\"width\"];\n"
    "\t3 -> 5;\n"
    "\t5 [label=\"*\"];\n"
    "\t5 -> 6;\n"
    "\t6 [label=\"height\"];\n"
    "\t5 -> 7;\n"
    "\t7 [label=\"depth\"];\n"
    "\t0 -> 8;\n"
    "\t8 [label=\"RETURN\"];\n"
    "\t8 -> 9;\n"
    "\t9 [label=\"*\"];\n"
    "\t9 -> 10;\n"
    "\t10 [label=\"density\"];\n"
    "\t9 -> 11;\n"
    "\t11 [label=\"volume\"];\n"
    "}\n";
//---------------------------------------------------------------------------
static const char* inputFunction03 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  d := 2;\n"
    "  RETURN (3 + 1) - 2 * 4 + d\n"
    "END.";
static const char* expectedOutput03 =
    "digraph {\n"
    "\t0 [label=\"Function\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\":=\"];\n"
    "\t1 -> 2;\n"
    "\t2 [label=\"d\"];\n"
    "\t1 -> 3;\n"
    "\t3 [label=\"2\"];\n"
    "\t0 -> 4;\n"
    "\t4 [label=\"RETURN\"];\n"
    "\t4 -> 5;\n"
    "\t5 [label=\"-6\"];\n}"
    "\n";
//---------------------------------------------------------------------------
static const char* inputFunction04 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  d := 2;\n"
    "  d := c;\n"
    "  RETURN (3 + 1) - 2 * 4 + d\n"
    "END.";
static const char* expectedOutput04 =
    "digraph {\n"
    "\t0 [label=\"Function\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\":=\"];\n"
    "\t1 -> 2;\n"
    "\t2 [label=\"d\"];\n"
    "\t1 -> 3;\n"
    "\t3 [label=\"2\"];\n"
    "\t0 -> 4;\n"
    "\t4 [label=\":=\"];\n"
    "\t4 -> 5;\n"
    "\t5 [label=\"d\"];\n"
    "\t4 -> 6;\n"
    "\t6 [label=\"c\"];\n"
    "\t0 -> 7;\n"
    "\t7 [label=\"RETURN\"];\n"
    "\t7 -> 8;\n"
    "\t8 [label=\"-\"];\n"
    "\t8 -> 9;\n"
    "\t9 [label=\"4\"];\n"
    "\t8 -> 10;\n"
    "\t10 [label=\"+\"];\n"
    "\t10 -> 11;\n"
    "\t11 [label=\"8\"];\n"
    "\t10 -> 12;\n"
    "\t12 [label=\"d\"];\n}"
    "\n";
//---------------------------------------------------------------------------
static const char* inputFunction05 =
    "PARAM a, b, c;\n"
    "VAR d, e, f;\n"
    "CONST g = 0, h = 1, i = 2;\n\n"
    "BEGIN\n"
    "  d := 2;\n"
    "  d := c;\n"
    "  e := 10;\n"
    "  RETURN (h + 4) * d * e * (i + 1);\n"
    " f := 2 + 3;\n"
    "  RETURN e\n"
    "END.";
static const char* expectedOutput05 =
    "digraph {\n"
    "\t0 [label=\"Function\"];\n"
    "\t0 -> 1;\n"
    "\t1 [label=\":=\"];\n"
    "\t1 -> 2;\n"
    "\t2 [label=\"d\"];\n"
    "\t1 -> 3;\n"
    "\t3 [label=\"2\"];\n"
    "\t0 -> 4;\n"
    "\t4 [label=\":=\"];\n"
    "\t4 -> 5;\n"
    "\t5 [label=\"d\"];\n"
    "\t4 -> 6;\n"
    "\t6 [label=\"c\"];\n"
    "\t0 -> 7;\n"
    "\t7 [label=\":=\"];\n"
    "\t7 -> 8;\n"
    "\t8 [label=\"e\"];\n"
    "\t7 -> 9;\n"
    "\t9 [label=\"10\"];\n"
    "\t0 -> 10;\n"
    "\t10 [label=\"RETURN\"];\n"
    "\t10 -> 11;\n"
    "\t11 [label=\"*\"];\n"
    "\t11 -> 12;\n"
    "\t12 [label=\"5\"];\n"
    "\t11 -> 13;\n"
    "\t13 [label=\"*\"];\n"
    "\t13 -> 14;\n"
    "\t14 [label=\"d\"];\n"
    "\t13 -> 15;\n"
    "\t15 [label=\"30\"];\n"
    "}\n";
//---------------------------------------------------------------------------
TEST(TestOptimizer, OptimizeDeadCode01) {
    ostringstream buf;
    Lexer lexer(inputFunction01, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    DotASTVisitor visitor(buf);
    DeadCodeOptimizer opt;
    opt.optimize(ast);
    ast->accept(visitor);
    ASSERT_EQ(buf.str(), expectedOutput01);
}
//---------------------------------------------------------------------------
TEST(TestOptimizer, OptimizeDeadCode02) {
    ostringstream buf;
    Lexer lexer(inputFunction02, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    DotASTVisitor visitor(buf);
    DeadCodeOptimizer opt;
    opt.optimize(ast);
    ast->accept(visitor);
    ASSERT_EQ(buf.str(), expectedOutput02);
}
//---------------------------------------------------------------------------
TEST(TestOptimizer, OptimizeConstProp01) {
    ostringstream buf;
    Lexer lexer(inputFunction03, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    DotASTVisitor visitor(buf);
    ConstPropOptimizer opt;
    opt.optimize(ast);
    ast->accept(visitor);
    ASSERT_EQ(buf.str(), expectedOutput03);
}
//---------------------------------------------------------------------------
TEST(TestOptimizer, OptimizeConstProp02) {
    ostringstream buf;
    Lexer lexer(inputFunction04, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    DotASTVisitor visitor(buf);
    ConstPropOptimizer opt;
    opt.optimize(ast);
    ast->accept(visitor);
    ASSERT_EQ(buf.str(), expectedOutput04);
}
//---------------------------------------------------------------------------
TEST(TestOptimizer, OptimizeBoth01) {
    ostringstream buf;
    Lexer lexer(inputFunction05, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    DotASTVisitor visitor(buf);
    DeadCodeOptimizer opt1;
    ConstPropOptimizer opt2;
    opt1.optimize(ast);
    opt2.optimize(ast);
    ast->accept(visitor);
    ASSERT_EQ(buf.str(), expectedOutput05);
}
//---------------------------------------------------------------------------
TEST(TestOptimizer, OptimizeBoth02) {
    ostringstream buf;
    Lexer lexer(inputFunction05, buf);
    Parser parser(lexer, buf);
    auto root = parser.parseFunctionDefinition();
    ASSERT_TRUE(root);

    SemanticAnalyser sem(buf);
    auto ast = sem.analyzeFunction(*root);
    ASSERT_TRUE(ast);

    DotASTVisitor visitor(buf);
    ConstPropOptimizer opt1;
    DeadCodeOptimizer opt2;
    opt1.optimize(ast);
    opt2.optimize(ast);
    ast->accept(visitor);
    ASSERT_EQ(buf.str(), expectedOutput05);
}
//---------------------------------------------------------------------------